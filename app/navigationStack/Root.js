import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import React from 'react';
import ParentState from '../context/parent/provider';
import UserState from '../context/user/provider';
import ParentStack from './Parent';

const Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0F6880',
  },
};

export default function RootNavigation() {
  return (
    <UserState>
      <ParentState>
        <NavigationContainer theme={Theme}>
          <ParentStack />
        </NavigationContainer>
      </ParentState>
    </UserState>
  );
}
