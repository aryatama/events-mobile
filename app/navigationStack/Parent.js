import React, { useContext } from 'react';
import { Image, TouchableOpacity, Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import Icon from '../utils/parent/icon';
import {
  AccountSwitcherScreen,
  DashboardScreen,
  TasksScreen,
  AccountScreen,
  AdjustLimitScreen,
  AddTaskScreen,
  ManageChildScreen,
  NotificationsScreen,
  TopUpBalanceScreen,
  TransferFunds,
  SeeGoalsScreen,
  LockScreen,
  ActivateCardScreen,
  AddChildStep1,
  AddChildStep2,
  AddChildStep3,
  AddChildStep4,
  AddChildStep5,
  AddChildStep6,
  TaskDetailsScreen,
  WithdrawBalanceScreen,
  ManageAccount,
  NotFoundScreen,
  ManageSubscription,
} from '../screen/parent';
import { CommonActions, useNavigation } from '@react-navigation/native';
import parentContext from '../context/parent/parentContext';
import NotificationIcon from '../components/parent/notificationIcon';

const MainStack = createStackNavigator();
function ParentStack() {
  const ParentContext = useContext(parentContext);
  return (
    <MainStack.Navigator
      screenOptions={{
        headerTitleAlign: 'center',
        headerTitleStyle: { color: '#0F6880', fontSize: 18 },
        headerRight: function headerRight(props) {
          return <NotificationIcon {...props} />;
        },
        headerLeft:
          Platform.OS === 'ios'
            ? function backBut() {
                const navigation = useNavigation();
                return (
                  <HeaderBackButton
                    label="Back"
                    tintColor="#1DA2C3"
                    labelVisible={true}
                    onPress={() => navigation.goBack()}
                  />
                );
              }
            : function backBut() {
                const navigation = useNavigation();
                return (
                  <HeaderBackButton
                    label="Back"
                    tintColor="#1DA2C3"
                    labelVisible={true}
                    backImage={CustomBackButton}
                    onPress={() => navigation.goBack()}
                  />
                );
              },

        headerStyle: {
          shadowOpacity: 0,
          shadowOffset: {
            height: 0,
          },
          shadowRadius: 0,
          elevation: 0,
          height: Platform.OS === 'ios' ? 110 : 70,
        },
      }}>
      <MainStack.Screen
        name="ParentTabs"
        component={ParentTabs}
        options={{
          headerTitle: '',
          headerLeft: function _() {
            const navigation = useNavigation();
            return (
              <TouchableOpacity
                onPress={() => navigation.navigate('AccountSwitcherScreen')}>
                <Image
                  style={{ width: 120, margin: 10, resizeMode: 'contain' }}
                  source={require('../../assets/icons/parent/logo.png')}
                />
              </TouchableOpacity>
            );
          },
        }}
      />
      <MainStack.Screen
        name="AccountSwitcherScreen"
        component={AccountSwitcherScreen}
        options={{
          headerTitle: 'Switch Accounts',
        }}
      />
      <MainStack.Screen
        name="ManageChildScreen"
        component={ManageChildScreen}
        options={{
          headerTitle: 'Manage Child',
        }}
      />
      <MainStack.Screen
        name="AdjustLimitScreen"
        component={AdjustLimitScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddTaskScreen"
        component={AddTaskScreen}
        options={{
          headerTitle: '',
        }}
      />

      <MainStack.Screen
        name="TopUpBalanceScreen"
        component={TopUpBalanceScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="NotificationsScreen"
        component={NotificationsScreen}
        options={{
          headerTitle: '',
          headerLeft:
            Platform.OS === 'ios'
              ? function backBut() {
                  const navigation = useNavigation();
                  return (
                    <HeaderBackButton
                      label="Back"
                      tintColor="#1DA2C3"
                      labelVisible={true}
                      onPress={() => {
                        navigation.goBack();
                        ParentContext.refetchNotifications();
                      }}
                    />
                  );
                }
              : function backBut() {
                  const navigation = useNavigation();
                  return (
                    <HeaderBackButton
                      label="Back"
                      tintColor="#1DA2C3"
                      labelVisible={true}
                      backImage={CustomBackButton}
                      onPress={() => {
                        ParentContext.refetchNotifications();
                        navigation.goBack();
                      }}
                    />
                  );
                },
        }}
      />
      <MainStack.Screen
        name="TransferFundsScreen"
        component={TransferFunds}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="SeeGoalsScreen"
        component={SeeGoalsScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="LockScreen"
        component={LockScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="ActivateCardScreen"
        component={ActivateCardScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep1"
        component={AddChildStep1}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep2"
        component={AddChildStep2}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep3"
        component={AddChildStep3}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep4"
        component={AddChildStep4}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep5"
        component={AddChildStep5}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="AddChildStep6"
        component={AddChildStep6}
        options={{
          headerTitle: '',
          headerLeft: function backBut() {
            const navigation = useNavigation();
            return (
              <HeaderBackButton
                label="Back"
                onPress={() =>
                  navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [{ name: 'ParentTabs' }],
                    }),
                  )
                }
              />
            );
          },
        }}
      />
      <MainStack.Screen
        name="TaskDetailsScreen"
        component={TaskDetailsScreen}
        options={{
          headerTitle: 'Task Details',
        }}
      />
      <MainStack.Screen
        name="WithdrawBalanceScreen"
        component={WithdrawBalanceScreen}
        options={{
          headerTitle: '',
        }}
      />
      <MainStack.Screen
        name="ManageAccountScreen"
        component={ManageAccount}
        options={{
          headerTitle: 'Manage Account',
        }}
      />
      <MainStack.Screen
        name="ManageSubscriptionScreen"
        component={ManageSubscription}
        options={{
          headerTitle: 'Manage Subscription',
        }}
      />
      <MainStack.Screen
        name="NotFoundScreen"
        component={NotFoundScreen}
        options={{
          headerTitle: '',
        }}
      />
    </MainStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();
function ParentTabs() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#24C1E7',
        tabStyle: { backgroundColor: '#F8F8F8' },
      }}>
      <Tab.Screen
        name="Dashboard"
        component={DashboardScreen}
        options={{
          title: 'Dashboard',
          tabBarIcon: function icon(props) {
            return <Icon size={28} color={props.color} name="home" />;
          },
        }}
      />
      <Tab.Screen
        name="Tasks"
        component={TasksScreen}
        options={{
          title: 'Tasks',
          tabBarIcon: function icon(props) {
            return <Icon size={27} color={props.color} name="task" />;
          },
        }}
      />
      <Tab.Screen
        name="Account"
        component={AccountScreen}
        options={{
          title: 'Account',
          tabBarIcon: function icon(props) {
            return <Icon size={27} color={props.color} name="users" />;
          },
        }}
      />
    </Tab.Navigator>
  );
}

const CustomBackButton = () => {
  return (
    <Image
      style={{ width: 28, height: 28 }}
      source={require('../../assets/icons/parent/cheveron.png')}
    />
  );
};

export default ParentStack;
