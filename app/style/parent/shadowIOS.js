export const shadowIOS = {
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 1 },
  shadowOpacity: 0.6,
  shadowRadius: 1,
};
