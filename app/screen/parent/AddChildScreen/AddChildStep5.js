import React, { useState, useContext } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import { LoadingScreen, RoundedButton } from '../../../components/parent';
import { postParentChildRegister } from '../../../api/parent';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../../context/parent/parentContext';

const AddChildStep5 = (props) => {
  const {
    email,
    phoneNum,
    password,
    fullName,
    year,
    isEnabled,
    countryCode,
    country,
    streetName,
    unitNo,
    postalNumber,
  } = props.route.params;
  const [color, setColor] = useState('');
  const mutationPostParentChildRegister = postParentChildRegister();
  const ParentContext = useContext(parentContext);

  const handlePickCard = (val) => {
    setColor(val);
  };
  if (mutationPostParentChildRegister.isLoading) {
    // return <Text>Loading...</Text>;
    return <LoadingScreen />
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{ width: '100%' }} showsVerticalScrollIndicator={false}>
        <View
          style={{
            width: '100%',
            paddingVertical: 18,
            backgroundColor: '#FBFBFB',
          }}>
          <Text style={styles.title}>Add Child</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.bigDash}></View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
          </View>
          <View style={{ width: '90%', marginBottom: 30 }}>
            <View style={{ paddingVertical: 20 }}>
              <Text style={styles.atitle}>Setup Your Child's card</Text>
              <Text style={{ fontSize: 14, color: '#0F6880' }}>
                Ask your child their card preference.
              </Text>
            </View>
            <TouchableOpacity
              style={styles.cardCon}
              onPress={() => handlePickCard('blue')}>
              <View style={styles.imageCon}>
                <Image
                  style={
                    color === 'blue'
                      ? styles.imageCard
                      : styles.imageCardCommmon
                  }
                  resizeMode="cover"
                  source={require('../../../../assets/images/app/Blue_Card.png')}
                />
                <View style={styles.icon}>
                  <View style={styles.icon}>
                    {color === 'blue' && (
                      <MIcon name="check-circle" size={40} color="#36CF58" />
                    )}
                  </View>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardCon}
              onPress={() => handlePickCard('green')}>
              <View style={styles.imageCon}>
                <Image
                  style={
                    color === 'green'
                      ? styles.imageCard
                      : styles.imageCardCommmon
                  }
                  resizeMode="cover"
                  source={require('../../../../assets/images/app/Green_Card.png')}
                />
                <View style={styles.icon}>
                  {color === 'green' && (
                    <MIcon name="check-circle" size={40} color="#36CF58" />
                  )}
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardCon}
              onPress={() => handlePickCard('purple')}>
              <View style={styles.imageCon}>
                <Image
                  style={
                    color === 'purple'
                      ? styles.imageCard
                      : styles.imageCardCommmon
                  }
                  resizeMode="cover"
                  source={require('../../../../assets/images/app/Purple_Card.png')}
                />
                <View style={styles.icon}>
                  <View style={styles.icon}>
                    {color === 'purple' && (
                      <MIcon name="check-circle" size={40} color="#36CF58" />
                    )}
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <RoundedButton
            text="Finish"
            onPress={() => {
              mutationPostParentChildRegister.mutate(
                {
                  email: email,
                  phone: phoneNum,
                  password: password,
                  fullName: fullName,
                  dob: year,
                  personalizedCardId:
                    color === 'blue'
                      ? '5f61efff234e400c548efe28'
                      : color === 'green'
                        ? '5f61f015234e400c548efe29'
                        : '5f61f022234e400c548efe2a',
                  deviceType: Platform.OS,
                  sameAddressOn: isEnabled,
                  address: {
                    countryCode: countryCode,
                    country: country,
                    address_1: streetName,
                    address_2: unitNo,
                    postalCode: postalNumber,
                  },
                },
                {
                  onSuccess: async (message) => {
                    ParentContext.refetchChildren();
                    Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                    return props.navigation.navigate('AddChildStep6');
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AddChildStep5;
let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  icon: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    position: 'absolute',
  },
  cardCon: {
    width: '100%',
    marginBottom: 50,
    height: (deviceWidth * 4.5) / 10,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },
  imageCard: {
    width: 298,
    height: 193,
    borderWidth: 5,
    borderColor: '#36CF58',
    borderRadius: 14,
  },
  imageCardCommmon: {
    width: 298,
    height: 193,
    borderRadius: 14,
  },
  imageCon: {
    width: 340,
    height: 240,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 16,
    color: '#0F6880',
  },
});
