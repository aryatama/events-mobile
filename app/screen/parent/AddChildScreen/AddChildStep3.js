import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import MIcon from 'react-native-vector-icons/Ionicons';
import { RoundedButton } from '../../../components/parent';
import { shadowIOS } from '../../../style/parent/shadowIOS';

const AddChildStep3 = (props) => {
  const { email, phoneNum} = props.route.params;
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const [isMatch, setisMatch] = useState(true);
  const [isPassMatch, setisPassMatch] = useState(true);

  const [showPass, setShowPass] = useState(true);
  const [showRePass, setShowRePass] = useState(true);

  const handleChangePassword = (val) => {
    if (val.match(regex)) {
      setisMatch(true);
    } else setisMatch(false);
    setPassword(val);
  };
  const handleShowPass = () => {
    setShowPass(!showPass);
  };
  const handleChangeRePassword = (val) => {
    if (val === password) {
      setisPassMatch(true);
    } else setisPassMatch(false);
    setRePassword(val);
  };
  const handleShowRePass = () => {
    setShowRePass(!showRePass);
  };

  const handleNext = () => {
    if (rePassword === password && password.match(regex)) {
      props.navigation.navigate('AddChildStep4', {
        password: password,
        email: email,
        phoneNum: phoneNum,
      });
    } else if (!password.match(regex)) {
      setisMatch(false);
    } else setisPassMatch(false);
  };

  const regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;

  const chara = /.{8,}/;
  const lowercase = /[a-z]{1,}/;
  const uppercase = /[A-Z]{1,}/;
  const digit = /\d{1,}/;
  const special = /[!@#$&]{1,}/;

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{ width: '100%' }} showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <Text style={styles.title}>Add Child</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.bigDash}></View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={styles.dash}></View>
            <View style={styles.dash}></View>
          </View>
          <View style={{ width: '90%', marginBottom: 30 }}>
            <View style={{ paddingVertical: 20 }}>
              <Text style={styles.atitle}>Secure Child's Account</Text>
              <Text style={{ fontSize: 16, color: '#0F6880' }}>
                Complete your child's account with a password.
              </Text>
            </View>
            <View style={{ width: '100%', paddingVertical: 10 }}>
              
              <View style={styles.textInput}>
                <TextInput
                  placeholder="Password"
                  style={{ flex: 1 }}
                  value={password}
                  onChangeText={(val) => handleChangePassword(val)}
                  secureTextEntry={showPass}
                />
                <TouchableOpacity onPress={handleShowPass}>
                  <MIcon
                    name={showPass ? 'eye-outline' : 'eye'}
                    color="#0F6880"
                    size={24}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ width: '100%' }}>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: password.match(chara)
                        ? '#0F8020'
                        : '#F44336',
                    }}></View>
                  <Text style={styles.textDesc}>At least 8 characters</Text>
                </View>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: password.match(lowercase)
                        ? '#0F8020'
                        : '#F44336',
                    }}></View>
                  <Text style={styles.textDesc}>Contain one lower case</Text>
                </View>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: password.match(uppercase)
                        ? '#0F8020'
                        : '#F44336',
                    }}></View>
                  <Text style={styles.textDesc}>Contain one upper case</Text>
                </View>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: password.match(digit)
                        ? '#0F8020'
                        : '#F44336',
                    }}></View>
                  <Text style={styles.textDesc}>Contain one digit</Text>
                </View>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: password.match(special)
                        ? '#0F8020'
                        : '#F44336',
                    }}></View>
                  <Text style={styles.textDesc}>
                    Contain one special character of one of
                  </Text>
                </View>
                <View style={styles.descCon}>
                  <View
                    style={{
                      ...styles.dot,
                      backgroundColor: 'rgba(0,0,0,0)',
                    }}></View>
                  <Text style={styles.textDesc}>the following (!@#$%)</Text>
                </View>

                <View style={{ ...styles.textInput, marginTop: 30 }}>
                  <TextInput
                    placeholder="Re-type Password"
                    style={{ flex: 1 }}
                    value={rePassword}
                    onChangeText={(val) => handleChangeRePassword(val)}
                    secureTextEntry={showRePass}
                  />
                  <TouchableOpacity onPress={handleShowRePass}>
                    <MIcon
                      name={showRePass ? 'eye-outline' : 'eye'}
                      color="#0F6880"
                      size={24}
                    />
                  </TouchableOpacity>
                </View>
                <Text style={{ color: 'red', width: '80%' }}>
                  {isPassMatch ? '' : '*Please Re-type same Password'}
                </Text>
              </View>
            </View>
          </View>
          <RoundedButton text="Next" onPress={handleNext} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AddChildStep3;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },
  header: {
    width: '100%',
    paddingVertical: 18,
    backgroundColor: '#FBFBFB',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
    marginBottom: 20,
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 18,
    color: '#0F6880',
  },
  textInput: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    fontSize: 18,
    marginVertical: 10,
    height:50,
    flexDirection: 'row',
    alignItems: 'center',
  },
  numberInput: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 18,
    marginVertical: 10,
    marginLeft: 10,
  },
  pickCountry: {
    backgroundColor: 'white',
    borderRadius: 10,
    ...shadowIOS,
    elevation: 2,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 18,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },

  descCon: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 10,
    backgroundColor: '#0F8020',
    marginRight: 10,
  },
  textDesc: { fontSize: 16, color: '#0F6880' },
});
