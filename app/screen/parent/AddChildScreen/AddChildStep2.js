import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import { RoundedButton } from '../../../components/parent';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import Snackbar from 'react-native-snackbar';
import PropTypes from 'prop-types';

const AddChildStep2 = (prop) => {
  const { otpNumber, email, phoneNum} = prop.route.params;
  const [value, setValue] = useState('');
  const [timeLeft, setTimeLeft] = useState(120);
  const [isDisabled, setIsDisabled] = useState(false);
  const [isTimeout, setIsTimeout] = useState(false);
  const [timeDecrease, setTimeDecrease] = useState(1);
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const CELL_COUNT = 6;

  useEffect(() => {
    if (!timeLeft) {
      setIsTimeout(true);
      return;
    }
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - timeDecrease);
    }, 1000);
    return () => clearInterval(intervalId);
  }, [timeLeft, timeDecrease]);

  const handleResendOTP = () => {
    setTimeLeft(120);
  };

  const handleNext = () => {
    if (otpNumber == value) {
      prop.navigation.navigate('AddChildStep3', {
        email: email,
        phoneNum: phoneNum,
      });
    } else {
      Snackbar.show({
        text: 'OTP number is incorrect.',
        duration: Snackbar.LENGTH_INDEFINITE,
        backgroundColor: 'red',
        action: {
          text: 'CLOSE',
          textColor: 'white',
          onPress: () => true,
        },
      });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          width: '100%',
          paddingVertical: 18,
          backgroundColor: '#FBFBFB',
        }}>
        <Text style={styles.title}>Add Child</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.bigDash}></View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
          <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
          <View style={styles.dash}></View>
          <View style={styles.dash}></View>
          <View style={styles.dash}></View>
        </View>
        <View style={{ width: '90%', marginBottom: 80 }}>
          <View style={{ paddingVertical: 20 }}>
            <Text style={styles.atitle}>Verify Account</Text>
            <Text style={{ fontSize: 16, color: '#0F6880' }}>
              Please enter verification code that has been sent to your child's
              email address.
            </Text>
          </View>

          <View
            style={{ width: '100%', paddingVertical: 20, paddingBottom: 40 }}>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFieldRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({ index, symbol, isFocused }) => (
                <View
                  onLayout={getCellOnLayoutHandler(index)}
                  key={index}
                  style={[styles.cellRoot, isFocused && styles.focusCell]}>
                  <Text style={styles.cellText}>
                    {symbol || (isFocused ? <Cursor /> : null)}
                  </Text>
                </View>
              )}
            />
          </View>
          <Text
            style={{
              ...styles.textInputTitle,
              marginBottom: 10,
              alignSelf: 'center',
            }}>
            Verification Code expires in 2 minutes
          </Text>
          <Text
            style={{
              ...styles.textInputTitle,
              alignSelf: 'center',
              fontSize: 30,
            }}>
            {Math.floor(timeLeft / 60)
              .toString()
              .padStart(2, '0')}
            :
            {Math.floor(timeLeft % 60)
              .toString()
              .padStart(2, '0')}
          </Text>
        </View>

        {isDisabled ? (
          <RoundedButton
            text="Next"
            onPress={() => {}}
            backgroundColor="#919191"
          />
        ) : (
          <RoundedButton text="Next" onPress={handleNext} />
        )}
        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10,
            flexDirection: 'row',
          }}
          onPress={handleResendOTP}>
          <MIcon name="replay" size={24} color="#0F6880" />
          <Text
            style={{
              ...styles.textInputTitle,
              alignSelf: 'center',
              fontSize: 14,
              fontWeight: 'bold',
            }}>
            Re-send verification code
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default AddChildStep2;

AddChildStep2.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 18,
    color: '#0F6880',
  },
  textInput: {
    backgroundColor: 'blue',
    alignSelf: 'center',
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 30,
    marginVertical: 10,
  },
  root: { padding: 20, minHeight: 300 },
  codeFieldRoot: {
    marginTop: 20,
    width: 280,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cellRoot: {
    width: 30,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  cellText: {
    color: '#1DC1E6',
    fontSize: 32,
    textAlign: 'center',
  },
  focusCell: {
    borderBottomColor: '#007AFF',
    borderBottomWidth: 2,
  },
});
