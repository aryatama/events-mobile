import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import { LoadingScreen, RoundedButton } from '../../../components/parent';
import { postOnBoardingSendEmailOTP } from '../../../api/parent';
import Snackbar from 'react-native-snackbar';
import { shadowIOS } from '../../../style/parent/shadowIOS';

const AddChildStep1 = (props) => {
  const [email, setEmail] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [countryCode, setCountryCode] = useState('SG');
  const [countryNum, setCountryNum] = useState('+65-');
  const [isVisible, setVisible] = useState(false);
  const mutationPostOnBoardingSendEmailOTP = postOnBoardingSendEmailOTP();

  const handleChangeEmail = (val) => {
    setEmail(val);
  };

  const handlePhoneNum = (val) => {
    setPhoneNum(val);
  };

  const onSelect = (country) => {
    console.log('object', country);
    setCountryCode(country.cca2);
    setCountryNum("+" + country.callingCode[0] + "-");
  };

  if (mutationPostOnBoardingSendEmailOTP.isLoading) {
    return <LoadingScreen />
  }

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          width: '100%',
          paddingVertical: 18,
          backgroundColor: '#FBFBFB',
        }}>
        <Text style={styles.title}>Add Child</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.bigDash}></View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
          <View style={styles.dash}></View>
          <View style={styles.dash}></View>
          <View style={styles.dash}></View>
          <View style={styles.dash}></View>
        </View>
        <View style={{ width: '90%', marginBottom: 80 }}>
          <View style={{ paddingVertical: 20 }}>
            <Text style={styles.atitle}>Create Child Account</Text>
          </View>

          <View style={{ width: '100%', paddingVertical: 10 }}>
            <Text style={{ ...styles.textInputTitle, marginBottom: 10 }}>
              Enter your child's details below
            </Text>
            <TextInput
              placeholder="Child's email"
              style={styles.textInput}
              value={email}
              onChangeText={(val) => handleChangeEmail(val)}
            />

            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity style={styles.pickCountry}
                onPress={()=> {
                    setVisible(true)
                  }}>
                <CountryPicker
                  modalProps={{ visible: isVisible }}
                  onClose={()=> setVisible(false)}
                  onOpen={()=> setVisible(true)}
                  {...{
                    countryCode,
                    withFilter: true,
                    withFlag: false,
                    withCountryNameButton: false,
                    onSelect,
                  }}
                  visible={false}
                />
                <MIcon size={20} color="black" name="expand-more" />
              </TouchableOpacity>
              <TextInput
                placeholder="Phone Number"
                style={styles.numberInput}
                value={phoneNum}
                onChangeText={(val) => handlePhoneNum(val)}
                keyboardType="number-pad"
              />
            </View>
          </View>
        </View>
        <RoundedButton
          text="Next"
          onPress={() => {
            mutationPostOnBoardingSendEmailOTP.mutate(
              {
                email: email,
              },
              {
                onSuccess: (message) => {
                  Snackbar.show({
                    text: message.data.message,
                    duration: Snackbar.LENGTH_LONG,
                  });
                  return props.navigation.navigate('AddChildStep2', {
                    otpNumber: message.data.details.otpNumber,
                    email: email,
                    phoneNum: countryNum + phoneNum,
                  });
                },
                onError: (error) =>
                  Snackbar.show({
                    text: error.message,
                    duration: Snackbar.LENGTH_INDEFINITE,
                    backgroundColor: 'red',
                    action: {
                      text: 'CLOSE',
                      textColor: 'white',
                      onPress: () => true,
                    },
                  }),
              },
            );
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default AddChildStep1;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 18,
    color: '#0F6880',
  },
  textInput: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    fontSize: 18,
    marginVertical: 10,
  },
  numberInput: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    fontSize: 18,
    marginVertical: 10,
    marginLeft: 10,
  },
  pickCountry: {
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 18,
    height: 50,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
