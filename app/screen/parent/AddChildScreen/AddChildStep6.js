import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native';

const AddChildStep6 = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          width: '100%',
          paddingVertical: 18,
          backgroundColor: '#FBFBFB',
        }}>
        <Text style={styles.title}>Add Child</Text>
      </View>
      <View style={styles.body}>
        <View style={{ width: '90%', marginBottom: 30 }}>
          <View
            style={{
              paddingVertical: 20,
              width: '100%',
              alignItems: 'center',
            }}>
            <Text style={styles.atitle}>Congratulations</Text>
            <Text style={{ fontSize: 14, color: '#0F6880' }}>
              Your Child's account is ready
            </Text>
            <Image
              style={{ width: 200, height: 240 }}
              resizeMode="contain"
              source={require('../../../../assets/images/parent/addcardfinish.png')}
            />
            <Text style={{ fontSize: 14, color: '#0F6880' }}>
              Your child's card is on it's way to your address.
            </Text>
            <Text style={{ fontSize: 14, color: '#0F6880' }}>
              Please activate the card once you receive it.
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default AddChildStep6;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 16,
    color: '#0F6880',
  },
});
