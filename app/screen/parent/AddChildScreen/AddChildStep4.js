import React, { useState, useContext } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Switch,
  TouchableOpacity
} from 'react-native';
import CountryPicker, { CountryModalProvider } from 'react-native-country-picker-modal';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import { RoundedButton } from '../../../components/parent';
import { shadowIOS } from '../../../style/parent/shadowIOS';
import parentContext from '../../../context/parent/parentContext';
import Snackbar from 'react-native-snackbar';

const AddChildStep4 = (props) => {
  const { password, email, phoneNum} = props.route.params;
  const [fullName, setFullName] = useState('');
  const [year, setYear] = useState('');
  const [country, setCountry] = useState('');
  const [countryCode, setCountryCode] = useState('');
  const [streetName, setStreetName] = useState('');
  const [postalNumber, setPostalNumber] = useState('');
  const [unitNo, setunitNo] = useState('');
  const [isEnabled, setIsEnabled] = useState(false);
  const [isVisible, setVisible] = useState(false);

  const handleChangeEmail = (val) => {
   
    setFullName(val);
  };

  const handleChangeYear = (val) => {
    if(val.length > 4){
      return
    }
    setYear(val);
  };

  const onSelect = (country) => {
    console.log(country.cca2, country.name)
    setCountryCode(country.cca2);
    setCountry(country.name);
  };
  const handleChangeStreetName = (val) => {
    setStreetName(val);
  };

  const handleChangeUnitNo = (val) => {
    setunitNo(val);
  };
  const handleChangePostalNumber = (val) => {
    setPostalNumber(val);
  };

  const ParentContext = useContext(parentContext);

  const handleSameAddres = () => {
    setIsEnabled(!isEnabled);
    if (!isEnabled) {
      setCountryCode(ParentContext.parent.address.countryCode);
      setCountry(ParentContext.parent.address.country);
      setStreetName(ParentContext.parent.address.address_1);
      setunitNo(ParentContext.parent.address.address_2);
      setPostalNumber(ParentContext.parent.address.postalCode);
    } else {
      setCountryCode('');
      setCountry('');
      setPostalNumber('');
      setStreetName('');
      setunitNo('');
    }
  };

  const snackbar = (text) =>
    Snackbar.show({
      text: 'Please Enter ' + text,
      duration: 4000,
      backgroundColor: 'red',
      action: {
        text: 'CLOSE',
        textColor: 'white',
        onPress: () => true,
      },
    });

  const handleSubmitButton = () => {
    if (fullName.length === 0) {
      return snackbar('Your Full Name');
    } else if (year.length === 0) {
      return snackbar('Your Year Of Birth');
    } else if (country.length === 0) {
      return snackbar("Your Child's Country");
    } else if (streetName.length === 0) {
      return snackbar('Your Street Name');
    } else if (postalNumber.length === 0) {
      return snackbar('Your Postal Number');
    } else if (unitNo.length === 0) {
      return snackbar('Your Unit No');
    }
    props.navigation.navigate('AddChildStep5', {
      email,
      phoneNum,
      password,
      fullName,
      year,
      isEnabled,
      countryCode,
      country,
      streetName,
      unitNo,
      postalNumber,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={{ width: '100%' }}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            width: '100%',
            paddingVertical: 18,
            backgroundColor: '#FBFBFB',
          }}>
          <Text style={styles.title}>Add Child</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.bigDash}></View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={{ ...styles.dash, backgroundColor: '#1dc1e6' }}></View>
            <View style={styles.dash}></View>
          </View>
          <View style={{ width: '90%', marginBottom: 30 }}>
            <View style={{ paddingVertical: 20 }}>
              <Text style={styles.atitle}>Setup Your Child's Profile</Text>
              <Text style={{ fontSize: 14, color: '#0F6880' }}>
                Provide your child's details to personalise their card.
              </Text>
            </View>

            <View style={{ width: '100%', paddingVertical: 10 }}>
              <Text style={{ ...styles.textInputTitle }}>
                Child's Full Name
              </Text>
              <TextInput
                placeholder="..."
                style={styles.textInputName}
                value={fullName}
                onChangeText={(val) => handleChangeEmail(val)}
              />
              <Text style={{ ...styles.textInputTitle, marginTop: 10 }}>
                Child's Year Of Birth
              </Text>
              <TextInput
                placeholder="Year Of Birth"
                style={styles.textInput}
                value={year}
                onChangeText={(val) => handleChangeYear(val)}
                keyboardType="number-pad"
              />

              <View style={{ paddingVertical: 20 }}>
                <Text style={styles.atitle}>Where can we send your card?</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                }}>
                <Text style={styles.textInputTitle}>
                  Same address as parents
                </Text>
                <Switch
                  trackColor={{ false: '#eaf7fe', true: '#4fd262' }}
                  thumbColor={'white'}
                  ios_backgroundColor="#eaf7fe"
                  onValueChange={() => handleSameAddres(isEnabled)}
                  value={isEnabled}
                />
              </View>

              <View style={{ width: '100%' }}>
                {isEnabled == false ?
                 <TouchableOpacity style={styles.pickCountry}
                    onPress={()=> {
                      setVisible(true)
                    }}>
                  <CountryPicker
                  style={{width: 'auto'}}
                  placeholder="Select your country"
                  modalProps={{ visible: isVisible }}
                  onClose={()=> setVisible(false)}
                  onOpen={()=> setVisible(true)}
                    {...{
                      countryCode,
                      withFilter: true,
                      withCountryNameButton: true,
                      withFlagButton: false,
                      withFlag: false,
                      onSelect,
                    }}

                  /><Text style={{marginLeft: 'auto'}} >
                    <MIcon size={20} color="black" name="expand-more" />
                  </Text>
                </TouchableOpacity> :
                <TextInput
                  placeholder="Country"
                  style={isEnabled ? styles.textInputFalse : styles.textInput}
                  placeholderTextColor={isEnabled ? 'white' : '#999'}
                  editable={!isEnabled}
                  value={country}
                  onChangeText={(val) => handleChangeStreetName(val)}
                />
                }
                <Text style={styles.requiredText}>(required)</Text>
                <TextInput
                  placeholder="Street Name, Block Number, Building Name"
                  style={isEnabled ? styles.textInputFalse : styles.textInput}
                  placeholderTextColor={isEnabled ? 'white' : '#999'}
                  editable={!isEnabled}
                  value={streetName}
                  onChangeText={(val) => handleChangeStreetName(val)}
                />
                <Text style={styles.requiredText}>(required)</Text>

                <TextInput
                  placeholder="Unit No."
                  style={isEnabled ? styles.textInputFalse : styles.textInput}
                  placeholderTextColor={isEnabled ? 'white' : '#999'}
                  editable={!isEnabled}
                  value={unitNo}
                  onChangeText={(val) => handleChangeUnitNo(val)}
                  keyboardType="number-pad"
                />
                <Text style={styles.requiredText}>(required)</Text>

                <TextInput
                  placeholder="Postal Number"
                  style={isEnabled ? styles.textInputFalse : styles.textInput}
                  placeholderTextColor={isEnabled ? 'white' : '#999'}
                  value={postalNumber}
                  editable={!isEnabled}
                  onChangeText={(val) => handleChangePostalNumber(val)}
                  keyboardType="number-pad"
                />
                <Text style={styles.requiredText}>(required)</Text>
              </View>
            </View>
          </View>
          <RoundedButton text="Submit" onPress={handleSubmitButton} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AddChildStep4;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },
  requiredText: {
    fontSize: 12,
    color: 'grey',
    alignSelf: 'flex-end',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputTitle: {
    fontSize: 16,
    color: '#0F6880',
  },
  textInput: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    color: 'black',
    fontSize: 16,
    marginVertical: 10,
  },
  textInputFalse: {
    backgroundColor: '#D4D4D4',
    color: 'white',
    width: '100%',
    borderRadius: 10,
    ...shadowIOS,
    elevation: 2,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 16,
    marginVertical: 10,
  },
  numberInput: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 10,
    ...shadowIOS,
    elevation: 2,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 18,
    marginVertical: 10,
    marginLeft: 10,
  },
  pickCountry: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 10,
    ...shadowIOS,
    elevation: 2,
    paddingHorizontal: 18,
    paddingVertical: 14,
    fontSize: 18,
    marginVertical: 10,
    flexDirection: 'row',
  },

  textInputName: {
    width: '100%',
    paddingHorizontal: 18,
    fontSize: 18,
    marginVertical: 10,
    height:50,
    borderBottomWidth: 1,
    borderColor: 'grey',
  },
});
