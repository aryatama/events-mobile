import React, { useState, useContext, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Pressable,
} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import parentContext from '../../context/parent/parentContext';
import {
  TaskCard,
  ErrorAPIHandler,
  LoadingScreen,
} from '../../components/parent';
import Carousel from 'react-native-snap-carousel';

const TasksScreen = (props) => {
  const [type, setType] = useState('0');
  const [selectedChild, setselectedChild] = useState('all');

  useEffect(() => {
    setselectedChild('all');
  }, []);

  const ParentContext = useContext(parentContext);

  const renderItem = ({ item }) => (
    <Pressable onPress={() => handleNavigateTaskDetails(item)}>
      <TaskCard
        title={item.taskName}
        reward={item.rewardAmount}
        rewardName={item.reward}
        type={item.type}
        dueDate={item.dueDate}
        monetaryReward={item.monetaryReward}
        status={item.status}
      />
    </Pressable>
  );

  const handleNavigateTaskDetails = (item) => {
    props.navigation.navigate('TaskDetailsScreen', {
      childId: item.childId,
      taskId: item.taskId,
    });
  };

  const handleAddTask = () => {
    if (ParentContext.childs.length > 0) {
      return props.navigation.navigate('AddTaskScreen', {
        childId:
          selectedChild === 'all'
            ? ParentContext.childs[0].childId
            : selectedChild,
      });
    } else if (ParentContext.parent.subscription.status === 0) {
      props.navigation.navigate('ManageSubscriptionScreen');
    } else {
      props.navigation.navigate('AddChildStep1');
    }
  };

  const handleChangeTask = (type) => {
    setType(type);
  };
  const handleChangeUser = (index) => {
    setselectedChild(dataChilds[index].childId);
  };

  const filteredData = () => {
    if (type === '0' && selectedChild === 'all') {
      console.log(type, selectedChild);
      return ParentContext.tasks
        .filter((item) => item.status === type || item.status === '1')
        .sort((item) => item.status === type);
    } else if (selectedChild === 'all') {
      return ParentContext.tasks.filter((item) => item.status === type);
    } else if (type === '0') {
      console.log(type, selectedChild, 'review');
      return ParentContext.tasks
        .filter(
          (item) =>
            (item.status === type && item.childId === selectedChild) ||
            (item.status === '1' && item.childId === selectedChild),
        )
        .sort((item) => item.status === type);
    } else {
      return ParentContext.tasks.filter(
        (item) => item.status === type && item.childId === selectedChild,
      );
    }
  };

  const all = {
    firstName: 'All',
    childId: 'all',
  };

  const dataChilds = [all, ...ParentContext.childs];

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}>
        <View style={styles.inScroll}>
          <View
            style={{
              width: '100%',
              backgroundColor: '#FBFBFB',
              alignItems: 'center',
            }}>
            <View style={{ width: '90%', marginVertical: 18 }}>
              <Text style={styles.title}>Tasks</Text>
            </View>

            <View style={styles.menuCon}>
              <TouchableOpacity
                style={{
                  backgroundColor: type === '2' ? '#24C1E7' : 'rgba(0,0,0,0)',
                  ...styles.selectType,
                  borderTopLeftRadius: 8,
                  borderBottomLeftRadius: 8,
                  borderRightWidth: 0,
                }}
                onPress={() => handleChangeTask('2')}>
                <Text style={{ color: type === '2' ? 'white' : '#24C1E7' }}>
                  Review
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: type === '0' ? '#24C1E7' : 'rgba(0,0,0,0)',
                  ...styles.selectType,
                }}
                onPress={() => handleChangeTask('0')}>
                <Text
                  style={{
                    color: type === '0' ? 'white' : '#24C1E7',
                  }}>
                  Pending
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: type === '3' ? '#24C1E7' : 'rgba(0,0,0,0)',
                  ...styles.selectType,
                  borderTopRightRadius: 8,
                  borderBottomRightRadius: 8,
                  borderLeftWidth: 0,
                }}
                onPress={() => handleChangeTask('3')}>
                <Text
                  style={{
                    color: type === '3' ? 'white' : '#24C1E7',
                  }}>
                  Completed
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          {ParentContext.childs.length > 0 ? (
            <View style={styles.body}>
              <View
                style={{
                  width: '100%',
                  paddingVertical: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Carousel
                  data={dataChilds}
                  renderItem={({ item }) => {
                    return (
                      <View
                        style={{
                          ...styles.childsMenu,
                          width: '100%',
                          borderBottomWidth:
                            selectedChild === item.childId ? 3 : 0,
                        }}
                        key={item.childId}>
                        <Text style={styles.childsTextMenu}>
                          {item.firstName}
                        </Text>
                      </View>
                    );
                  }}
                  contentContainerCustomStyle={{ alignItems: 'center' }}
                  sliderWidth={(deviceWidth * 8) / 10}
                  itemWidth={(deviceWidth * 4) / 10}
                  onSnapToItem={(index) => handleChangeUser(index)}
                />
              </View>

              <View style={styles.typeIndicator}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={styles.colorInd}></View>
                  <Text style={{ fontSize: 14 }}>Monetary Reward</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View
                    style={{
                      ...styles.colorInd,
                      backgroundColor: '#1DC1E6',
                    }}></View>
                  <Text style={{ fontSize: 14 }}>Non-Monetary Reward</Text>
                </View>
              </View>

              <View style={styles.taskCon}>
                <FlatList
                  data={filteredData()}
                  renderItem={renderItem}
                  keyExtractor={(item) => item.tasksId}
                />
              </View>

              <Text style={styles.noTaskMore}>
                {ParentContext.tasks.length === 0 ? 'No task to show' : true}
              </Text>
            </View>
          ) : (
            <View style={styles.body}>
              <View
                style={{
                  width: '100%',
                  height: 300,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={styles.noTaskMore}>
                  You have no registered child.
                </Text>
                <Text style={styles.noTaskMore}>
                  Add your child to start creating a task
                </Text>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
      <View style={{ width: '70%', position: 'absolute' }}>
        <TouchableOpacity
          style={{
            ...styles.butContainer,
            backgroundColor: '#0F6880',
          }}
          onPress={handleAddTask}>
          <MIcon name="add-circle-outline" color="white" size={30} />
          <Text style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>
            {ParentContext.childs.length > 0
              ? 'Add Task'
              : ParentContext.parent.subscription.status === 0
              ? 'Subscribe'
              : 'Add Child'}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default TasksScreen;

const deviceHeight = Dimensions.get('window').height;
let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  childsTextMenu: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0F6880',
    textAlign: 'center',
  },
  childsMenu: {
    paddingVertical: 5,
    borderBottomWidth: 3,
    borderColor: '#0F6880',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  noTaskMore: {
    alignSelf: 'center',
    color: '#1DC1E6',
    fontSize: 16,
    fontWeight: 'normal',
    marginTop: 0,
  },
  butContainer: {
    flexDirection: 'row',
    width: '80%',
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: '#0F6880',
    padding: 12,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  taskCon: {
    width: '90%',
    minHeight: (deviceHeight * 2) / 10,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  typeIndicator: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  colorInd: {
    width: 24,
    height: 24,
    borderRadius: 4,
    backgroundColor: '#0F8020',
    marginRight: 6,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },
  scrollview: {
    width: '100%',
  },
  inScroll: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  textInputCont: {
    width: '90%',
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: '#EAEBED',
    borderRadius: 14,
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    marginRight: 4,
    fontSize: 20,
  },
  menuCon: {
    width: '90%',
    marginVertical: 30,
    flexDirection: 'row',
    backgroundColor: '#FBFBFB',
  },
  selectType: {
    flex: 1,
    padding: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'grey',
  },
  body: {
    width: '100%',
    paddingVertical: 10,
  },
});
