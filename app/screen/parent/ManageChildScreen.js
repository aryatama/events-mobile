import React, { useEffect, useState, useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  LimitProgress,
  MenuCard,
  OnlyZimbleCard,
  RecentTransaction,
  ErrorAPIHandler,
  AllTransaction,
  LoadingScreen,
  RoundedButton,
} from '../../components/parent';
import { Dimensions } from 'react-native';
import {
  VictoryBar,
  VictoryChart,
  VictoryTheme,
  VictoryAxis,
} from 'victory-native';
import PropTypes from 'prop-types';
import parentContext from '../../context/parent/parentContext';
import Carousel from 'react-native-snap-carousel';
import currency from '../../utils/parent/currencyFormat';
import moment from 'moment';
import { getChildInsights, getChildSummary } from '../../api/parent';
import decodedImageURI from '../../utils/parent/decodedImageURI';

const screenWidth = Dimensions.get('window').width;

const ManageChildScreen = (props) => {
  const { childId, isTransaction } = props.route.params;

  const [selectedChild, setSelectedChild] = useState(childId);
  const [type, setType] = useState('summary');
  const [startWeek, setStartWeek] = useState();
  const [endWeek, setEndWeek] = useState();
  const handleChangeTab = (type) => {
    setType(type);
  };

  useEffect(() => {
    if (isTransaction) [setType('transactions')];
    setStartWeek(moment().startOf('week').format('YYYYMMDDTHHmmss'));
    setEndWeek(moment().endOf('week').format('YYYYMMDDTHHmmss'));
  }, []);
  const ParentContext = useContext(parentContext);

  let currentChild = ParentContext.childs.find(
    (child) => child.childId === selectedChild,
  );

  let dataChart = [
    { quarter: 'Sun', earnings: 0 },
    { quarter: 'Mon', earnings: 0 },
    { quarter: 'Tues', earnings: 0 },
    { quarter: 'Wed', earnings: 0 },
    { quarter: 'Thu', earnings: 0 },
    { quarter: 'Fri', earnings: 0 },
    { quarter: 'Sat', earnings: 0 },
  ];

  if (!currentChild) {
    // <Text>Loading...</Text>;
    return <LoadingScreen />;
  }

  const handleNavigateActivateChild = () => {
    if (ParentContext.childs.length !== 0) {
      let child = ParentContext.childs.find(
        (child) => child.childId === selectedChild,
      );
      props.navigation.navigate('ActivateCardScreen', {
        childId: child.childId,
        firstName: child.firstName,
        profilePicture: child.profilePicture,
      });
    }
  };

  const {
    isLoading: isLoadingGetChildInsights,
    error: errorGetChildInsights,
    data: dataGetChildInsights,
  } = getChildInsights(selectedChild);

  const {
    isLoading: isLoadingGetChildSummary,
    error: errorGetChildSummary,
    data: dataGetChildSummary,
  } = getChildSummary(selectedChild, startWeek, endWeek);

  if (isLoadingGetChildInsights || isLoadingGetChildSummary) {
    return <LoadingScreen />;
  }

  if (errorGetChildInsights || errorGetChildSummary) {
    console.log('990', errorGetChildSummary);
    return (
      <ErrorAPIHandler
        message={
          (errorGetChildInsights && errorGetChildInsights.message) ||
          (errorGetChildSummary && errorGetChildSummary.message)
        }
      />
    );
  }

  dataGetChildSummary.data.details.summaryData.map((transaction) => {
    if (transaction.type == 'purchase') {
      switch (moment(transaction.createdAt).day()) {
        case 0:
          dataChart[0].earnings += transaction.amount;
          break;
        case 1:
          dataChart[1].earnings += transaction.amount;
          break;
        case 2:
          dataChart[2].earnings += transaction.amount;
          break;
        case 3:
          dataChart[3].earnings += transaction.amount;
          break;
        case 4:
          dataChart[4].earnings += transaction.amount;
          break;
        case 5:
          dataChart[5].earnings += transaction.amount;
          break;
        case 6:
          dataChart[6].earnings += transaction.amount;
          break;
        default:
      }
    }
  });

  return (
    <View style={styles.container}>
      <ScrollView
        style={{ width: '100%' }}
        nestedScrollEnabled={true}
        showsVerticalScrollIndicator={false}>
        <View style={{ marginTop: 20, alignItems: 'center' }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{
                width: 70,
                height: 70,
                aspectRatio: 1,
                borderRadius: 100,
              }}
              resizeMode="cover"
              source={{ uri: decodedImageURI(currentChild?.profilePicture) }}
            />
            <Text
              style={{
                ...styles.textColor,
                fontSize: 20,
                marginBottom: 20,
                fontWeight: 'bold',
                textAlign:'center'
              }}>
              {currentChild?.firstName}
            </Text>
          </View>

          <Carousel
            data={ParentContext.childs}
            firstItem={ParentContext.childs.findIndex(
              (child) => child.childId === selectedChild,
            )}
            renderItem={({ item, index }) => {
              return (
                <View
                  key={index}
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <OnlyZimbleCard
                    cardActiveStatus={item.cardActiveStatus}
                    temporaryCardStatus={item.temporaryCardStatus}
                    firstName={item.firstName}
                    cardColor={item.cardColor}
                    matchMoveWalletCard={item.matchMoveWalletCard}
                    onPressActivate={() => {
                      props.navigation.navigate('ActivateCardScreen', {
                        childId: item.childId,
                        firstName: item.firstName,
                        profilePicture: item.profilePicture,
                      });
                    }}
                    onPressUnlock={() => {
                      props.navigation.navigate('LockScreen', {
                        childId: item.childId,
                        type: 'unlock',
                      });
                    }}
                  />
                </View>
              );
            }}
            sliderWidth={(screenWidth * 9) / 10}
            itemWidth={(screenWidth * 7) / 10}
            sliderHeight={(screenWidth * 51) / 100}
            onSnapToItem={(index) =>
              setSelectedChild(ParentContext.childs[index].childId)
            }
          />
        </View>

        {currentChild?.cardActiveStatus === '1' ? (
          <>
            <View style={styles.balanceContainer}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text style={{ ...styles.textColor, fontSize: 16 }}>
                  Balance
                </Text>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 40,
                    fontWeight: 'bold',
                  }}>
                  {currentChild &&
                    currency(parseFloat(currentChild.matchMoveWallet))}
                </Text>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text style={{ ...styles.textColor, fontSize: 16 }}>
                  Spent this week
                </Text>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 40,
                    fontWeight: 'bold',
                  }}>
                  {(function () {
                    let amountTotal = 0;

                    if (
                      dataGetChildSummary.data.details.summaryData.length > 0
                    ) {
                      dataGetChildSummary.data.details.summaryData.map((data) =>
                        data.type === 'purchase'
                          ? (amountTotal += data.amount)
                          : true,
                      );

                      return currency(parseFloat(amountTotal));
                    } else {
                      return currency(0);
                    }
                  })()}
                </Text>
              </View>
            </View>

            <View style={styles.menuStyle}>
              <TouchableOpacity
                style={{ marginRight: 10 }}
                onPress={() =>
                  props.navigation.navigate('TopUpBalanceScreen', {
                    childId: selectedChild,
                  })
                }>
                <MenuCard
                  title="Top Up Balance"
                  iconName="wallet"
                  color="#1DA2C3"
                />
              </TouchableOpacity>

              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('AdjustLimitScreen', {
                      childId: selectedChild,
                    })
                  }>
                  <MenuCard
                    title="Adjust Limit"
                    iconName="warning"
                    color="#1DA2C3"
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('AddTaskScreen', {
                      childId: selectedChild,
                    })
                  }>
                  <MenuCard title="Add Task" iconName="task" color="#1DA2C3" />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('SeeGoalsScreen', {
                      childId: selectedChild,
                    })
                  }>
                  <MenuCard
                    title="See Goals"
                    iconName="target-board"
                    color="#1DA2C3"
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('WithdrawBalanceScreen', {
                      childId: selectedChild,
                    })
                  }>
                  <MenuCard
                    title="Withdraw Balance"
                    iconName="target-board"
                    color="#1DA2C3"
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('LockScreen', {
                      childId: selectedChild,

                      type:
                        currentChild?.temporaryCardStatus === '0'
                          ? 'unlock'
                          : 'lock',
                    })
                  }>
                  <MenuCard
                    title={
                      currentChild?.temporaryCardStatus === '0'
                        ? 'Unlock Account'
                        : 'Lock Account'
                    }
                    iconName="padlock"
                    color="#FF2725"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() =>
                    props.navigation.navigate('LockScreen', {
                      childId: selectedChild,
                      type: 'deactivate',
                    })
                  }>
                  <MenuCard
                    title="Deactivate Account"
                    iconName="trash-can"
                    color="black"
                  />
                </TouchableOpacity>
              </ScrollView>
            </View>

            <View style={styles.bodyStyle}>
              <View style={styles.menuCon}>
                <TouchableOpacity
                  style={{
                    backgroundColor:
                      type === 'summary' ? '#24C1E7' : 'rgba(0,0,0,0)',
                    ...styles.selectType,
                    borderTopLeftRadius: 8,
                    borderBottomLeftRadius: 8,
                    borderRightWidth: 0,
                  }}
                  onPress={() => handleChangeTab('summary')}>
                  <Text
                    style={{
                      color: type === 'summary' ? 'white' : '#24C1E7',
                    }}>
                    Summary
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor:
                      type === 'insights' ? '#24C1E7' : 'rgba(0,0,0,0)',
                    ...styles.selectType,
                  }}
                  onPress={() => handleChangeTab('insights')}>
                  <Text
                    style={{
                      color: type === 'insights' ? 'white' : '#24C1E7',
                    }}>
                    Insights
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor:
                      type === 'transactions' ? '#24C1E7' : 'rgba(0,0,0,0)',
                    ...styles.selectType,
                    borderTopRightRadius: 8,
                    borderBottomRightRadius: 8,
                    borderLeftWidth: 0,
                  }}
                  onPress={() =>
                    handleChangeTab('transactions', selectedChild)
                  }>
                  <Text
                    style={{
                      color: type === 'transactions' ? 'white' : '#24C1E7',
                    }}>
                    Transactions
                  </Text>
                </TouchableOpacity>
              </View>

              {type === 'summary' ? (
                <View style={{ width: '100%', marginVertical: 20 }}>
                  <View style={{ paddingHorizontal: 10, marginBottom: 10 }}>
                    <Text
                      style={{
                        ...styles.regularTextSaving,
                        fontWeight: 'normal',
                        fontSize: 20,
                      }}>
                      Total Weekly Expenses
                    </Text>
                    <Text style={{ ...styles.regularTextSaving, fontSize: 28 }}>
                      {(function () {
                        let amountTotal = 0;

                        if (
                          dataGetChildSummary.data.details.summaryData.length >
                          0
                        ) {
                          dataGetChildSummary.data.details.summaryData.map(
                            (data) =>
                              data.type === 'purchase'
                                ? (amountTotal += data.amount)
                                : true,
                          );

                          return currency(parseFloat(amountTotal));
                        } else {
                          return currency(0);
                        }
                      })()}
                    </Text>
                    <Text
                      style={{
                        ...styles.regularTextSaving,
                        fontSize: 16,
                      }}>
                      {moment(startWeek).format('DD MMM') +
                        ' - ' +
                        moment(endWeek).format('DD MMM')}
                    </Text>
                  </View>

                  {ParentContext.transaction.length !== 0 ? (
                    true
                  ) : (
                    <VictoryChart
                      theme={VictoryTheme.material}
                      maxDomain={{ y: 200 }}
                      domainPadding={{ x: 20 }}>
                      <VictoryAxis
                        style={{
                          tickLabels: {
                            fill: '#0F6880',
                          },
                          axis: { stroke: '#0F6880' },
                          ticks: { stroke: 'grey' },
                        }}
                      />
                      <VictoryAxis
                        dependentAxis
                        orientation="left"
                        style={{
                          tickLabels: { fill: '#0F6880' },
                          axis: { stroke: 'grey' },
                          ticks: { stroke: 'grey' },
                        }}
                      />
                      <VictoryBar
                        data={dataChart}
                        labels={({ datum }) => `${datum.earnings}`}
                        x="quarter"
                        y="earnings"
                        barRatio={0.8}
                        cornerRadius={{ top: 5 }}
                        style={{
                          data: {
                            fill: '#F44336',
                          },
                          labels: { color: '#F44336' },
                        }}
                      />
                    </VictoryChart>
                  )}
                  <RecentTransaction
                    childId={
                      !selectedChild
                        ? ParentContext.childs.length > 0
                          ? ParentContext.childs[0].child
                          : null
                        : selectedChild
                    }
                    fromDashboard={false}
                    onPressMore={() => handleChangeTab('transactions')}
                  />
                </View>
              ) : type === 'insights' ? (
                <View style={{ width: '100%' }}>
                  <View
                    style={{
                      width: '100%',
                      paddingVertical: 20,
                    }}>
                    <LimitProgress
                      title="Daily Spending Limit"
                      progress={
                        dataGetChildInsights.data.details.insightsData
                          .dailySpendLimit !== 0
                          ? (dataGetChildInsights.data.details.insightsData
                              .useDailySpendLimit /
                              dataGetChildInsights.data.details.insightsData
                                .dailySpendLimit) *
                            100
                          : 0
                      }
                    />
                    <LimitProgress
                      title="Weekly Spending Limit"
                      progress={
                        dataGetChildInsights.data.details.insightsData
                          .weeklySpendLimit !== 0
                          ? (dataGetChildInsights.data.details.insightsData
                              .useWeeklySpendLimit /
                              dataGetChildInsights.data.details.insightsData
                                .weeklySpendLimit) *
                            100
                          : 0
                      }
                    />
                    <LimitProgress
                      title="Fornightly Spending Limit"
                      progress={
                        dataGetChildInsights.data.details.insightsData
                          .fortnightlySpendLimit !== 0
                          ? (dataGetChildInsights.data.details.insightsData
                              .useFortnightlySpendLimit /
                              dataGetChildInsights.data.details.insightsData
                                .fortnightlySpendLimit) *
                            100
                          : 0
                      }
                    />
                    <LimitProgress
                      title="Monthly Spending Limit"
                      progress={
                        dataGetChildInsights.data.details.insightsData
                          .monthlySpendLimit !== 0
                          ? (dataGetChildInsights.data.details.insightsData
                              .useMonthlySpendLimit /
                              dataGetChildInsights.data.details.insightsData
                                .monthlySpendLimit) *
                            100
                          : 0
                      }
                    />
                  </View>
                </View>
              ) : (
                <View style={{ width: '100%', alignItems: 'center' }}>
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <AllTransaction
                      childId={
                        !selectedChild
                          ? ParentContext.childs.length > 0
                            ? ParentContext.childs[0].child
                            : null
                          : selectedChild
                      }
                      fromDashboard={false}
                    />
                  </View>
                </View>
              )}
            </View>
          </>
        ) : (
          <View>
            <RoundedButton
              onPress={handleNavigateActivateChild}
              text="Activate Child's Card"
              backgroundColor="#25C1E7"
            />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default ManageChildScreen;

ManageChildScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

const styles = StyleSheet.create({
  menuCon: {
    width: '100%',
    marginVertical: 30,
    flexDirection: 'row',
  },
  container: {
    backgroundColor: '#FBFBFB',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textColor: {
    color: '#0F6880',
  },
  selectType: {
    flex: 1,
    padding: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'grey',
  },
  imageProfile: {
    width: 70,
    height: 70,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  regularTextSaving: { color: '#0F6880', fontSize: 18, fontWeight: 'bold' },
  tabStyle: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    borderColor: '#1DC1E6',
  },
  bodyStyle: {
    backgroundColor: '#EAF7FE',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  menuStyle: {
    width: '100%',
    padding: 20,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  balanceContainer: {
    width: '100%',
    padding: 20,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
