import React, { useContext, useState, useEffect } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
} from 'react-native';
import {
  ConfirmModal,
  ConfirmModal2,
  LoadingScreen,
  RoundedButton,
  UserOption,
} from '../../components/parent';
import parentContext from '../../context/parent/parentContext';
import { postWithdrawChildBalance } from '../../api/parent';
import currency from '../../utils/parent/currencyFormat';
import decodedImageURI from '../../utils/parent/decodedImageURI';
import Snackbar from 'react-native-snackbar';

const WithdrawBalanceScreen = (props) => {
  const { childId } = props.route.params;
  const [currentChild, setCurrentChild] = useState();
  const [isSuccess, setIsSuccess] = useState(false);
  const [amount, setAmount] = useState('');
  const [reason, setReason] = useState('');
  const [showModal, setShowModal] = useState(false);
  const mutationPostWithdrawChildBalance = postWithdrawChildBalance();

  const ParentContext = useContext(parentContext);
  const [selectedChild, setselectedChild] = useState(childId);

  useEffect(() => {
    if (!currentChild) {
      let dataChild = ParentContext.childs.find(
        (child) => child.childId === selectedChild,
      );
      setCurrentChild(dataChild);
    }
  }, []);

  if (mutationPostWithdrawChildBalance.isLoading) {
    return <LoadingScreen />;

  }
  const handleConfirmButton = () => {
    if (amount.length !== 0) {
      setShowModal(true);
    } else
      Snackbar.show({
        text: 'Please Enter Amount To Withdraw',
        duration: 4000,
        backgroundColor: 'red',
        action: {
          text: 'CLOSE',
          textColor: 'white',
          onPress: () => true,
        },
      });
  };

  const handleWithdrawBalance = () => {

    setShowModal(false)
      mutationPostWithdrawChildBalance.mutate(
        {
          childId: selectedChild,
          amount: amount,
          message : reason,
        },
        {
          onSuccess: (message) => {
            setIsSuccess(true);
          },
          onError: (error) =>
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            }),
        },
      );
  };

  const handleBackToManageChild = () => {
    props.navigation.goBack();
  };

  if (!currentChild) return <LoadingScreen />;

  return (
    <View style={styles.container}>
      <ConfirmModal2
        type="toDelete"
        title="Withdraw Child’s Balance"
        desc="Do you want to proceed to withdraw this child’s balance"
        buttonCancel="Cancel"
        buttonConfirm="Proceed"
        isShow={showModal}
        onCancel={() => setShowModal(false)}
        onConfirm={handleWithdrawBalance}
      />
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <View style={styles.topTitle}>
          <View style={{ width: '90%', alignSelf: 'center' }}>
            <Text
              style={{ fontSize: 30, fontWeight: 'bold', color: '#0F6880' }}>
              Withdraw
            </Text>
            <Text
              style={{ fontSize: 30, fontWeight: 'bold', color: '#0F6880' }}>
              Child's Balance
            </Text>
          </View>
        </View>

        {isSuccess ? (
          <View style={styles.body}>
            <View style={styles.bigDash}></View>
            <Text
              style={{
                ...styles.titleField,
                fontSize: 26,
                fontWeight: 'bold',
              }}>
              {currency(parseFloat(amount))}
            </Text>
            <Text style={{ ...styles.titleField, fontSize: 16 }}>
              Has been withdrawn from your child’s account.
            </Text>
            <View style={{ alignItems: 'center', marginVertical: 40 }}>
              <Image
                style={styles.imageParent}
                resizeMode="cover"
                source={{
                  uri: decodedImageURI(currentChild.profilePicture),
                }}
              />
              <Text
                style={{
                  ...styles.titleField,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                {currentChild.firstName}
              </Text>
            </View>
            <RoundedButton
              onPress={handleBackToManageChild}
              text="Back To Manage Child"
            />
          </View>
        ) : (
          <View style={styles.body}>
            <View style={styles.bigDash}></View>
            <View style={styles.bodyContainer}>
              <UserOption
                childs={ParentContext.childs}
                onValueChange={(v) => {
                  setselectedChild(v.childId);
                }}
                selectedChild={selectedChild}
              />
            </View>
            <View style={{ width: '85%' }}>
              <Text style={styles.titleField}>Enter Amount To Withdraw</Text>
              <View style={styles.textInputCon}>
                <Text
                  style={{
                    ...styles.dolarSign,
                    color: amount ? '#0F6880' : '#7dafbf',
                  }}>
                  $
                </Text>
                <TextInput
                  style={styles.textInput}
                  placeholder={'0'}
                  placeholderTextColor="#7dafbf"
                  keyboardType="numeric"
                  value={amount}
                  onChangeText={(v) => setAmount(v)}
                />
              </View>

              <Text style={styles.titleField}>Withdrawal Reason</Text>

              <TextInput
                style={styles.textInputDesc}
                placeholder={'Type your withdrawal reason here'}
                placeholderTextColor="#7dafbf"
                value={reason}
                numberOfLines={4}
                onChangeText={(v) => setReason(v)}
                multiline={true}                
              />

              <RoundedButton onPress={handleConfirmButton} text="Confirm" />
            </View>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default WithdrawBalanceScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAF7FE',
  },
  textInputDesc: {
    marginBottom: 20,
    backgroundColor: 'white',
    padding: 10,
    color: 'black',
    borderRadius: 10,
    textAlignVertical: 'top',
    height:100,
  },
  scrollView: {
    width: '100%',
  },
  dolarSign: {
    marginLeft: 0,
    fontSize: 24,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  textInput: {
    color: '#0F6880',
    marginLeft: 0,
    fontSize: 24,
    height:50,
    flex: 1,
    fontWeight: 'bold',
  },
  titleField: {
    color: '#0F6880',
    fontSize: 18,
    marginVertical: 10,
  },
  textInputCon: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    borderBottomColor: 'rgba(0,0,0,0.4)',
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  topTitle: {
    alignSelf: 'center',
    width: '100%',
    backgroundColor: '#FBFBFB',
    paddingVertical: 24,
    justifyContent: 'center',
  },
  imageParent: {
    width: 80,
    height: 80,
    aspectRatio: 1,
    borderRadius: 100,
  },
  body: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  bodyContainer: {
    width: '90%',
  },
  bodyCon: { width: '100%', justifyContent: 'center', marginVertical: 10 },
  line: {
    width: '100%',
    borderBottomWidth: 1,
    marginVertical: 14,
    borderBottomColor: 'grey',
  },
  textColor: { color: '#0F6880' },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginBottom: 10,
  },
});
