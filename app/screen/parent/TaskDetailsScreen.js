import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
} from 'react-native';
import { getChildTaskById } from '../../api/parent';
import {
  ErrorAPIHandler,
  LoadingScreen,
  RoundedButton,
  TaskCard,
} from '../../components/parent';

import Video from 'react-native-video';

const TaskDetailsScreen = (props) => {
  const { childId, taskId } = props.route.params;

  const { isLoading, isError, error, data } = getChildTaskById(childId, taskId);

  if (isLoading) return <LoadingScreen />;

  if (isError) {
    return <ErrorAPIHandler message={error.message} />;
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={{
          width: '100%',
        }}
        showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <View style={{ width: '90%' }}>
            <TaskCard
              title={data.data.details.taskData.taskName}
              reward={data.data.details.taskData.rewardAmount}
              rewardName={data.data.details.taskData.reward}
              dueDate={data.data.details.taskData.dueDate}
              monetaryReward={data.data.details.taskData.monetaryReward}
            />
          </View>
        </View>
        <View style={styles.body}>
          <Text style={styles.descTitle}>Task Description</Text>
          <Text style={styles.desc}>
            {data.data.details.taskData.taskDescription}
          </Text>
          <Text style={{...styles.descTitle, marginBottom: 20,}}>Photos & Videos</Text>
          {data.data.details.taskData.imageUrls.length !== 0 ? (
            <Image
              style={{
                width: '100%',
                aspectRatio: 1,
                resizeMode: 'contain',
                marginBottom: 30,
              }}
              source={{
                uri: data.data.details.taskData.imageUrls[0],
              }}
            />
          ) : (
            true
          )}

          {data.data.details.taskData.videoUrls.length !== 0 ? (
            <View
              style={{
                width: '100%',
                height: 300,
                backgroundColor: 'black',
              }}>
              <Video
                source={{
                  uri: data.data.details.taskData.videoUrls[0],
                }}
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                }}
                fullscreen={false}
                ignoreSilentSwitch="ignore"
                controls={true}
                resizeMode="contain"
                onError={() => console.log('ERROR')}
              />
            </View>
          ) : (
            true
          )}

          {data.data.details.taskData.imageUrls.length === 0 &&
          data.data.details.taskData.videoUrls.length === 0 ? (
            <View
              style={{
                width: '100%',
                height: 300,
                backgroundColor: '#C4C4C4',
                justifyContent: 'center',
                marginVertical: 10,
                alignItems: 'center',
              }}>
              <Text style={styles.descTitle}>No Photos & Videos</Text>
            </View>
          ) : (
            true
          )}
          <RoundedButton
            onPress={() => {}}
            text="Delete Task"
            backgroundColor="#F44336"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TaskDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },
  desc: {
    marginTop: 8,
    marginBottom: 20,
  },
  descTitle: {
    fontSize: 20,
    color: '#0F6880',
    fontWeight: 'bold',
  },
  header: {
    backgroundColor: '#FBFBFB',
    width: '100%',
    paddingVertical: 20,
    alignItems: 'center',
  },
  body: {
    width: '90%',
    paddingVertical: 20,
    alignSelf: 'center',
  },
});
