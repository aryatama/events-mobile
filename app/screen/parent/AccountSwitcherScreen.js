import React, { useContext, useRef, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  SafeAreaView,
  DevSettings,
} from 'react-native';
import { shadowIOS } from '../../style/parent/shadowIOS';
import { AccountCard, LoadingScreen } from '../../components/parent';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import userContext from '../../context/user/userContext';
import Snackbar from 'react-native-snackbar';

const AccountSwitcherScreen = () => {

  const UserContext = useContext(userContext);
  return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.scrollview}
          showsVerticalScrollIndicator={false}>
          <View style={styles.inScroll}>

            <View style={styles.bodyCon}>
             <View style={{ ...styles.bodyCon, width: '90%', marginVertical: 30 }}>
              <View style={styles.containerInside}>
                 <TouchableOpacity
                         onPress={() => {
                            console.log("SET API_KEY TO eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmODMwNzVlYWU1YmUyMjFhYzlkOTk4MiIsImlhdCI6MTYxNDkxNDYxNSwiZXhwIjoxNjE1NTE5NDE1fQ.09stw7KRxE9ZH6q88eev-xtiWQkWdhhDuYfCldhZQmU")
                            UserContext.setApiKey('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmODMwNzVlYWU1YmUyMjFhYzlkOTk4MiIsImlhdCI6MTYxNDkxNDYxNSwiZXhwIjoxNjE1NTE5NDE1fQ.09stw7KRxE9ZH6q88eev-xtiWQkWdhhDuYfCldhZQmU');
                            DevSettings.reload()
                         }

                         }
                         style={{ width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                         <View style={{ flex: 1, paddingHorizontal: 10 }}>
                           <Text style={styles.nameText}>Account 1</Text>
                         </View>
                 </TouchableOpacity>
               </View>
              <View style={styles.containerInside}>
                 <TouchableOpacity
                          onPress={() => {
                            console.log("SET API_KEY TO eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDVjMDFhZjRhNTgwNDY4YzMwZDE0NiIsImlhdCI6MTYxNTE4NDI0NywiZXhwIjoxNjE1Nzg5MDQ3fQ.jwURaGlJUY7fermgn6y-lYMqIN6AgI0bxRQGjsNvMEg")
                            UserContext.setApiKey('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDVjMDFhZjRhNTgwNDY4YzMwZDE0NiIsImlhdCI6MTYxNTE4NDI0NywiZXhwIjoxNjE1Nzg5MDQ3fQ.jwURaGlJUY7fermgn6y-lYMqIN6AgI0bxRQGjsNvMEg')
                            DevSettings.reload()
                          }}
                          style={{ width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                          <View style={{ flex: 1, paddingHorizontal: 10 }}>
                            <Text style={styles.nameText}>Account 2</Text>
                          </View>
                  </TouchableOpacity>
                  </View>
               <View style={styles.containerInside}>
                  <TouchableOpacity
                         onPress={() => console.log("pressed")}
                         style={{ width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                         <View style={{ flex: 1, paddingHorizontal: 10 }}>
                           <Text style={styles.nameText}>Account 3</Text>
                         </View>
                 </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  };

export default AccountSwitcherScreen;

const styles = StyleSheet.create({
containerInside: {
    width: '90%',
    padding: 10,
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2
  },
 nameText: {
    color: '#0F6880',
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    height: '100%',


  },
  scrollview: {
    width: '100%',
        height: '100%',
    backgroundColor: '#EAF7FE',
  },
  inScroll: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',

  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  bodyCon: {
    width: '100%',
    backgroundColor: '#EAF7FE',
    alignItems: 'center',
        height: '100%',
  },

});