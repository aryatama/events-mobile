import React, { useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  RoundedButton,
  TopUpBalanceAllowance,
  TopUpBalancePocketMoney,
} from '../../components/parent';

const TopUpBalanceScreen = (props) => {
  const { childId } = props.route.params;

  const [type, setType] = useState('task');
  return (
    <View style={styles.container}>
      <ScrollView style={styles.scrollview} showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <View
            style={{
              width: '100%',
              alignSelf: 'flex-start',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: '80%',
                alignSelf: 'flex-start',
                justifyContent: 'center',
                padding: 20,
              }}>
              <Text
                style={{
                  ...styles.headerText,
                  fontSize: 30,
                  fontWeight: 'bold',
                }}>
                Top Up Balance
              </Text>
              <Text style={{ ...styles.headerText, fontSize: 14 }}>
                Choose one out of these 3 methods to top up your child's
                balance.
              </Text>
            </View>
            <View
              style={{
                height: '100%',
                width: '100%',
                position: 'absolute',
                alignItems: 'flex-end',
              }}>
              <Image
                source={require('../../../assets/images/parent/topupbalance.png')}
                style={{ resizeMode: 'center', height: '100%', aspectRatio: 1 }}
              />
            </View>
          </View>

          <View
            style={{
              width: '100%',
              margin: 10,
              paddingHorizontal: 20,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: type === 'task' ? '#24C1E7' : 'rgba(0,0,0,0)',
                ...styles.selectType,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderRightWidth: 0,
              }}
              onPress={() => setType('task')}>
              <Text style={{ color: type === 'task' ? 'white' : '#24C1E7' }}>
                Task
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor:
                  type === 'allowance' ? '#24C1E7' : 'rgba(0,0,0,0)',
                ...styles.selectType,
              }}
              onPress={() => setType('allowance')}>
              <Text
                style={{ color: type === 'allowance' ? 'white' : '#24C1E7' }}>
                Allowance
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor:
                  type === 'p_money' ? '#24C1E7' : 'rgba(0,0,0,0)',
                ...styles.selectType,
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
                borderLeftWidth: 0,
              }}
              onPress={() => setType('p_money')}>
              <Text style={{ color: type === 'p_money' ? 'white' : '#24C1E7' }}>
                Pocket Money
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            backgroundColor: '#EAF7FE',
          }}>
          <View
            style={{
              width: '15%',
              height: 7,
              borderRadius: 50,
              backgroundColor: '#DBDBDB',
            }}></View>
        </View>

        {type === 'task' ? (
          <View
            style={{
              width: '100%',
              padding: 20,
              height: (deviceHeight * 6) / 10,
              justifyContent: 'center',
              backgroundColor: '#EAF7FE',
            }}>
            <Text
              style={{
                ...styles.headerText,
                fontWeight: 'bold',
                fontSize: 24,
                marginBottom: 5,
              }}>
              Task
            </Text>
            <Text>
              Add Child's task and reward them. Payment will be automatically
              calculated to your Child's balance.
            </Text>

            <TouchableOpacity
              style={{
                width: '80%',
                alignSelf: 'center',
                marginVertical: 50,
                backgroundColor: '#0F6880',
                padding: 16,
                borderRadius: 50,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() =>
                props.navigation.navigate('AddTaskScreen', { childId: childId })
              }>
              <Text
                style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
                Add Task
              </Text>
            </TouchableOpacity>
          </View>
        ) : type === 'allowance' ? (
          <TopUpBalanceAllowance childId={childId} />
        ) : (
          <TopUpBalancePocketMoney childId={childId} />
        )}
      </ScrollView>
    </View>
  );
};

export default TopUpBalanceScreen;

let deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EAF7FE',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
  },
  header: {
    backgroundColor:'#FBFBFB',
    height: (deviceHeight * 3) / 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  selectType: {
    flex: 1,
    padding: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'grey',
  },
});
