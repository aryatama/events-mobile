import React, { useContext, useRef, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  SafeAreaView,
} from 'react-native';
import { AccountCard, LoadingScreen } from '../../components/parent';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import userContext from '../../context/user/userContext';
import parentContext from '../../context/parent/parentContext';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Snackbar from 'react-native-snackbar';
import {
  postLogout,
  postProfilePicture,
  postCoverPicture,
  postChildProfilePicture,
  postChildCoverPicture,
} from '../../api/parent';
import { RNS3 } from 'react-native-aws3';
import { S3_ACCESS_KEY, S3_SECRET_KEY, S3_BUCKET, S3_REGION } from '@env';
import decodedImageURI from '../../utils/parent/decodedImageURI';

const AccountScreen = (props) => {
  const ParentContext = useContext(parentContext);
  const UserContext = useContext(userContext);

  const [image, setImage] = useState('');
  const [isInprogressUploadingImage, setIsInprogressUploadingImage] = useState(
    false,
  );
  const typeRef = useRef();
  const mutationPostLogout = postLogout();
  const mutationPostProfilePicture = postProfilePicture();
  const mutationPostCoverPicture = postCoverPicture();
  const mutationPostChildProfilePicture = postChildProfilePicture();
  const mutationPostChildCoverPicture = postChildCoverPicture();
  let options = {
    keyPrefix: typeRef.current == 'profile' ? 'user/profile/' : 'user/cover/',
    bucket: S3_BUCKET,
    region: S3_REGION,
    accessKey: S3_ACCESS_KEY,
    secretKey: S3_SECRET_KEY,
    successActionStatus: 201,
  };

  const formatDate_yyyymmdd_HHmmss = (datetime) => {
    const year = datetime.getFullYear().toString();
    let month = (datetime.getMonth() + 1).toString();
    let date = datetime.getDate().toString();
    let hours = datetime.getHours().toString();
    let minutes = datetime.getMinutes().toString();
    let seconds = datetime.getSeconds().toString();

    if (month.length < 2) {
      month = '0' + month;
    }
    if (date.length < 2) {
      date = '0' + date;
    }
    if (hours.length < 2) {
      hours = '0' + hours;
    }
    if (minutes.length < 2) {
      minutes = '0' + minutes;
    }
    if (seconds.length < 2) {
      seconds = '0' + seconds;
    }

    const formatted =
      year + '-' + month + '-' + date + '_' + hours + minutes + seconds;

    return formatted;
  };

  const randomizeFileName = () =>
    'image_' + formatDate_yyyymmdd_HHmmss(new Date()) + '.png';

  const pickImage = async (childId) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      const file = {
        uri: result.uri,
        name: randomizeFileName(),
        type: 'image/png',
      };
      setIsInprogressUploadingImage(true);
      RNS3.put(file, options)
        .progress((e) => console.log(e.loaded / e.total))
        .then((response) => {
          setIsInprogressUploadingImage(false);
          if (response.status !== 201)
            throw new Error('Failed to upload image to S3');
          console.log('RNS3.put response', response);
          if (!childId) {
            if (typeRef.current === 'profile') {
              mutationPostProfilePicture.mutate(
                {
                  profilePicture: response.body.postResponse.location,
                },
                {
                  onSuccess: async (message) => {
                    ParentContext.refetchParent();
                    return Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            } else {
              mutationPostCoverPicture.mutate(
                {
                  coverPicture: response.body.postResponse.location,
                },
                {
                  onSuccess: async (message) => {
                    ParentContext.refetchParent();
                    return Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            }
          } else {
            if (typeRef.current === 'profile') {
              mutationPostChildProfilePicture.mutate(
                {
                  profilePicture: response.body.postResponse.location,
                  childId: childId,
                },
                {
                  onSuccess: async (message) => {
                    ParentContext.refetchChildren();
                    return Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            } else {
              mutationPostChildCoverPicture.mutate(
                {
                  coverPicture: response.body.postResponse.location,
                  childId: childId,
                },
                {
                  onSuccess: async (message) => {
                    ParentContext.refetchChildren();
                    return Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            }
          }
        });
    }
  };

  const selectImage = async (_type, chilId) => {
    if (Platform.OS !== 'web') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Snackbar.show({
          text: 'Sorry, we need camera roll permissions to make this work!',
          duration: 5000,
          backgroundColor: 'red',
          action: {
            text: 'CLOSE',
            textColor: 'white',
            onPress: () => true,
          },
        })
      } else if (status === 'granted') {
        pickImage(chilId);
        typeRef.current = _type;
      }
    }
  };

  const handleManageScreen = (childId) => {
    props.navigation.navigate('ManageChildScreen', {
      childId: childId,
    });
  };

  const handleManageAccount = () => {
    props.navigation.navigate('ManageAccountScreen');
  };

  if (
    isInprogressUploadingImage ||
    mutationPostProfilePicture.isLoading ||
    mutationPostCoverPicture.isLoading ||
    mutationPostLogout.isLoading
  ) {
    return <LoadingScreen />;
  }


  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}>
        <View style={styles.inScroll}>
          <View style={{ width: '90%', marginVertical: 18 }}>
            <Text style={styles.title}>Account</Text>
          </View>

          <View style={styles.bodyCon}>
            <View
              style={{ ...styles.bodyCon, width: '90%', marginVertical: 30 }}>
              <TouchableOpacity
                style={{ width: '100%' }}
                onPress={handleManageAccount}>
                <AccountCard
                  name={ParentContext.parent.familyName}
                  profilePicture={{
                    uri: decodedImageURI(ParentContext.parent.profilePicture),
                  }}
                  balance={ParentContext.parentBalance}
                  onCameraPress={(v) => selectImage(v)}
                  coverPicture={decodedImageURI(
                    ParentContext.parent.coverPicture,
                  )}
                />
              </TouchableOpacity>
              <View style={styles.childCon}>
                <Text
                  style={{
                    fontSize: 22,
                    fontWeight: 'bold',
                    color: '#0F6880',
                  }}>
                  Child Accounts
                </Text>
                <Text style={{ fontSize: 16, color: '#0F6880' }}>
                  Select child to manage their account
                </Text>
                {ParentContext.childs.map((item) => {
                  return (
                    <TouchableOpacity
                      onPress={() => handleManageScreen(item.childId)}
                      key={item.childId}
                      style={{ width: '100%' }}>
                      <AccountCard
                        name={item.firstName}
                        profilePicture={{
                          uri: decodedImageURI(item.profilePicture),
                        }}
                        balance={item.matchMoveWallet}
                        onCameraPress={(v) => selectImage(v, item.childId)}
                        coverPicture={decodedImageURI(item.coverPicture)}
                      />
                    </TouchableOpacity>
                  );
                })}

                {ParentContext.parent?.subscription.numberOfChild >
                ParentContext.childs?.length ? (
                  <TouchableOpacity
                    style={styles.addChild}
                    onPress={() => props.navigation.navigate('AddChildStep1')}>
                    <MIcon name="add-circle-outline" size={36} color="white" />
                    <Text
                      style={{ fontSize: 24, color: 'white', marginLeft: 10 }}>
                      Add Child
                    </Text>
                  </TouchableOpacity>
                ) : (
                  true
                )}
                <TouchableOpacity
                  style={{
                    width: '80%',
                    alignSelf: 'center',
                    marginVertical: 30,
                    backgroundColor: '#F44336',
                    padding: 16,
                    borderRadius: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() =>
                    mutationPostLogout.mutate(
                      {},
                      {
                        onSuccess: async (message) => {
                          await UserContext.logOut();
                          return Snackbar.show({
                            text: message.data.message,
                            duration: Snackbar.LENGTH_LONG,
                          });
                        },
                        onError: (error) =>
                          Snackbar.show({
                            text: error.message,
                            duration: Snackbar.LENGTH_INDEFINITE,
                            backgroundColor: 'red',
                            action: {
                              text: 'CLOSE',
                              textColor: 'white',
                              onPress: () => true,
                            },
                          }),
                      },
                    )
                  }>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    Logout
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

AccountScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default AccountScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
  },
  scrollview: {
    width: '100%',
  },
  inScroll: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  bodyCon: {
    width: '100%',
    backgroundColor: '#EAF7FE',
    alignItems: 'center',
  },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
  childCon: {
    width: '100%',
    marginTop: 40,
  },
  addChild: {
    width: '100%',
    aspectRatio: 3,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 16,
    backgroundColor: '#21BBE6',
  },
});
