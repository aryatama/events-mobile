import React, { useContext } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import currency from '../../utils/parent/currencyFormat';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import parentContext from '../../context/parent/parentContext';
import decodedImageURI from '../../utils/parent/decodedImageURI';
import moment from 'moment';
import { getSubscription } from '../../api/parent';
import {
  PlanCard,
  ErrorAPIHandler,
  LoadingScreen,
} from '../../components/parent';

const ManageAccountScreen = (props) => {
  const ParentContext = useContext(parentContext);

  const { isLoading, isError, error, data } = getSubscription();

  if (isLoading) return <LoadingScreen />;

  if (isError) {
    return <ErrorAPIHandler message={error.message} />;
  }

  let plan = data.details.plans.data.find(
    (item) => item.id === ParentContext.parent.subscription.planId,
  );
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{ uri: decodedImageURI(ParentContext.parent.profilePicture) }}
        />
        <Text style={styles.nameText}>{ParentContext.parent.familyName}</Text>
        <Text style={styles.parentText}>Parent</Text>
      </View>
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10,
          backgroundColor: '#EAF7FE',
        }}>
        <View
          style={{
            width: '15%',
            height: 7,
            borderRadius: 50,
            backgroundColor: '#DBDBDB',
          }}></View>
      </View>

      <View style={styles.body}>
        <View>
          <Text style={styles.subscriptionText}>Subscription</Text>
          <Text style={styles.parentText}>Manage your subscription</Text>
        </View>

        <View style={{ width: '100%' , marginVertical:20}}>
          <PlanCard
            expire={moment(ParentContext.parent.subscription.expiry).format(
              'D MMM YYYY',
            )}
            numOfChild={ParentContext.parent.subscription.numberOfChild}
            price={plan.amount / 100}
          />
        </View>

        <TouchableOpacity
          style={{
            ...styles.butContainer,
            marginTop: 20,
          }}
          onPress={() => props.navigation.navigate('ManageSubscriptionScreen')}>
          <MIcon name="add" color="white" size={30} />
          <Text style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>
            Upgrade Plans
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default ManageAccountScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#EAF7FE',
  },
  butContainer: {
    flexDirection: 'row',
    width: '70%',
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: '#F44336',
    padding: 12,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  body: {
    flex: 1,
    padding: 20,
    width: '100%',
  },
  header: {
    alignItems: 'center',
    width: '100%',
    paddingVertical: 30,
    backgroundColor: '#FBFBFB',
  },
  descButt: {
    backgroundColor: '#0F6880',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    marginVertical: 30,
    borderRadius: 14,
  },
  sumChilds: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },
  priceText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  expireText: {
    marginTop: 10,
    color: 'white',
    fontSize: 14,
  },
  image: {
    width: 80,
    height: 80,
    marginRight: 8,
    borderRadius: 100,
  },
  nameText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0F6880',
    marginTop: 8,
    textAlign: 'center',
    paddingHorizontal:20
  },
  parentText: {
    fontSize: 16,
    color: '#0F6880',
  },
  subscriptionText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0F6880',
  },
});
