import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import { UserOption, ErrorAPIHandler, LoadingScreen } from '../../components/parent';
import parentContext from '../../context/parent/parentContext';
import { getSpendingLimits, postSpendingLimits } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import { shadowIOS } from '../../style/parent/shadowIOS';

const AdjustLimitScreen = (props) => {
  const { childId } = props.route.params;
  const [selectedChild, setselectedChild] = useState(childId);

  const mutationPostSpendingLimits = postSpendingLimits();
  const [dailySpendLimit, setDailySpendLimit] = useState('');
  const [weeklySpendLimit, setWeeklySpendLimit] = useState('');
  const [fortnightlySpendLimit, setFortnightlySpendLimit] = useState('');
  const [monthlySpendLimit, setMonthlySpendLimit] = useState('');

  const ParentContext = useContext(parentContext);

  const {
    isLoading: isLoadingSpendingLimits,
    error: errorGetSpendingLimits,
    data: dataGetSpendingLimits,
    refetch: refetchGetSpendingLimits,
  } = getSpendingLimits(selectedChild);

  if (isLoadingSpendingLimits) return <LoadingScreen />;

  if (errorGetSpendingLimits) {
    return <ErrorAPIHandler message={errorGetSpendingLimits.message} />;
  }

  if (mutationPostSpendingLimits.isLoading) {
    // return <Text>Loading...</Text>;
    return <LoadingScreen />;

  }

  if (
    dailySpendLimit === '' &&
    weeklySpendLimit === '' &&
    fortnightlySpendLimit === '' &&
    monthlySpendLimit === ''
  ) {
    setDailySpendLimit(dataGetSpendingLimits.data.details.dailySpendLimit);
    setWeeklySpendLimit(dataGetSpendingLimits.data.details.weeklySpendLimit);
    setFortnightlySpendLimit(
      dataGetSpendingLimits.data.details.fortnightlySpendLimit,
    );
    setMonthlySpendLimit(dataGetSpendingLimits.data.details.monthlySpendLimit);
  }

  return (
    <View style={styles.container}>
      <ScrollView style={{ width: '100%' }} showsVerticalScrollIndicator={false}>
        <View style={{ width: '90%', marginVertical: 18, alignSelf: 'center' }}>
          <Text style={styles.title}>Adjust Limit</Text>
        </View>

        <View style={{ width: '100%', backgroundColor: '#EAF7FE' }}>
          <View style={{ width: '90%', alignSelf: 'center' }}>
            <Text style={{ ...styles.nameText, marginVertical: 10 }}>
              Limit Applies to
            </Text>
            <UserOption
              childs={ParentContext.childs}
              onValueChange={(v) => {
                setDailySpendLimit('');
                setWeeklySpendLimit('');
                setFortnightlySpendLimit('');
                setMonthlySpendLimit('');
                return setselectedChild(v.childId);
              }}
              selectedChild={selectedChild}
            />
          </View>
          <View style={styles.limitContainer}>
            <Text style={{ ...styles.nameText, marginBottom: 20 }}>
              Monthly Limit
            </Text>
          
            <View style={styles.textInput}>
              <Text style={{ fontSize: 20 }}>$</Text>
              <TextInput
                placeholder="0"
                style={{ flex: 1, fontSize: 20 }}
                value={monthlySpendLimit.toString()}
                onChangeText={(val) => setMonthlySpendLimit(val)}
                keyboardType="number-pad"
                selectTextOnFocus
              />
            </View>
          </View>

          <View style={styles.limitContainer}>
            <Text style={{ ...styles.nameText, marginBottom: 20 }}>
              Weekly Limit
            </Text>
            {/* <Options
            items={data}
            onValueChange={(val) => setWeeklySpendLimit(val)}
            modalTitle="Weekly Limit"
          /> */}
            <View style={styles.textInput}>
              <Text style={{ fontSize: 20 }}>$</Text>
              <TextInput
                placeholder="0"
                style={{ flex: 1, fontSize: 20 }}
                value={weeklySpendLimit.toString()}
                onChangeText={(val) => setWeeklySpendLimit(val)}
                keyboardType="number-pad"
                selectTextOnFocus
              />
            </View>
          </View>

          <View style={styles.limitContainer}>
            <Text style={{ ...styles.nameText, marginBottom: 20 }}>
              Fortnightly Limit
            </Text>
            {/* <Options
            items={data}
            onValueChange={(val) => setWeeklySpendLimit(val)}
            modalTitle="Weekly Limit"
          /> */}
            <View style={styles.textInput}>
              <Text style={{ fontSize: 20 }}>$</Text>
              <TextInput
                placeholder="0"
                style={{ flex: 1, fontSize: 20 }}
                // value={weeklySpendLimit}
                value={fortnightlySpendLimit.toString()}
                onChangeText={(val) => setFortnightlySpendLimit(val)}
                keyboardType="number-pad"
                selectTextOnFocus
              />
            </View>
          </View>

          <View style={styles.limitContainer}>
            <Text style={{ ...styles.nameText, marginBottom: 20 }}>
              Daily Limit
            </Text>
            {/* <Options
            items={data}
            onValueChange={(val) => setDailySpendLimit(val)}
            modalTitle="Daily Limit"
          /> */}
            <View style={styles.textInput}>
              <Text style={{ fontSize: 20 }}>$</Text>
              <TextInput
                placeholder="0"
                style={{ flex: 1, fontSize: 20 }}
                value={dailySpendLimit.toString()}
                onChangeText={(val) => setDailySpendLimit(val)}
                keyboardType="number-pad"
                selectTextOnFocus
              />
            </View>
          </View>

          <TouchableOpacity
            style={{
              width: '80%',
              alignSelf: 'center',
              marginVertical: 50,
              backgroundColor: '#0F6880',
              padding: 16,
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() =>
              mutationPostSpendingLimits.mutate(
                {
                  childId: selectedChild,
                  dailySpendLimit: parseFloat(dailySpendLimit),
                  weeklySpendLimit: parseFloat(weeklySpendLimit),
                  fortnightlySpendLimit: parseFloat(fortnightlySpendLimit),
                  monthlySpendLimit: parseFloat(monthlySpendLimit),
                },
                {
                  onSuccess: (message) => {
                    refetchGetSpendingLimits();
                    Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    });
                    props.navigation.goBack()
                  },
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              )
            }>
            <Text
              style={{
                color: 'white',
                fontSize: 18,
                fontWeight: 'bold',
              }}>
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default AdjustLimitScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FBFBFB',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  limitContainer: {
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 10,
  },
  nameText: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'normal',
    alignSelf: 'flex-start',
  },
  textInput: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    fontSize: 18,
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
});
