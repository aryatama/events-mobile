import React, { useEffect, useState, useContext } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  ChildBalanceCard,
  SavingTargetCard,
  ZimbleCardView,
  RecentTransaction,
  ErrorAPIHandler,
  LoadingScreen,
  RoundedButton,
} from '../../components/parent';
import Icon from '../../utils/parent/icon';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import parentContext from '../../context/parent/parentContext';
import Carousel from 'react-native-snap-carousel';
import currency from '../../utils/parent/currencyFormat';
import decodedImageURI from '../../utils/parent/decodedImageURI';
import Snackbar from 'react-native-snackbar';

const DashboardScreen = (props) => {
  const [selectedChild, setSelectedChild] = useState(null);

  const ParentContext = useContext(parentContext);

  const handleNavigateManageChild = () => {
    props.navigation.navigate('ManageChildScreen', {
      childId: selectedChild,
    });
  };

  const handleNavigateActivateChild = () => {
    if (ParentContext.childs.length !== 0) {
      let child = ParentContext.childs.find(
        (child) => child.childId === selectedChild,
      );
      props.navigation.navigate('ActivateCardScreen', {
        childId: child.childId,
        firstName: child.firstName,
        profilePicture: child.profilePicture,
      });
    }
  };

  if (!selectedChild && ParentContext.childs.length > 0) {
    setSelectedChild(ParentContext.childs[0].childId);
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <View style={styles.profileView}>
          <Image
            style={{ resizeMode: 'cover', position: 'absolute' }}
            source={require('../../../assets/images/parent/backgroundDotWhite.png')}
          />
          <Image
            style={styles.imageParent}
            resizeMode="cover"
            source={{
              uri:
                ParentContext.parent?.profilePicture &&
                decodedImageURI(ParentContext.parent?.profilePicture),
            }}
          />
          <Text style={{ ...styles.textColor, fontSize: 18 }}>
            Welcome back,
          </Text>
          <Text
            style={{ ...styles.textColor, fontSize: 24, fontWeight: 'bold', textAlign:"center", paddingHorizontal:20 }}>
            {ParentContext.parent?.familyName}
          </Text>
        </View>

        <ScrollView
          style={styles.ScrollView}
          showsVerticalScrollIndicator={false}>
          <View
            style={{ width: '100%', height: (deviceHeight * 30) / 100 }}></View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#EAF7FE',
            }}>
            <View style={styles.headerCon}>
              <View style={styles.headerPadd}></View>
              <Text
                style={{
                  ...styles.textColor,
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Your Account Balance
              </Text>
              <Text
                style={{
                  ...styles.textColor,
                  fontSize: 30,
                  fontWeight: 'bold',
                }}>
                {currency(parseFloat(ParentContext.parentBalance))}
              </Text>

              <View style={styles.bodyCon}>
                <TouchableOpacity
                  style={styles.transferFundsCon}
                  onPress={() =>
                    // props.navigation.navigate('NotFoundScreen')
                    props.navigation.navigate('TransferFundsScreen')
                  }>
                  <Icon name="transfer" size={32} color="#ffffff" />
                  <Text
                    style={{
                      ...styles.textColor,
                      color: 'white',
                      width: '100%',
                      fontSize: 16,
                    }}>
                    Transfer Fund
                  </Text>
                </TouchableOpacity>

                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  {ParentContext.childs.map((item) => {
                    return (
                      <TouchableOpacity
                        key={item.childId}
                        onPress={() =>
                          props.navigation.navigate('ManageChildScreen', {
                            childId: item.childId,
                          })
                        }>
                        <ChildBalanceCard
                          firstName={item.firstName}
                          totalCardBalance={currency(
                            parseFloat(item.matchMoveWallet),
                          )}
                          childId={item.childId}
                        />
                      </TouchableOpacity>
                    );
                  })}
                  {ParentContext.parent?.subscription.numberOfChild >
                  ParentContext.childs?.length ? (
                    <TouchableOpacity
                      style={styles.addChildButt}
                      onPress={() =>
                        props.navigation.navigate('AddChildStep1')
                      }>
                      <MIcon
                        name="add-circle-outline"
                        size={32}
                        color="#ffffff"
                      />
                      <Text
                        style={{
                          ...styles.textColor,
                          color: 'white',
                          width: '100%',
                          fontSize: 16,
                        }}>
                        Add Child
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    true
                  )}
                </ScrollView>
              </View>
            </View>

            {ParentContext.childs.length === 0 ? (
              <View style={styles.noChildCon}>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  {ParentContext.childs.length === 1
                    ? 'Your Child'
                    : 'Your Children'}
                </Text>
                <View style={styles.noRegisterChild}>
                  <Text style={{ color: '#1DC1E6' }}>
                    You have no registered child.
                  </Text>
                  <Text style={{ color: '#1DC1E6' }}>Please add child.</Text>
                </View>
                <View style={{ width: '70%' }}>
                  <TouchableOpacity
                    style={{
                      ...styles.butContainer,
                      backgroundColor: '#0F6880',
                    }}
                    onPress={() => {}}>
                    <MIcon name="add-circle-outline" color="white" size={30} />
                    <Text
                      style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>
                      Add Child
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={{ width: '100%' }}>
                <View style={styles.bodyConNoChild}>
                  <Text
                    style={{
                      ...styles.textColor,
                      fontSize: 20,
                      fontWeight: 'bold',
                    }}>
                    {ParentContext.childs.length === 1
                      ? 'Your Child'
                      : 'Your Children'}
                  </Text>

                  <View
                    style={{
                      width: '100%',
                      paddingTop: 10,
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Carousel
                      data={ParentContext.childs}
                      renderItem={({ item }) => {
                        return (
                          <ZimbleCardView
                            cardActiveStatus={item.cardActiveStatus}
                            temporaryCardStatus={item.temporaryCardStatus}
                            childId={item.childId}
                            firstName={item.firstName}
                            profilePicture={decodedImageURI(
                              item.profilePicture,
                            )}
                            cardColor={item.cardColor}
                            matchMoveWalletCard={item.matchMoveWalletCard}
                            onPressActivate={() => {
                              props.navigation.navigate('ActivateCardScreen', {
                                childId: item.childId,
                                firstName: item.firstName,
                                profilePicture: item.profilePicture,
                              });
                            }}
                            onPressUnlock={() => {
                              props.navigation.navigate('LockScreen', {
                                childId: item.childId,
                                type: 'unlock',
                              });
                            }}
                          />
                        );
                      }}
                      sliderWidth={(deviceWidth * 9) / 10}
                      itemWidth={(deviceWidth * 7) / 10}
                      onSnapToItem={(index) =>
                        setSelectedChild(ParentContext.childs[index].childId)
                      }
                    />
                  </View>
                  {ParentContext.childs.length !== 0 && (
                    <View
                      style={{
                        width: '90%',
                        padding: 8,
                      }}>
                      {ParentContext.childs.find(
                        (child) => child.childId === selectedChild,
                      )?.cardActiveStatus === '1' ? (
                        <RoundedButton
                          onPress={handleNavigateManageChild}
                          text="Manage This Child"
                        />
                      ) : (
                        <RoundedButton
                          onPress={handleNavigateActivateChild}
                          text="Activate Child's Card"
                          backgroundColor="#25C1E7"
                        />
                      )}
                    </View>
                  )}
                  <View
                    style={{
                      width: '90%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <RecentTransaction
                      childId={
                        !selectedChild
                          ? ParentContext.childs.length > 0
                            ? ParentContext.childs[0].child
                            : null
                          : selectedChild
                      }
                      fromDashboard={true}
                      onPressMore={() =>
                        props.navigation.navigate('ManageChildScreen', {
                          childId: selectedChild,
                          isTransaction: true,
                        })
                      }
                    />
                  </View>
                </View>
              </View>
            )}
            {ParentContext.childs.length === 0 ? (
              <View style={styles.savingCon}>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Saving Targets & Goals
                </Text>
                <View
                  style={{
                    marginVertical: 40,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ color: '#1DC1E6' }}>
                    You have no registered child.
                  </Text>
                  <Text style={{ color: '#1DC1E6' }}>Please add child.</Text>
                </View>
                <View style={{ width: '70%' }}>
                  <TouchableOpacity
                    style={{
                      ...styles.butContainer,
                      backgroundColor: '#0F6880',
                    }}
                    onPress={() => {}}>
                    <MIcon name="add-circle-outline" color="white" size={30} />
                    <Text
                      style={{ color: 'white', fontSize: 18, marginLeft: 10 }}>
                      Add Child
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={styles.savingConNoChild}>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Saving Targets & Goals
                </Text>
                {ParentContext.childs.map((item) => {
                  return (
                    <View key={item.childId}>
                      <SavingTargetCard
                        childId={item.childId}
                        name={item.firstName}
                        imageSRC={decodedImageURI(item.profilePicture)}
                        weeklySavingTarget={item.weeklySavingTarget}
                        totalCardBalance={item.totalCardBalance}
                        onPressMore={() =>
                          props.navigation.navigate('SeeGoalsScreen', {
                            childId: item.childId,
                        })}
                        snackbar={Snackbar}
                      />
                    </View>
                  );
                })}
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default DashboardScreen;

DashboardScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  butContainer: {
    flexDirection: 'row',
    width: '80%',
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: '#0F6880',
    padding: 12,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyCon: {
    width: '100%',
    padding: 20,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  bodyConNoChild: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    borderRadius: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  headerCon: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    borderRadius: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileView: {
    height: '35%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  noRegisterChild: {
    marginVertical: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noChildCon: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    borderRadius: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  savingConNoChild: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    borderRadius: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  addChildButt: {
    width: (deviceWidth * 25) / 100,
    aspectRatio: 0.9,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginRight: 10,
    borderRadius: 16,
    padding: 8,
    backgroundColor: '#1DC1E6',
  },
  textColor: {
    color: '#0F6880',
  },
  ScrollView: {
    width: '100%',
  },
  imageParent: {
    width: 80,
    height: 80,
    aspectRatio: 1,
    borderRadius: 100,
  },
  savingCon: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
    borderRadius: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  headerPadd: {
    width: '15%',
    height: 7,
    marginVertical: 5,
    borderRadius: 50,
    backgroundColor: '#DBDBDB',
  },
  transferFundsCon: {
    width: (deviceWidth * 25) / 100,
    aspectRatio: 0.9,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginRight: 10,
    borderRadius: 16,
    padding: 8,
    backgroundColor: '#1DA2C3',
  },
});

// jiarong@ezsofe.com
// P@ssword1!
