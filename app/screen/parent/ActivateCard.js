import React, { useState, useContext } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
} from 'react-native';
import { LoadingScreen, RoundedButton, ZimbleCardView } from '../../components/parent';
import { postChildActivateCard } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../context/parent/parentContext';
import { shadowIOS } from '../../style/parent/shadowIOS';
const ActivateCardScreen = (props) => {
  const { childId, profilePicture, firstName } = props.route.params;
  const ParentContext = useContext(parentContext);
  const [cardNum, setCardNum] = useState('');
  const [inputStatus, setInputStatus] = useState(true);
  const [activated, setActivated] = useState(false);
  const mutationPostChildActivateCard = postChildActivateCard();

  const handleChangeText = (val) => {
    let num = val;
    if (num.length < 15) {
      setCardNum(num);
      if (num.length < 14) {
        setInputStatus(false);
      } else setInputStatus(true);
    } else setInputStatus(true);
  };

  const handleButtonActivate = () => {
    let creditNum = cardNum.replace(/-/g, '');
    if (inputStatus && cardNum.length > 0) {
      mutationPostChildActivateCard.mutate(
        {
          childId: childId,
          proxyCode: creditNum,
        },
        {
          onSuccess: async (message) => {
            ParentContext.refetchChildren();
            Snackbar.show({
              text: message.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
            return setActivated(true);
          },
          onError: (error) =>
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            }),
        },
      );
    } else setInputStatus(false);
  };

  function cc_format(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    var matches = v.match(/\d{4,12}/g);
    var match = (matches && matches[0]) || '';
    var parts = [];

    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
      return parts.join('-');
    } else {
      return value;
    }
  }

  if (mutationPostChildActivateCard.isLoading) {
    // return <Text>Loading...</Text>;
    return <LoadingScreen />;

  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{ width: '100%' }} showsVerticalScrollIndicator={false}>
        <View style={{ width: '100%', paddingVertical: 18, alignSelf: 'center', backgroundColor:'#FBFBFB' }}>
          <Text style={{...styles.title, width:'90%', alignSelf:'center'}}>Activate Card</Text>
        </View>

        {!activated ? (
          <View style={styles.body}>
            <View style={styles.bodyCon}>
              <View
                style={{
                  width: '80%',
                  aspectRatio: 1.5,
                  marginVertical: 10,
                }}>
                <Image
                  style={{ width: '100%', height: '100%' }}
                  resizeMode="contain"
                  source={require('../../../assets/images/app/card-back-side.png')}
                />
              </View>

              <KeyboardAvoidingView
                style={{ width: '100%', alignItems: 'center' }}
                behavior="padding">
                <View
                  style={{
                    width: '85%',
                    alignItems: 'center',
                    marginVertical: 20,
                  }}>
                  <Text style={styles.desc}>
                    Enter the 12 digit proxy number located at the back of the
                    card.
                  </Text>
                </View>
                <TextInput
                  placeholder="1234-1234-1234"
                  style={styles.textInput}
                  value={cc_format(cardNum)}
                  onChangeText={(val) => handleChangeText(val, cardNum)}
                  keyboardType="number-pad"
                />
                <Text style={{ color: 'red', marginBottom: 8, width: '80%' }}>
                  {!inputStatus && '*Please enter valid proxy number'}
                </Text>
                <RoundedButton
                  text="Activate"
                  backgroundColor={'#0F6880'}
                  onPress={handleButtonActivate}
                />
              </KeyboardAvoidingView>
            </View>
          </View>
        ) : (
          <View style={styles.body}>
            <View style={styles.bodyCon}>
              <View style={{ width: '70%' }}>
                <ZimbleCardView
                  cardActiveStatus="1"
                  childId={childId}
                  firstName={firstName}
                  profilePicture={profilePicture}
                />
              </View>
              <KeyboardAvoidingView
                style={{ width: '100%', alignItems: 'center' }}
                behavior="padding">
                <View
                  style={{
                    width: '90%',
                    alignItems: 'center',
                    marginVertical: 20,
                  }}>
                  <Text style={styles.desc}>
                    Your Child's card is now activated! Add funds, create tasks
                    and encourage your child to set saving goals
                  </Text>
                </View>
                <View style={{ height: 80 }}></View>
                <RoundedButton
                  text="Manage Child"
                  backgroundColor={'#0F6880'}
                  onPress={() => props.navigation.goBack()}
                />
              </KeyboardAvoidingView>
            </View>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ActivateCardScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAF7FE',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textInput: {
    backgroundColor: 'white',
    width: '85%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    fontSize: 18,
  },

  body: {
    width: '100%',
    flex: 1,
    backgroundColor: '#EAF7FE',
    paddingBottom: 40,
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  bodyCon: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 30,
  },

  desc: {
    fontSize: 18,
    color: '#0F6880',
    textAlign: 'center',
  },
});
