import React, { useContext, useState } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { UserOption, SavingTargetCardPerChild } from '../../components/parent';
import parentContext from '../../context/parent/parentContext';
import Snackbar from 'react-native-snackbar';

const SeeGoalsScreen = (props) => {
  const { childId } = props.route.params;
  const [currentChild, setCurrentChild] = useState();

  const ParentContext = useContext(parentContext);
  const [selectedChild, setselectedChild] = useState(childId);

  if (!currentChild) {
    let dataChild = ParentContext.childs.find(
      (child) => child.childId === selectedChild,
    );
    setCurrentChild(dataChild);
  }

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <View style={styles.topTitle}>
          <View style={{ width: '90%', alignSelf: 'center' }}>
            <Text
              style={{ fontSize: 30, fontWeight: 'bold', color: '#0F6880' }}>
              Child's Goals
            </Text>
          </View>
        </View>

        <View style={styles.body}>
          <View style={styles.bodyContainer}>
            <UserOption
              childs={ParentContext.childs}
              onValueChange={(v) => {
                setselectedChild(v.childId);
              }}
              selectedChild={selectedChild}
            />
          </View>
          <SavingTargetCardPerChild
            childId={selectedChild}
            snackbar={Snackbar}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default SeeGoalsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAF7FE',
  },
  scrollView: {
    width: '100%',
  },
  topTitle: {
    alignSelf: 'center',
    width: '100%',
    backgroundColor: '#FBFBFB',
    paddingVertical: 24,
    justifyContent: 'center',
  },
  body: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  bodyContainer: {
    width: '90%',
  },
  bodyCon: { width: '100%', justifyContent: 'center', marginVertical: 10 },
  line: {
    width: '100%',
    borderBottomWidth: 1,
    marginVertical: 14,
    borderBottomColor: 'grey',
  },
  textColor: { color: '#0F6880' },
});
