import React, { useEffect, useState, useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Switch,
  FlatList,
} from 'react-native';
import { Flow } from 'react-native-animated-spinkit';
import {
  getNotificationStatus,
  postToggleNotification,
  postNotificationsRead,
  getSomeNotifications,
} from '../../api/parent';
import {
  ErrorAPIHandler,
  LoadingScreen,
  NotificationCard,
} from '../../components/parent';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../context/parent/parentContext';

const NotificationsScreen = (props) => {
  const [pageNum, setpageNum] = useState(1);
  const [limitAmt, setLimitAmt] = useState(20);
  const [isLoading, setisLoading] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [isEnabled, setIsEnabled] = useState(null);
  const [isEnd, setisEnd] = useState(false);
  const mutationPostToggleNotification = postToggleNotification();
  const mutationPostNotificationsRead = postNotificationsRead();

  useEffect(() => {
    if (pageNum === 1) {
      getNotifications(1, 20);
    }
    mutationPostNotificationsRead.mutate({});
    return () => {};
  }, []);

  const {
    isLoading: isLoadingGetNotificationStatus,
    error: errorGetNotificationStatus,
    data: dataGetNotificationStatus,
  } = getNotificationStatus();

  const getNotifications = async (pageNum, limitAmt) => {
    try {
      setisLoading(true);
      const response = await getSomeNotifications(pageNum, limitAmt);
      let data = response.data.details.notificationData;
      if (data.length === 0) {
        setisEnd(true);
      }
      setNotifications([...notifications, ...data]);
      setisLoading(false);
    } catch (error) {
      console.log(error.message);
      setisLoading(false);
    }
  };

  if (isLoadingGetNotificationStatus) {
    return <LoadingScreen />;
  }

  if (errorGetNotificationStatus) {
    return <ErrorAPIHandler message={errorGetNotificationStatus.message} />;
  }

  if (dataGetNotificationStatus) {
    if (isEnabled === null) {
      setIsEnabled(dataGetNotificationStatus.data.details.notification);
    }
  }

  if (notifications.length === 0 && !isLoading)
    return (
      <View style={styles.container}>
        <View
          style={{ ...styles.profileView, justifyContent: 'space-between' }}>
          <View style={{ width: '100%' }}>
            <Text
              style={{
                ...styles.textColor,
                fontSize: 35,
                fontWeight: 'bold',
                letterSpacing: 1,
              }}>
              Notifications
            </Text>
            <Text style={{ ...styles.noticeColor, fontSize: 15 }}>
              You have {notifications.length} notifications
            </Text>
          </View>
          <View
            style={{
              ...styles.pushNotification,
              width: '100%',
              alignSelf: 'flex-end',
            }}>
            <Text style={{ ...styles.enableTextColor, fontSize: 17 }}>
              Enable push notification
            </Text>
            <Switch
              style={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }] }}
              trackColor={{ false: '#eaf7fe', true: '#4fd262' }}
              thumbColor={'white'}
              ios_backgroundColor="#eaf7fe"
              onValueChange={() => {
                setIsEnabled(!isEnabled);
                return mutationPostToggleNotification.mutate(
                  {},
                  {
                    onSuccess: (message) =>
                      Snackbar.show({
                        text: message.data.message,
                        duration: Snackbar.LENGTH_LONG,
                      }),
                    onError: (error) =>
                      Snackbar.show({
                        text: error.message,
                        duration: Snackbar.LENGTH_INDEFINITE,
                        backgroundColor: 'red',
                        action: {
                          text: 'CLOSE',
                          textColor: 'white',
                          onPress: () => true,
                        },
                      }),
                  },
                );
              }}
              value={isEnabled}
            />
          </View>
        </View>

        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            backgroundColor: '#EAF7FE',
          }}>
          <View
            style={{
              width: '15%',
              height: 7,
              borderRadius: 50,
              backgroundColor: '#DBDBDB',
            }}></View>
        </View>
        <View
          style={{
            width: '100%',
            height: '100%',
            paddingTop: '50%',
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: '#EAF7FE',
          }}>
          <Text
            style={{
              ...styles.textColor,
              fontSize: 20,
            }}>
            You have no notification
          </Text>
        </View>
      </View>
    );
  if (notifications.length === 0 && isLoading) return <LoadingScreen />;

  const handleLoadMore = (pageNo) => {
    if (isLoading) {
      return;
    }
    if (isEnd) {
      return;
    }
    setpageNum(pageNo);
    getNotifications(pageNo, limitAmt);
  };

  return (
    <View style={styles.container}>
      <View style={{ ...styles.profileView, justifyContent: 'space-between' }}>
        <View style={{ width: '100%' }}>
          <Text
            style={{
              ...styles.textColor,
              fontSize: 35,
              fontWeight: 'bold',
              letterSpacing: 1,
            }}>
            Notifications
          </Text>
          <Text style={{ ...styles.noticeColor, fontSize: 15 }}>
            You have {notifications.length} notifications
          </Text>
        </View>
        <View
          style={{
            ...styles.pushNotification,
            width: '100%',
            alignSelf: 'flex-end',
          }}>
          <Text style={{ ...styles.enableTextColor, fontSize: 17 }}>
            Enable push notification
          </Text>
          <Switch
            style={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }] }}
            trackColor={{ false: '#eaf7fe', true: '#4fd262' }}
            thumbColor={'white'}
            ios_backgroundColor="#eaf7fe"
            onValueChange={() => {
              setIsEnabled(!isEnabled);
              return mutationPostToggleNotification.mutate(
                {},
                {
                  onSuccess: (message) =>
                    Snackbar.show({
                      text: message.data.message,
                      duration: Snackbar.LENGTH_LONG,
                    }),
                  onError: (error) =>
                    Snackbar.show({
                      text: error.message,
                      duration: Snackbar.LENGTH_INDEFINITE,
                      backgroundColor: 'red',
                      action: {
                        text: 'CLOSE',
                        textColor: 'white',
                        onPress: () => true,
                      },
                    }),
                },
              );
            }}
            value={isEnabled}
          />
        </View>
      </View>

      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10,
          backgroundColor: '#EAF7FE',
        }}>
        <View
          style={{
            width: '15%',
            height: 7,
            borderRadius: 50,
            backgroundColor: '#DBDBDB',
          }}></View>
      </View>

      <View style={{ flex: 1 }}>
        <FlatList
          data={notifications}
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <NotificationCard
              createdAt={item.createdAt}
              status={item.status}
              title={item.title}
              message={item.message}
              type={item.type}
              fromUserId={item.fromUserId}
            />
          )}
          refreshing={isLoading}
          onEndReached={() => handleLoadMore(pageNum + 1)}
          ListFooterComponent={() =>
            isLoading ? (
              <View
                style={{
                  backgroundColor: '#eaf7fe',
                  alignItems: 'center',
                  width: '100%',
                  paddingVertical: 20,
                }}>
                <Flow size={40} color="#24c1e7" />
              </View>
            ) : null
          }
        />
      </View>

      {/* <ScrollView
        style={styles.ScrollView}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#EAF7FE',
          }}>
          <View
            style={{
              width: '100%',

              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            {notifications.length > 0 ? (
              notifications.map((data) =>
                data.type === 'topup' || data.type === 'notice' ? (
                  <View
                    style={
                      data.status === '1'
                        ? { ...styles.zimbleReadView }
                        : { ...styles.zimbleUnreadView }
                    }>
                    <Image
                      style={{
                        ...styles.imageProfile,
                        width: 60,
                        height: 60,
                        alignSelf: 'flex-end',
                        borderRadius: 100,
                      }}
                      resizeMode="cover"
                      source={ZimbleIcon}
                    />
                    <View style={{ width: '60%', paddingLeft: 5 }}>
                      <Text
                        style={{ ...styles.notiTextName, paddingBottom: 5 }}>
                        Zimble
                      </Text>
                      <Text style={{ ...styles.noticeText, color: 'black' }}>
                        {data.message}
                      </Text>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ ...styles.noticeText }}>
                        {moment(data.createdAt).fromNow()}
                      </Text>
                    </View>
                  </View>
                ) : (
                  <View
                    style={
                      data.status === '1'
                        ? { ...styles.notificationView }
                        : { ...styles.unreadNotiView }
                    }>
                    {(function () {
                      let picture = ParentContext.childs.find(
                        (child) => child.childId === data.fromUserId,
                      );

                      return (
                        <Image
                          style={{
                            ...styles.imageProfile,
                            width: 60,
                            height: 60,
                            aspectRatio: 1,
                            alignSelf: 'flex-end',
                            borderRadius: 100,
                          }}
                          resizeMode="cover"
                          source={
                            !data.fromUserId
                              ? ZimbleIcon
                              : picture && picture.profilePicture
                              ? {
                                  uri: decodedImageURI(picture.profilePicture),
                                }
                              : {
                                  uri: decodedImageURI(
                                    ParentContext.childs[0].profilePicture,
                                  ),
                                }
                          }
                        />
                      );
                    })()}

                    <View style={{ width: '60%', paddingLeft: 5 }}>
                      <Text
                        style={{ ...styles.notiTextName, paddingBottom: 5 }}>
                        {data.title}
                      </Text>
                      <Text style={{ ...styles.noticeText, color: 'black' }}>
                        {data.message}
                      </Text>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ ...styles.noticeText }}>
                        {moment(data.createdAt).fromNow()}
                      </Text>
                    </View>
                  </View>
                ),
              )
            ) : (
              <View style={{ ...styles.emptyView }}>
                <Text
                  style={{
                    ...styles.textColor,
                    fontSize: 30,
                    fontWeight: 'bold',
                    paddingBottom: 5,
                  }}>
                  Current Notifications
                </Text>
              </View>
            )}
          </View>
        </View>
      </ScrollView> */}
    </View>
  );
};

export default NotificationsScreen;

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FBFBFB',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileView: {
    height: (deviceHeight * 22) / 100,
    width: '100%',
    justifyContent: 'flex-start',
    padding: 20,
  },
  textColor: {
    color: '#0F6880',
  },
  noticeColor: {
    color: '#a9a9a9',
  },
  enableTextColor: {
    color: '#202020',
  },
  ScrollView: {
    width: '100%',
  },
  notificationView: {
    width: deviceWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#eaf7fe',
    borderBottomColor: '#a9a9a9',
    borderBottomWidth: 1,
  },
  zimbleReadView: {
    width: deviceWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#eaf7fe',
    borderBottomColor: '#a9a9a9',
    borderBottomWidth: 1,
  },
  unreadNotiView: {
    width: deviceWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#d4e9f1',
  },
  zimbleUnreadView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#d4e9f1',
    borderLeftColor: '#0f6980',
    borderLeftWidth: 5,
  },
  emptyView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: (deviceHeight * 6) / 10,
  },
  pushNotification: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  notiTextName: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
  },
  noticeText: {
    color: '#a9a9a9',
    fontSize: 14,
  },
  imageProfile: {
    width: 80,
    height: 80,
    marginBottom: 10,
  },
});
