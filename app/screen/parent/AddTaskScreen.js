import React, { useState, useContext } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyleSheet,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  UserOption,
  OptionTask,
  TaskTitleDescription,
  MonetaryOption,
  LoadingScreen,
} from '../../components/parent';
import parentContext from '../../context/parent/parentContext';
import { postChildTask } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import moment from 'moment';
import { shadowIOS } from '../../style/parent/shadowIOS';

const AddTaskScreen = (props) => {
  const { childId } = props.route.params;
  const [selectedChild, setselectedChild] = useState(childId);
  const mutationPostChildTask = postChildTask();
  const [taskTitle, setTaskTitle] = useState('');

  const [taskDescription, setTaskDescription] = useState('');
  const [isRepeat, setIsRepeat] = useState('0');
  const [dueDate, setDueDate] = useState(
    moment(new Date()).format('D MMM YYYY'),
  );
  const [isMonetaryReward, setIsMonetaryReward] = useState(false);
  const [rewardAmount, setRewardAmount] = useState('');
  const [rewardName, setRewardName] = useState('');

  const ParentContext = useContext(parentContext);

  if (mutationPostChildTask.isLoading) {
    // return <Text>Loading...</Text>;
    return <LoadingScreen />;
  }

  const handleAddTask = () => {
    mutationPostChildTask.mutate(
      {
        childId: selectedChild,
        taskName: taskTitle,
        taskDescription: taskDescription,
        repeat: isRepeat,
        dueDate: moment(dueDate).format('YYYY-MM-DD'),
        monetaryReward: isMonetaryReward,
        rewardAmount: rewardAmount,
        rewardName: rewardName,
      },
      {
        onSuccess: (message) => {
          setTaskTitle('');
          setTaskDescription('');
          setIsRepeat('0');
          setDueDate(moment(new Date()).format('D MMM YYYY'));
          setIsMonetaryReward(false);
          setRewardAmount('');
          setRewardName('');
          Snackbar.show({
            text: message.data.message,
            duration: Snackbar.LENGTH_LONG,
          });
          ParentContext.refetchTasks();
          props.navigation.goBack();
        },
        onError: (error) =>
          Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_INDEFINITE,
            backgroundColor: 'red',
            action: {
              text: 'CLOSE',
              textColor: 'white',
              onPress: () => true,
            },
          }),
      },
    );
  };

  return (
    <ScrollView style={styles.ScrollView} showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        <View
          style={{
            width: '100%',
            paddingVertical: 18,
            backgroundColor: '#FBFBFB',
          }}>
          <Text style={styles.title}>Add Task</Text>
          <Text
            style={{
              ...styles.nameText,
              fontSize: 18,
              fontWeight: 'normal',
              marginBottom: 10,
              alignSelf: 'center',
              width: '90%',
            }}>
            Create Tasks and Targets for your child.
          </Text>
        </View>

        <View style={styles.bigDash}></View>
        <View style={{ width: '90%' }}>
          <UserOption
            childs={ParentContext.childs}
            onValueChange={(v) => {
              return setselectedChild(v.childId);
            }}
            selectedChild={selectedChild}
          />
        </View>

        <TaskTitleDescription
          title="Task"
          description="Task Description"
          taskTitle={taskTitle}
          setTaskTitle={setTaskTitle}
          taskDescription={taskDescription}
          setTaskDescription={setTaskDescription}
        />

        <OptionTask
          title="Task Frequency"
          dueTitle="Due Date"
          setIsRepeat={setIsRepeat}
          setDueDate={setDueDate}
        />
        <MonetaryOption
          title="Reward"
          dueTitle="test"
          setIsMonetaryReward={setIsMonetaryReward}
          setRewardAmount={setRewardAmount}
          setRewardName={setRewardName}
        />
        <View
          style={{
            marginTop: 25,
            marginBottom: 90,
          }}>
          <TouchableOpacity
            style={{
              ...styles.confirmButton,
            }}
            onPress={handleAddTask}>
            <Text
              style={{
                ...styles.confirmText,
                fontWeight: '500',
                fontSize: 20,
                textAlign: 'center',
              }}>
              Confirm Task
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default AddTaskScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EAF7FE',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profilePicker: {
    width: '90%',
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 10,
  },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  nameText: {
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
  },
  ScrollView: {
    width: '100%',
    backgroundColor: '#EAF7FE',
  },
  confirmText: { color: '#FFF', fontSize: 18, fontWeight: 'bold' },
  confirmButton: {
    backgroundColor: '#0F6980',
    width: 280,
    padding: 12,
    borderRadius: 20,
  },
});
