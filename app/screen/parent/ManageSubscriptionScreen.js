import React, { useState } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import {
  PlanCard,
  ErrorAPIHandler,
  LoadingScreen,
  RoundedButton,
} from '../../components/parent';
import { getSubscription } from '../../api/parent';
import moment from 'moment';

const ManageSubscription = (props) => {
  const { isLoading, isError, error, data } = getSubscription();

  if (isLoading) return <LoadingScreen />;

  if (isError) {
    return <ErrorAPIHandler message={error.message} />;
  }
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{ width: '100%' }}>
        <View
          style={{
            width: '100%',
            paddingVertical: 18,
            backgroundColor: '#FBFBFB',
          }}>
          <Text style={styles.title}>Upgrade Plan</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.bigDash}></View>

          <View style={{ width: '90%' }}>
            <View style={{ paddingVertical: 28 }}>
              <Text style={styles.atitle}>Subscription Plan</Text>
              <Text style={{ fontSize: 13, color: '#0F6880' }}>
                Upgrade and renew your subscription plan.
              </Text>
            </View>
            {data.details.plans.data.map((item) => {
              return (
                <View key={item.id}>
                  <PlanCard
                    numOfChild={item.metadata.childCount}
                    price={item.amount / 100}
                    expire={moment(
                      new Date().setFullYear(new Date().getFullYear() + 1),
                    ).format('D MMM YYYY')}
                  />
                </View>
              );
            })}
          </View>
          <RoundedButton
            text="Logout"
            onPress={() => {}}
            backgroundColor="#F44336"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ManageSubscription;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignItems: 'center',
  },

  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
  },
  atitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0F6880',
  },
});
