import React, { useContext, useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ActivityIndicator,
} from 'react-native';
import { LoadingScreen, OnlyZimbleCard, RoundedButton } from '../../components/parent';
import PropTypes from 'prop-types';
import parentContext from '../../context/parent/parentContext';
import currency from '../../utils/parent/currencyFormat';
import { CommonActions } from '@react-navigation/native';
import { postChildLock, postChildUnlock, deleteChild } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import decodedImageURI from '../../utils/parent/decodedImageURI';
import { shadowIOS } from '../../style/parent/shadowIOS';
const LockScreen = (props) => {
  const { childId, type } = props.route.params;
  const ParentContext = useContext(parentContext);
  const [child, setChild] = useState();
  const [content, setContent] = useState();
  const mutationPostChildLock = postChildLock();
  const mutationPostChildUnlock = postChildUnlock();
  const mutationDeleteChild = deleteChild();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getChild();
  }, []);

  const getChild = async () => {
    let data = await ParentContext.childs.find(
      (child) => child.childId === childId,
    );
    setChild(data);
    if (type === 'lock') {
      setContent(lock);
    } else if (type === 'unlock') {
      setContent(unlock);
    } else if (type === 'deactivate') {
      setContent(deactivate);
    } else if (type === 'successDeactivate') {
      setContent(successDeactivate);
    }
  };

  const handlePressButton2 = () => {
    setIsLoading(true);
    if (type === 'deactivate') {
      mutationDeleteChild.mutate(
        {
          childId: childId,
        },
        {
          onSuccess: async (message) => {
            ParentContext.refetchChildren();
            props.navigation.navigate('LockScreen', {
              childId: childId,
              type: 'successDeactivate',
            });
            setIsLoading(false);
          },
          onError: (error) => {
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            });
            setIsLoading(false);
          },
        },
      );
    } else if (type === 'lock') {
      mutationPostChildLock.mutate(
        {
          childId: childId,
        },
        {
          onSuccess: async (message) => {
            ParentContext.refetchChildren();
            Snackbar.show({
              text: message.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
            props.navigation.navigate('ManageChildScreen', {
              childId: childId,
            });
            setIsLoading(false);
          },
          onError: (error) => {
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            });
            setIsLoading(false);
          },
        },
      );
    }
  };
  const handlePressButton1 = () => {
    setIsLoading(true);
    if (type === 'successDeactivate') {
      props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'ParentTabs' }],
        }),
      );
    } else if (type === 'unlock') {
      mutationPostChildUnlock.mutate(
        {
          childId: childId,
        },
        {
          onSuccess: async (message) => {
            ParentContext.refetchChildren();
            Snackbar.show({
              text: message.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
            props.navigation.navigate('ManageChildScreen', {
              childId: childId,
            });
            setIsLoading(false);
          },
          onError: (error) => {
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            });
            setIsLoading(false);
          },
        },
      );
    } else {
      setIsLoading(false);
      props.navigation.navigate('ManageChildScreen', {
                  childId: childId,
      });

    };
  };

  if (!content || !child) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size={50} color="red" />
      </View>
    );
  }

  if (
    mutationPostChildLock.isLoading ||
    mutationPostChildUnlock.isLoading ||
    mutationDeleteChild.isLoading ||
    isLoading
  ) {
    // return <Text>Loading...</Text>;
    return <LoadingScreen />;
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}>
        {content.title ? (
          <View
            style={{ width: '90%', marginVertical: 18, alignSelf: 'center' }}>
            <Text style={styles.title}>{content.title}</Text>
          </View>
        ) : (
          true
        )}

        <View
          style={
            content.type === 'lock'
              ? styles.lock
              : content.type === 'deactivate'
                ? styles.deactivate
                : styles.body
          }>
          <View style={styles.bodyCon}>
            <View style={styles.bigDash}></View>

            <Image
              style={{
                width: 80,
                height: 80,
                aspectRatio: 1,
                borderRadius: 100,
              }}
              resizeMode="cover"
              source={{ uri: decodedImageURI(child.profilePicture) }}
            />
            <Text style={styles.nameText}>{child.firstName}</Text>

            <View style={{ width: '70%', marginTop: 10 }}>
              <OnlyZimbleCard
                cardActiveStatus={type === 'unlock' ? '0' : '1'}
                firstName={child.firstName}
                cardColor={child.cardColor}
                matchMoveWalletCard={child.matchMoveWalletCard}
                temporaryCardStatus="1"
                profilePicture={decodedImageURI(child.profilePicture)}
              />
            </View>

            {type === 'successDeactivate' ? (
              true
            ) : (
              <View style={styles.subBody}>
                <View>
                  <Text style={styles.subTitle}>Balance</Text>
                  <Text style={styles.money}>
                    {currency(parseFloat(child.matchMoveWallet))}
                  </Text>
                </View>
                <View>
                  <Text style={styles.subTitle}>Spent this week</Text>
                  <Text style={styles.money}>$65.00</Text>
                </View>
              </View>
            )}
            <View
              style={{
                width: '90%',
                alignItems: 'center',
                marginVertical: 10,
              }}>
              <Text style={{ ...styles.desc, fontSize: 16 }}>
                {content.desc1}
              </Text>
              {content.desc2 ? (
                <Text
                  style={{
                    ...styles.desc,
                    fontWeight: 'normal',
                    marginVertical: 5,
                    marginTop: content.type === 'deactivate' ? 20 : 0,
                  }}>
                  {content.desc2}
                </Text>
              ) : (
                true
              )}

              {type === 'lock' ? (
                <View style={styles.infoLockCon}>
                  <View style={styles.titleInfoLock}>
                    <Text style={styles.titleInfoText}>
                      After locking, card will not be able to
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '90%',
                      paddingVertical: 12,
                      backgroundColor: 'white',
                      elevation: 2,
                      ...shadowIOS,
                    }}>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Transfer money to goals,
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Withdraw money from goals,
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Complete tasks with monetary reward,
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Make purchase, and
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>Public Transport</Text>
                    </View>
                  </View>

                  <View
                    style={{
                      ...styles.titleInfoLock,
                      backgroundColor: '#0F8020',
                    }}>
                    <Text style={styles.titleInfoText}>
                      After locking, card will still be able to
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '90%',
                      paddingVertical: 12,
                      backgroundColor: 'white',
                      elevation: 2,
                      ...shadowIOS,
                    }}>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Event request (create, delete, approve)
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Add, edit, and delete goals,
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View style={styles.dotInfoLock}></View>
                      <Text style={styles.itemInfoText}>
                        Add, submit and complete tasks
                      </Text>
                    </View>
                    <View style={styles.itemInfoLockCon}>
                      <View
                        style={{
                          ...styles.dotInfoLock,
                          backgroundColor: 'white',
                        }}></View>
                      <Text style={styles.itemInfoText}>
                        without monetary reward.
                      </Text>
                    </View>
                  </View>
                </View>
              ) : (
                true
              )}
            </View>

            {content.desc3 ? (
              <Text
                style={{
                  ...styles.desc,
                  fontWeight: 'normal',
                  marginTop: 0,
                  marginBottom: 14,
                }}>
                {content.desc3}
              </Text>
            ) : (
              true
            )}
            <RoundedButton
              text={content.button1}
              backgroundColor="#0F6880"
              onPress={handlePressButton1}
            />
            {content.button2 ? (
              <RoundedButton
                text={content.button2}
                backgroundColor={
                  content.type === 'deactivate' ? 'black' : '#F44336'
                }
                onPress={handlePressButton2}
              />
            ) : (
              true
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default LockScreen;

LockScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBFBFB',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
  },
  infoLockCon: { width: '100%', alignItems: 'center' },
  titleInfoLock: {
    width: '90%',
    backgroundColor: '#F44336',
    padding: 10,
    alignItems: 'center',
    marginTop: 20,
    elevation: 2,
    ...shadowIOS,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  titleInfoText: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
  },
  itemInfoLockCon: {
    width: '100%',
    paddingVertical: 2,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  dotInfoLock: {
    width: 6,
    height: 6,
    borderRadius: 50,
    backgroundColor: '#0F6880',
  },
  itemInfoText: { marginLeft: 5, color: '#0F6880', fontSize: 12 },
  lock: {
    width: '100%',
    backgroundColor: '#EAF7FE',
    borderRadius: 16,
    borderWidth: 6,
    borderBottomWidth: 0,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    borderColor: '#F44336',
  },
  body: {
    width: '100%',
    backgroundColor: '#EAF7FE',
    paddingBottom: 40,
  },
  deactivate: {
    width: '100%',
    backgroundColor: '#EAF7FE',
    borderRadius: 16,
    borderWidth: 6,
    borderBottomWidth: 0,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    borderColor: 'black',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  bodyCon: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: 30,
  },
  nameText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#0F6880',
  },
  subBody: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginVertical: 10,
  },
  subTitle: { fontSize: 14, color: '#0F6880' },
  money: {
    color: '#0F6880',
    fontSize: 32,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    color: '#0F6880',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
});

const lock = {
  type: 'lock',
  title: 'Lock Account',
  image: require('../../../assets/images/app/Blue_Card.png'),
  desc1: "Lock Your Child's Card",
  desc2: 'If it is lost/stolen or if tasks are incomplete.',
  desc3: "Are you sure you want to lock your child's card?",
  button1: "I've Changed My Mind",
  button2: "Lock Child's Card",
};

const unlock = {
  type: 'unlock',
  title: 'Unlock Account',
  image: require('../../../assets/images/app/disabled_card.png'),
  desc3: "Are you sure you want to unlock this child's account?",
  button1: "Unlock Child's Card",
};

const deactivate = {
  type: 'deactivate',
  title: 'Deactivate Account',
  image: require('../../../assets/images/app/disabled_card.png'),
  desc1:
    "If you wish to deactivate your child's account, they will no longer have access.",
  desc2: "The remaining balance will be transferred to the parent's account.",
  desc3: "Are you sure you want to deactivate your child's account?",
  button1: "I've Changed My Mind",
  button2: "Deactivate Child's Account",
};

const successDeactivate = {
  type: 'successDeactivate',
  image: require('../../../assets/images/app/disabled_card.png'),
  desc1: "Your child's account has been successfully deactivate",
  button1: 'Back to Dashboard',
};
