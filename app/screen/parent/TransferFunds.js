import React, { useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  TransferFudsTransfer,
  SetUpVirtualAccount,
  LoadingScreen,
  ErrorAPIHandler,
} from '../../components/parent';
import Clipboard from '@react-native-clipboard/clipboard';
import Icon from '../../utils/parent/icon';
import Snackbar from 'react-native-snackbar';
import { getWalletVirtualAccounts } from '../../api/parent';
import { shadowIOS } from '../../style/parent/shadowIOS';

const TransferFunds = (props) => {
  const [type, setType] = useState('task');
  const [hasVA, setHasVA] = useState(null);

  const copyToClipboard = (v) => {
    Clipboard.setString(v);
    Snackbar.show({
      text: 'Copied',
      duration: Snackbar.LENGTH_SHORT,
    });
  };

  const handleSetTab = (v) => {
    setType(v);
  };

  const handleBackToDashBoard = () => {
    props.navigation.goBack();
  };

  const {
    isLoading: isLoadingGetWalletVirtualAccounts,
    error: errorGetWalletVirtualAccounts,
    data: dataGetWalletVirtualAccounts,
    refetch: refetchGetWalletVirtualAccount,
  } = getWalletVirtualAccounts();

  if (isLoadingGetWalletVirtualAccounts) {
    return <LoadingScreen />;
  }

  if (errorGetWalletVirtualAccounts) {
    return (
      <ErrorAPIHandler
        message={
          errorGetWalletVirtualAccounts || errorGetWalletVirtualAccounts.message
        }
      />
    );
  }

  if (
    dataGetWalletVirtualAccounts.data.message !=
      'User does not have a Virtual Account' &&
    hasVA === null
  ) {
    setHasVA(1);
  }

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <View
            style={{
              width: '100%',
              alignSelf: 'flex-start',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: '80%',
                alignSelf: 'flex-start',
                justifyContent: 'center',
                padding: 20,
              }}>
              <Text
                style={{
                  ...styles.headerText,
                  fontSize: 30,
                  fontWeight: 'bold',
                }}>
                Transfer With Ease
              </Text>
              <Text style={{ ...styles.headerText, fontSize: 16 }}>
                Choose to Transfer via Virtual Account or Credit/Debit Card.
              </Text>
            </View>
            <View
              style={{
                height: '100%',
                width: '100%',
                position: 'absolute',
                alignItems: 'flex-end',
              }}>
              <Image
                source={require('../../../assets/images/parent/transfer.png')}
                style={{ resizeMode: 'center', height: '100%', aspectRatio: 1 }}
              />
            </View>
          </View>

          <View
            style={{
              width: '100%',
              margin: 10,
              paddingHorizontal: 20,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: type === 'task' ? '#24C1E7' : 'rgba(0,0,0,0)',
                ...styles.selectType,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderRightWidth: 0,
              }}
              onPress={() => handleSetTab('task')}>
              <Text style={{ color: type === 'task' ? 'white' : '#24C1E7' }}>
                Virtual Account
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                backgroundColor:
                  type === 'p_money' ? '#24C1E7' : 'rgba(0,0,0,0)',
                ...styles.selectType,
                borderTopRightRadius: 8,
                borderBottomRightRadius: 8,
                borderLeftWidth: 0,
              }}
              onPress={() => handleSetTab('p_money')}>
              <Text style={{ color: type === 'p_money' ? 'white' : '#24C1E7' }}>
                Credit/Debit card
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            backgroundColor: '#EAF7FE',
          }}>
          <View
            style={{
              width: '15%',
              height: 7,
              borderRadius: 50,
              backgroundColor: '#DBDBDB',
            }}></View>
        </View>

        {type === 'task' ? (
          <>
            {hasVA === 1 ? (
              <View
                style={{
                  width: '100%',
                  padding: 20,
                  justifyContent: 'center',
                  backgroundColor: '#EAF7FE',
                }}>
                <View style={styles.virtualAccountContainer}>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 30,
                      fontWeight: 'bold',
                      marginBottom: 8,
                    }}>
                    Virtual Account
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 14,
                      marginBottom: 20,
                    }}>
                    Transfer funds from your bank account to your Zimble Wallet.
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 18,
                      fontWeight: 'bold',
                      marginBottom: 0,
                    }}>
                    Bank
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 16,
                      marginBottom: 18,
                    }}>
                    {
                      dataGetWalletVirtualAccounts.data.details
                        .virtualAccountDetail.bankName
                    }
                  </Text>
                  <View style={styles.vaNumber}>
                    <View>
                      <Text
                        style={{
                          ...styles.headerText,
                          fontSize: 18,
                          fontWeight: 'bold',
                          marginBottom: 0,
                        }}>
                        Virtual Account Number
                      </Text>
                      <Text style={{ ...styles.headerText, fontSize: 16 }}>
                        {
                          dataGetWalletVirtualAccounts.data.details
                            .virtualAccountDetail.virtualAccountNumber
                        }
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        paddingHorizontal: 10,
                        paddingVertical: 8,
                        backgroundColor: '#0F6880',
                        flexDirection: 'row',
                        height: 36,
                        borderRadius: 5,
                        justifyContent: 'center',
                      }}
                      onPress={() =>
                        copyToClipboard(
                          dataGetWalletVirtualAccounts.data.details.virtualAccountDetail.virtualAccountNumber.toString(),
                        )
                      }>
                      <Text
                        style={{
                          ...styles.headerText,
                          fontSize: 14,
                          color: 'white',
                          marginRight: 8,
                        }}>
                        Copy
                      </Text>
                      <Icon name="copy" size={18} color="white" />
                    </TouchableOpacity>
                  </View>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 18,
                      fontWeight: 'bold',
                      marginBottom: 0,
                    }}>
                    Status
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 16,
                      marginBottom: 18,
                    }}>
                    {dataGetWalletVirtualAccounts.data.details
                      .virtualAccountDetail.virtualAccountStatus
                      ? 'Active'
                      : 'Inactive'}
                  </Text>

                  <View style={styles.line}></View>

                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 22,
                      fontWeight: 'bold',
                      marginBottom: 18,
                    }}>
                    Bank Account
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 18,
                      fontWeight: 'bold',
                      marginBottom: 0,
                    }}>
                    Bank Holder Name
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 16,
                      marginBottom: 18,
                    }}>
                    {
                      dataGetWalletVirtualAccounts.data.details
                        .bankAccountDetail.bankHolderName
                    }
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 18,
                      fontWeight: 'bold',
                      marginBottom: 0,
                    }}>
                    Account Number
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 16,
                      marginBottom: 18,
                    }}>
                    {
                      dataGetWalletVirtualAccounts.data.details
                        .bankAccountDetail.bankAccountNumber
                    }
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 18,
                      fontWeight: 'bold',
                      marginBottom: 0,
                    }}>
                    Bank Code
                  </Text>
                  <Text
                    style={{
                      ...styles.headerText,
                      fontSize: 16,
                      marginBottom: 18,
                    }}>
                    {
                      dataGetWalletVirtualAccounts.data.details
                        .bankAccountDetail.bankCode
                    }
                  </Text>
                </View>
              </View>
            ) : (
              <SetUpVirtualAccount onRefetch={refetchGetWalletVirtualAccount}/>
            )}
          </>
        ) : (
          <TransferFudsTransfer onBackToDashboard={handleBackToDashBoard} />
        )}
      </ScrollView>
    </View>
  );
};

export default TransferFunds;

const styles = StyleSheet.create({
  virtualAccountContainer: {
    width: '100%',
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
  },
  vaNumber: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 18,
  },
  container: {
    backgroundColor: '#EAF7FE',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
  },
  header: {
    paddingBottom: 20,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#FBFBFB',
  },
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  selectType: {
    flex: 1,
    padding: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'grey',
  },
  line: {
    width: '100%',
    borderBottomWidth: 1,
    marginVertical: 14,
    borderBottomColor: 'grey',
  },
});
