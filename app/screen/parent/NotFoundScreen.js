import { CommonActions } from '@react-navigation/routers';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native';
import { RoundedButton } from '../../components/parent';

const NotFoundScreen = (props) => {
  const handleBack = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: 'ParentTabs' }],
      }),
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.body}>
        <View style={{ width: '100%', marginBottom: 30 }}>
          <View
            style={{
              paddingVertical: 20,
              width: '100%',
              alignItems: 'center',
              marginBottom:30
            }}>
            <Image
              style={{ width: 200, height: 240 }}
              resizeMode="contain"
              source={require('../../../assets/images/parent/notFound.png')}
            />
            <Text
              style={{ fontSize: 20, fontWeight: 'bold', color: '#0F6880' }}>
              Sorry, Try Again!
            </Text>
          </View>
          <View style={{ width: '100%' }}>
          <RoundedButton
            text="Back To Dashboard"
            backgroundColor="#0F6880"
            onPress={handleBack}
          />
        </View>
        </View>
        
      </View>
    </SafeAreaView>
  );
};

export default NotFoundScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EAF7FE',
  },

  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#0F6880',
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dash: {
    width: 20,
    height: 4,
    borderRadius: 10,
    backgroundColor: '#BBBBBB',
    margin: 4,
  },
  bigDash: {
    width: 52,
    height: 6,
    borderRadius: 10,
    backgroundColor: '#DBDBDB',
    marginTop: 10,
    marginBottom: 26,
  },
  atitle: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#0F6880',
  },

});
