import {
  GET_PARENT,
  GET_TRANSACTION,
  GET_SAVING,
  GET_CHILDS,
  SET_SELECTED_CHILD,
  GET_TASKS,
  GET_BALANCE,
} from '../types';

export const initState = {
  parent: null,
  parentBalance : null,
  transaction: [],
  childs: [],
  saving: [],
  selectedChild: {},
  tasks: [],
};

export const reducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_PARENT:
      return {
        ...state,
        parent: payload,
      };
    case GET_TRANSACTION:
      return {
        ...state,
        transaction: payload,
      };
    case GET_SAVING:
      return {
        ...state,
        saving: payload,
      };
    case GET_CHILDS:
      return {
        ...state,
        childs: payload,
      };
    case SET_SELECTED_CHILD:
      console.log('SET_SELECTED_CHILD state', payload);
      return {
        ...state,
        selectedChild: payload,
      };
    case GET_TASKS:
      return {
        ...state,
        tasks: payload,
      };
      case GET_BALANCE:
        return {
          ...state,
          parentBalance: payload,
        };
    default:
      return state;
  }
};
