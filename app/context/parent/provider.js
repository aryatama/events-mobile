import React from 'react';
import { useReducer } from 'react';
import ParentContext from './parentContext';
import { initState, reducer } from './state';
import { getChildren, getParent, getParentChildrenTask, getParentBalance, getNotifications } from '../../api/parent';
import { LoadingScreen } from '../../components/parent';

const ParentState = (props) => {

  const [state, dispatch] = useReducer(reducer, initState);
  const getParentQuery = getParent();
  const getChildrenQuery = getChildren();
  const getBalanceQuery = getParentBalance();
  const getTasksQuery = getParentChildrenTask();
  const getNotification = getNotifications();

  if (getNotification.isLoading|| getChildrenQuery.isLoading || getParentQuery.isLoading || getTasksQuery.isLoading || getBalanceQuery.isLoading) {
    return <LoadingScreen />
  }

  return (
    <ParentContext.Provider
      value={{
        parent: getParentQuery.data.details?.parentData,
        transaction: state.transaction,
        childs: getChildrenQuery.data.details?.childData || [],
        saving: state.saving,
        childId: state.childId,
        tasks: getTasksQuery.data.data.details?.taskData || [],
        parentBalance: getBalanceQuery.data.details?.balance,
        notifications : getNotification.data.details?.notificationData || [],
        refetchParent: getParentQuery.refetch,
        refetchChildren: getChildrenQuery.refetch,
        refetchBalance: getBalanceQuery.refetch,
        refetchTasks: getTasksQuery.refetch,
        refetchNotifications : getNotification.refetch,
      }}>
      {props.children}
    </ParentContext.Provider>
  );
};

export default ParentState;
