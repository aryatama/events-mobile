export const GET_TRANSACTION = 'GET_TRANSACTION';
export const GET_PARENT = 'GET_PARENT';
export const GET_SAVING = 'GET_SAVING';
export const GET_CHILDS = 'GET_CHILDS';
export const SET_SELECTED_CHILD = 'SET_SELECTED_CHILD';
export const SET_USER_INFO = 'SET_USER_INFO';
export const GET_USER_INFO = 'GET_USER_INFO';
export const SIGN_OUT = 'SIGN_OUT';
export const GET_TASKS = 'GET_TASKS';
export const GET_BALANCE = 'GET_BALANCE';
export const SET_API_KEY = 'SET_API_KEY';

