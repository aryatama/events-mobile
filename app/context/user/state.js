import { SET_USER_INFO, GET_USER_INFO, SIGN_OUT, SET_API_KEY } from '../types';

export const initState = {
  email: '',
  firstName: '',
  lastName: '',
  familyName: '',
  nationality: '',
  profilePicture: '',
  coverPicture: '',
  userToken: '',
  api_key: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmODMwNzVlYWU1YmUyMjFhYzlkOTk4MiIsImlhdCI6MTYxNDkxNDYxNSwiZXhwIjoxNjE1NTE5NDE1fQ.09stw7KRxE9ZH6q88eev-xtiWQkWdhhDuYfCldhZQmU',
};

export const reducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case SET_USER_INFO:
      Object.keys(payload).map((key) => {
        state[key] = payload[key];
      });

      return {
        ...state,
      };
    case SET_API_KEY:
      return {
        ...state,
        api_key: payload,
      };
    case GET_USER_INFO:
      return {
        ...state,
      };
    case SIGN_OUT:
      return {
        ...initState,
      };
    default:
      return state;
  }
};
