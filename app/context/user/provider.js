import React from 'react';
import { useReducer } from 'react';
import { SET_USER_INFO, GET_USER_INFO, SIGN_OUT, SET_API_KEY } from '../types';
import UserContext from './userContext';
import { initState, reducer } from './state';

const UserState = (props) => {
  const [state, dispatch] = useReducer(reducer, initState);

  const setUserInfo = async (userInfo) => {
    try {
      dispatch({ type: SET_USER_INFO, payload: userInfo });
    } catch (error) {
      console.log(error);
    }
  };

  const setApiKey = async (apiKey) => {
      try {
        console.log(apiKey)
        dispatch({ type: SET_API_KEY, payload: apiKey });
      } catch (error) {
        console.log(error);
      }
    };

  const getUserInfo = async () => {
    try {
      dispatch({ type: GET_USER_INFO });
    } catch (error) {
      console.log(error);
    }
  };

  const logOut = async () => {
    try {
      dispatch({ type: SIGN_OUT });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <UserContext.Provider
      value={{
        user: state.user,
        setUserInfo,
        getUserInfo,
        setApiKey,
        logOut,
      }}>
      {props.children}
    </UserContext.Provider>
  );
};

export default UserState;
