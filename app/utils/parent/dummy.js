export const data = {
  parent: {
    familyName: 'Ae Jay Rong',
    lastName: 'Rong',
    middleName: 'Ae Jay',
    totalWallet: 500,
    //childs total balance
    totalCardBalance: 102020,
    totalAmountSaved: 124443,
    parentId: 1231232421,
    deviceType: null,
    deviceToken: 12321413213,
    emailUrl: 'jayrong@mail.com',

    // address:
    // stripe:
    // notification:
    // userType:
    // cardActiveStatus:
    // temporaryCardStatus: >>>
    // cardDeleteRequest:
    // status:
    // phoneOtpStatus:
    // emailVerifyStatus:
    // onlineStatus:

    gender: 'male',
    subscription: {
      numberOfChild: 5,
      planId: 342536465,
      subscriptionId: 2341245,
      renew: 1613696610665,
      expiry: 1613696635370,
      status: 1,
    },
    // hasVirtualAccount:
    // vaBankHolderName:
    // vaBankCode:
    // vaAccountNumber:
    // vaStatus:
  },
  childs: [
    {
      childId: 142367347654,
      firstName: 'Chloe',
      profilePicture: 'imageURL.com/asdasd',
      totalCardBalance: 150,
      cardActiveStatus: true,
      saving: {
        weeklySavingTarget: 150,
        goals: [
          {
            wishlistName: 'PS 5 Pro',
            amountNeeded: 599,
            goalId: 56474845,
            favourite: true,
            amountSave: 40,
          },
          {
            wishlistName: 'Nike Shoes',
            amountNeeded: 200,
            goalId: 44534537,
            favourite: false,
            amountSave: 30,
          },
        ],
      },
      // allowanceDayOfMonth:
      // allowanceAmountLimit:
      // allowanceOn:
      // allowanceSentAt:
      // pocketMoneyFrequency:
      // pocketMoneyAmount:
      // pocketMoneyOn
      // spendLimit:
      // useSpendLimit:
      // dailySpendLimit:
      // useDailySpendLimit:
      // weeklySpendLimit:
      // useWeeklySpendLimit:
      // monthlySpendLimit:
      // useMonthlySpendLimit:
      // weeklySavingTarget:
    },
    {
      childId: 2535768455,
      firstName: 'Nata',
      profilePicture: 'imageURL.com/asdasd',
      totalCardBalance: 300,
      cardActiveStatus: false,
      saving: null,
    },
    {
      childId: 23423423123,
      firstName: 'Boi',
      profilePicture: 'imageURL.com/asdasd',
      totalCardBalance: 100,
      cardActiveStatus: true,
      saving: {
        weeklySavingTarget: 330,
        goals: [
          {
            wishlistName: 'Adidas Shoes',
            amountNeeded: 200,
            goalId: 234234166,
            favourite: false,
            amountSave: 60,
          },
        ],
      },
    },
  ],

  transaction: [
    {
      transactionId: 3422346,
      date: '2021-02-15',
      message: 'Transaction 1',
      childId: 142367347654,
      type: 'Purchase',
      amount: 21,
      indicator : "credit"
    },
    {
      transactionId: 523645345,
      date: '2021-02-16',
      message: 'Transaction 1',
      childId: 142367347654,
      type: 'Purchase',
      amount: 21,
      indicator : "notcredit"
    },
    {
      transactionId: 34645686734,
      date: '2021-02-15',
      message: 'Transaction 2',
      childId: 142367347654,
      type: 'Purchase',
      amount: 25,
      indicator : "credit"
    },
    {
      transactionId: 2647,
      message: 'Transaction 1',
      date: '2021-02-15',
      childId: 23423423123,
      type: 'Transfer',
      amount: 12,
      indicator : "notcredit"
    },
    {
      transactionId: 33534656434,
      message: 'Transaction 1',
      date: '2021-02-16',
      childId: 23423423123,
      type: 'Transfer',
      amount: 12,
      indicator : "notcredit"
    },
    {
      transactionId: 1245215,
      message: 'Transaction 1',
      date: '2021-02-17',
      childId: 23423423123,
      type: 'Transfer',
      amount: 12,
      indicator : "credit"
    },
  ],
  saving: [
    {
      childId: 142367347654,
      savingId: 2342341,
      weeklySavingTarget: 150,
      goals: [
        {
          wishlistName: 'PS 5 Pro',
          amountNeeded: 599,
          goalId: 56474845,
          favourite: true,
          amountSave: 20,
        },
        {
          wishlistName: 'Nike Shoes',
          amountNeeded: 200,
          goalId: 44534537,
          favourite: false,
          amountSave: 60,
        },
      ],
    },
    {
      childId: 23423423123,
      savingId: 2342344,
      weeklySavingTarget: 330,
      goals: [
        {
          wishlistName: 'Adidas Shoes',
          amountNeeded: 200,
          goalId: 234234166,
          favourite: false,
          amountSave: 60,
        },
      ],
    },
  ],

  tasks : [
    {
      tasksId: 2342346,
      childId : 23423423123,
      title : "Complete Homework",
      reward : 3,
      type: "M",
      status : 'pending',
      dueDate : "05/03/2021"
    },
    {
      tasksId: 567435,
      childId : 142367347654,
      title : "Exercise",
      reward : "Playing Game",
      type: "NM",
      status : 'submitted',
      dueDate : "06/03/2021"
    },
    {
      tasksId: 56757,
      childId : 142367347654,
      title : "Cleaning Room",
      reward : 5,
      status : 'complete',
      type: "M",
      dueDate : "04/03/2021"
    },
    {
      tasksId: 3452345,
      childId : 23423423123,
      title : "Exercise",
      status : 'pending',
      reward : "Playing Video Game",
      type: "NM",
      dueDate : "06/03/2021"
    },
    {
      tasksId: 32423,
      childId : 142367347654,
      status : 'submitted',
      title : "Study",
      reward : 4,
      type: "M",
      dueDate : "05/03/2021"
    },
  ]
};
