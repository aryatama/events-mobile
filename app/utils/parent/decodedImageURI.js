const decodedImageURI = (str) => {
  const replacedChar = {
    '&#x2F;': '/',
    '%2F': '/',
  };

  const cleanedURI = str.replace(/&#x2F;|%2F/g, function (matched) {
    return replacedChar[matched];
  });

  return cleanedURI;
};

export default decodedImageURI;
