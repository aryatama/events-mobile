import cook from '../../../assets/icons/parent/cook.png';
import bed from '../../../assets/icons/parent/bed.png';
import dogWalking from '../../../assets/icons/parent/dogWalking.png';
import music from '../../../assets/icons/parent/music.png';
import homework from '../../../assets/icons/parent/homework.png';
import trash from '../../../assets/icons/parent/trash.png';
import topGrade from '../../../assets/icons/parent/topGrade.png';
import cleanHouse from '../../../assets/icons/parent/cleanHouse.png';
import sport from '../../../assets/icons/parent/sport.png';

const listTask = [
  { name: 'Cook Dinner/Breakfast', icon: cook },
  { name: 'Do Homework', icon: homework },
  { name: 'Get top grade', icon: topGrade },
  { name: 'Make up bed', icon: bed },
  { name: 'Dog Walking', icon: dogWalking },
  { name: 'Clean up House', icon: cleanHouse },
  { name: 'Take out trash', icon: trash },
  { name: 'Play music instruments', icon: music },
  { name: 'Do sports/exercise', icon: sport },
];

export { listTask };
