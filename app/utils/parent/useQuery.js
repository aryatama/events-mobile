import { useQuery } from 'react-query';
import Snackbar from 'react-native-snackbar';

export default (key, request) => {
    const query = useQuery(key, request);

    if (query.isError) {
        console.error(key, query.error);
        Snackbar.show({
            text: query.error.message,
            duration: Snackbar.LENGTH_LONG,
        });
    }

    return query;
}