import React from 'react'
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../assets/icons/parent/fontello/config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from 'prop-types';

const CustomIcon = (props) => {
    return (
        <Icon name={props.name} size={props.size} color={props.color} />
    )
}

export default CustomIcon

CustomIcon.proptypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    size: PropTypes.number,
}
