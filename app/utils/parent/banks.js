export const banks = [
    {
      name: 'Australia and New Zealand Banking Group',
      code: 'ANZBSGSXAFX',
    },
    {
      name: 'Bank of China',
      code: 'BKCHSGSG',
    },
    {
      name: 'The Bank of Tokyo-Mitsubishi UFJ',
      code: 'BOTKSGS',
    },
    {
      name: 'BNP Paribas',
      code: 'BNPASGSG',
    },
    {
      name: 'CIMB Bank',
      code: 'CIBBSGSG',
    },
    {
      name: 'Citibank',
      code: 'CITISGSGGCB',
    },
    {
      name: 'DBS/POSB',
      code: 'DBSSSGSG',
    },
    {
      name: 'Deutsche Bank',
      code: 'DEUTSGSG',
    },
    {
      name: 'HL Bank',
      code: 'HLBBSGSG',
    },
    {
      name: 'HSBC',
      code: 'HSBCSGS2',
    },
    {
      name: 'ICICI',
      code: 'ICICSGSG',
    },
    {
      name: 'Industrial and Commercial Bank of China Limited',
      code: 'ICBKSGSG',
    },
    {
      name: 'Maybank',
      code: 'MBBESGS2',
    },
    {
      name: 'Mizuho Bank',
      code: 'MHCBSGSG',
    },
    {
      name: 'OCBC Bank',
      code: 'OCBCSGSG',
    },
    {
      name: 'RHB Bank',
      code: 'RHBBSGSG',
    },
    {
      name: 'Standard Chartered Bank',
      code: 'SCBLSGSG',
    },
    {
      name: 'Sumitomo Mitsui Banking Corporation',
      code: 'STBCSGSG',
    },
    {
      name: 'United Overseas Bank Limited',
      code: 'UOVBSGSG',
    },
  ];