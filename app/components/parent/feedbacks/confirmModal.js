import React, { useState } from 'react';
import {
  Dimensions,
  Pressable,
  StyleSheet,
  View,
  Modal,
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { shadowIOS } from '../../../style/parent/shadowIOS';

const ConfirmModal = (props) => {
  const [type, setType] = useState(props.type);
  //type value = "withHeader", "toCreate" ,"toDelete" ,"withoutHeader"
  

  const handleCloseModal = () => {
    props.onCancel()
  };
  return (
    <Modal animationType="fade" transparent={true} visible={props.isShow}>
      <Pressable style={styles.container} onPress={handleCloseModal}>
        {type === 'withHeader' ? (
          <View style={styles.body}>
            <View style={styles.header}>
              <Text style={styles.title}>{props.title}</Text>
              <Text style={{ ...styles.subTitle, marginTop: 20 }}>
                {props.desc}
              </Text>
            </View>
            <TouchableOpacity style={styles.button1} onPress={props.onConfirm}>
              <Text style={styles.textButton}>{props.buttonConfirm}</Text>
            </TouchableOpacity>
          </View>
        ) : type === 'withoutHeader' ? (
          <View style={styles.body}>
            <View style={styles.header}>
              <Text style={{ ...styles.subTitle, textAlign: 'center' }}>
                {props.desc}
              </Text>
            </View>
            <TouchableOpacity style={styles.button1}onPress={props.onConfirm}>
              <Text style={styles.textButton}>{props.buttonConfirm}</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.body}>
            <View style={styles.header}>
              <Text style={styles.title}>{props.title}</Text>
              <Text style={{ ...styles.subTitle, marginTop: 20 }}>
                {props.desc}
              </Text>
            </View>
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <TouchableOpacity style={styles.button} onPress={props.onCancel}>
                <Text style={{ ...styles.textButton, color: 'black' }}>
                  {props.buttonCancel}
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  width: 1,
                  height: '100%',
                  backgroundColor: 'rgba(0,0,0,0.2)',
                }}></View>
              <TouchableOpacity style={styles.button} onPress={props.onConfirm}>
                <Text
                  style={{
                    ...styles.textButton,
                    color: type === 'toDelete' ? 'red' : '#24C1E7',
                  }}>
                  {props.buttonConfirm}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Pressable>
    </Modal>
  );
};

ConfirmModal.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  buttonCancel: PropTypes.string,
  buttonConfirm: PropTypes.string,
  isShow: PropTypes.bool,
  onCancel : PropTypes.func,
  onConfirm : PropTypes.func,
};

export default ConfirmModal;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  title: {
    color: '#0F6880',
    fontSize: 22,
    fontWeight: 'bold',
  },
  button1: {
    width: '100%',
    padding: 16,
    borderTopWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    padding: 16,
    borderTopWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
  },
  header: { width: '100%', padding: 20, alignItems: 'center' },
  subTitle: {
    fontSize: 16,
    color: '#0F6880',
    textAlign:'center'
  },
  textButton: {
    fontSize: 18,
    color: '#24C1E7',
  },
  container: {
    width: '100%',
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  body: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
  },
});
