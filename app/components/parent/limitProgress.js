import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import shadowIOS from '../../style/parent/shadowIOS'

const LimitProgress = (props) => {
  return (
    <View style={styles.savingContainer}>
      <View
        style={{ ...styles.savingFill, width: `${props.progress}%` }}></View>
      <View style={styles.savingContentContainer}>
        <Text style={{ ...styles.regularTextSaving, fontWeight: 'normal' }}>
          {props.title}
        </Text>
        <Text style={styles.regularTextSaving}>
          {props.progress.toFixed()}%
        </Text>
      </View>
    </View>
  );
};

export default LimitProgress;

const styles = StyleSheet.create({
  savingContainer: {
    width: '100%',
    marginBottom: 10,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS
  },
  savingFill: {
    height: '100%',
    borderRadius: 10,
    backgroundColor: '#C1FFC6',
    alignSelf: 'flex-start',
    position: 'absolute',
  },
  savingContentContainer: {
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
  },
  regularTextSaving: { color: '#0F6880', fontSize: 18, fontWeight: 'bold' },
});
