import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

const RoundedButton = (props) => {
  const handlePress = () => props.onPress();

  return (
    <TouchableOpacity
      style={{
        ...styles.container,
        backgroundColor: props.backgroundColor || '#0F6880',
      }}
      onPress={handlePress}>
      <Text style={{ color: 'white', fontSize: 16, }}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

export default RoundedButton;

RoundedButton.propTypes = {
  onPress: PropTypes.func,
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    width: '80%',
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: '#0F6880',
    padding: 16,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
