import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { SavingTargetProgressCard } from '.';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from '../../utils/parent/icon';
import { getChildGoals } from '../../api/parent';
import { shadowIOS } from '../../style/parent/shadowIOS';

const SavingTargetCardPerChild = (props) => {
  const {
    isLoading: isLoadingGetGoals,
    error: errorGetGoals,
    data: dataGetGoals,
    refetch: refetchGetGoals,
  } = getChildGoals(props.childId);

  if (isLoadingGetGoals) return <Text>Loading...</Text>;

  if (errorGetGoals) {
    return <Text>No Goals</Text>;
   
  }

  return (
    <View
      style={{
        width: '90%',
        alignSelf: 'center',
        marginVertical: 10,
        padding: 10,
        paddingHorizontal: 20,
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 2,
        ...shadowIOS
      }}>
      <View style={styles.bodyCon}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 12,
          }}>
          <Icon name="pig-saving" size={32} color="#F44336" />
          <View style={{ flex: 1 }}>
            <Text
              style={{
                ...styles.textColor,
                fontSize: 20,
                marginLeft: 20,
              }}>
              Saving Target
            </Text>
          </View>
          <Text style={{ ...styles.textColor, fontSize: 20 }}>
            ${dataGetGoals.data.details.savingTarget}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Icon name="two-money" size={32} color="#0F8020" />
          <View style={{ flex: 1 }}>
            <Text
              style={{
                ...styles.textColor,
                fontSize: 20,
                marginLeft: 20,
              }}>
              Current Balance
            </Text>
          </View>
          <Text style={{ ...styles.textColor, fontSize: 20 }}>
            ${dataGetGoals.data.details.currBalance}
          </Text>
        </View>

          <View style={styles.line}></View>


        {dataGetGoals.data.details.goalsData.length != 0 ? (dataGetGoals.data.details.goalsData.map((item, index) => {
          return (
            <View style={{ width: '100%' }} key={index}>
              <SavingTargetProgressCard
                childId={props.childId}
                goalId={item.goalId}
                favourite={item.favorite}
                wishlistName={item.wishlistName}
                amountNeeded={item.amountNeeded}
                amountSave={item.amountSave}
                snackbar={props.snackbar}
                refetchGetGoals={() => refetchGetGoals()}
              />
            </View>
          );
        })) : (
        <Text
          style={{
            ...styles.textColor,
            fontSize: 20,
            marginVertical: 30,
            textAlign: 'center',
          }}>
          No goals to show from this child
        </Text>
        )}

        {/*  <View style={styles.line}></View>
              {currentChild.saving.goals?.map((item) => {
                return (
                  <View style={{ width: '100%' }} key={item.goalID}>
                    <SavingTargetProgressCard
                      favourite={item.favourite}
                      wishlistName={item.wishlistName}
                      amountNeeded={item.amountNeeded}
                      amountSave={item.amountSave}
                    />
                  </View>
                );
              })}
            </View> */}
      </View>
    </View>
  );
};

SavingTargetCardPerChild.propTypes = {
  childId: PropTypes.number.isRequired,
  snackbar: PropTypes.object.isRequired,
};

export default SavingTargetCardPerChild;

const styles = StyleSheet.create({
  textColor: { color: '#0F6880' },
  container: {
    width: '90%',
    marginVertical: 10,
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS
  },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
  nameText: {
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  bodyContainer: {
    width: '100%',
    justifyContent: 'center',
    marginVertical: 10,
  },
  line: { width: '100%', borderBottomWidth: 1, marginVertical: 14 },
});
