import moment from 'moment';
import React, { useContext } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import PropTypes from 'prop-types';

import parentContext from '../../context/parent/parentContext';
import decodedImageURI from '../../utils/parent/decodedImageURI';

var ZimbleIcon = require('../../../assets/icons/parent/z-icon.png');
const NotificationCard = (props) => {
  const ParentContext = useContext(parentContext);

  let child = ParentContext.childs.find(
    (child) => child.childId === props.fromUserId,
  );
  return props.type === 'notice' || props.type === 'topup' ? (
    <View
      style={props.status === '1' ? styles.container : styles.containerZimble}>
      <Image style={styles.image} resizeMode="cover" source={ZimbleIcon} />
      <View style={styles.main}>
        <Text style={styles.title}>{props.title}</Text>
        <Text>{props.message}</Text>
      </View>
      <View style={{ alignSelf: 'center', width: '20%' }}>
        <Text style={styles.time}> {moment(props.createdAt).fromNow()}</Text>
      </View>
    </View>
  ) : (
    <View
      style={{
        ...styles.container,
        backgroundColor: props.status === '1' ? '#eaf7fe' : '#d4e9f1',
      }}>
      <Image
        style={styles.image}
        resizeMode="cover"
        source={
          // uri: decodedImageURI(child.profilePicture),
          !props.fromUserId
          ? ZimbleIcon
          : child && child.profilePicture
          ? {
              uri: decodedImageURI(child.profilePicture),
            }
          : {
              uri: decodedImageURI(
                ParentContext.childs[0].profilePicture,
              ),
            }
        }
      />

      <View style={styles.main}>
        <Text style={styles.title}>{props.title}</Text>
        <Text>{props.message}</Text>
      </View>
      <View style={{ alignSelf: 'center', width: '20%' }}>
        <Text style={styles.time}> {moment(props.createdAt).fromNow()}</Text>
      </View>
    </View>
  );
};

export default NotificationCard;

NotificationCard.propTypes = {
  title: PropTypes.string,
  createdAt: PropTypes.string,
  message: PropTypes.string,
  type: PropTypes.string,
  status: PropTypes.string,
  fromUserId: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderBottomColor: '#a9a9a9',
    borderBottomWidth: 0.6,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#eaf7fe',
    justifyContent: 'space-between',
  },
  containerZimble: {
    width: '100%',
    borderBottomColor: '#a9a9a9',
    borderBottomWidth: 0.6,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#d4e9f1',
    borderLeftColor: '#0f6980',
    borderLeftWidth: 5,
  },
  borderNot: {},
  main: { width: '55%' },
  image: {
    width: 50,
    height: 50,
    marginRight: 8,
    borderRadius: 100,
  },
  title: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
  },
  time: {
    color: 'grey',
    fontSize: 12,
  },
});
