import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Modal,
  Pressable,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { shadowIOS } from '../../style/parent/shadowIOS';

const Options = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [data, setData] = useState(props.items);
  const [selecteditem, setSelecteditem] = useState(data[0]);

  function handlePick(item) {
    setSelecteditem(item);
    props.onValueChange(item);
    setIsShow(false);
  }

  return (
    <TouchableOpacity
      onPress={() => setIsShow(!isShow)}
      iew
      style={styles.container}>
      <View style={{ flex: 1 }}>
        {props.isMoney ? (
          <Text style={{ ...styles.nameText }}>$ {selecteditem}</Text>
        ) : (
          <Text style={{ ...styles.nameText }}>{selecteditem}</Text>
        )}
      </View>
      <View>
        <Icon name={isShow ? 'expand-less' : 'expand-more'} size={40} />
      </View>

      <Modal animationType="fade" transparent={true} visible={isShow}>
        <Pressable style={styles.pressable} onPress={() => {}}>
          <View style={{ width: '90%' }}>
            <ScrollView style={styles.scrollview} showsVerticalScrollIndicator={false}>
              <Text style={styles.textModalTitle}>
                Select {props.modalTitle} :
              </Text>

              {data?.map((item) => {
                return (
                  <TouchableOpacity
                    key={item}
                    style={styles.optionStyle}
                    onPress={() => handlePick(item)}>
                    <View style={{ width: '90%' }}>
                      {props.isMoney ? (
                        <Text style={{ ...styles.nameText }}>$ {item}</Text>
                      ) : (
                        <Text style={{ ...styles.nameText }}>{item}</Text>
                      )}
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </TouchableOpacity>
  );
};

Options.propTypes = {
  isMoney: PropTypes.bool,
  onValueChange: PropTypes.func,
  modalTitle: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.any),
};

export default Options;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS
  },
  nameText: {
    color: 'black',
    marginLeft: 0,
    fontSize: 20,
    fontWeight: 'normal',
  },
  optionStyle: {
    width: '100%',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    backgroundColor: '#EAF7FE',
    marginBottom: 10,
  },
  pressable: {
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  scrollview: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: 0,
  },
  textModalTitle: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});
