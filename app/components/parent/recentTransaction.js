import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import TransactionCard from './transactionCard';
import Icon from 'react-native-vector-icons/Ionicons';
import { getChildTransactions } from '../../api/parent';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Flow } from 'react-native-animated-spinkit';

const RecentTransaction = (props) => {
  if (!props.childId) {
    return null;
  }
  const {
    isLoading: isLoadingGetChildTransaction,
    error: errorGetChildTransaction,
    data: dataGetChildTransaction,
  } = getChildTransactions(props.childId);

  const handlePressMore = () => {
    props.onPressMore();
  };

  if (isLoadingGetChildTransaction)
    return (
      <View
        style={{
          backgroundColor: '#FBFBFB',
          alignItems: 'center',
          width: '100%',
          paddingVertical: 20,
        }}>
        <Flow size={40} color="#24c1e7" />
      </View>
    );

  if (errorGetChildTransaction) {
    return <Text>{errorGetChildTransaction.message}</Text>;
  }

  if (dataGetChildTransaction.details.transactionsData.length === 0) {
    return (
      <View style={{ width: '100%', alignItems: 'center' }}>
        <Text
          style={{
            ...styles.textColor,
            fontSize: 20,
            fontWeight: 'bold',
            marginTop: 10,
          }}>
          Recent Transaction
        </Text>
        <Text
          style={{
            color: '#24C1E7',
            fontSize: 14,
            marginVertical: 30,
          }}>
          This Child has no transaction history
        </Text>
      </View>
    );
  }

  return (
    <>
      {props.fromDashboard && (
        <Text
          style={{
            ...styles.textColor,
            fontSize: 20,
            fontWeight: 'bold',
            marginTop: 10,
          }}>
          Recent Transaction
        </Text>
      )}
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 20,
        }}>
        <View style={styles.transIndicatorCon}>
          <View style={styles.subTransCon}>
            <Icon name="caret-up" size={30} color="#0F8020" />
            <Text style={{ color: '#0F6880' }}>Income</Text>
          </View>
          <View style={styles.subTransCon}>
            <Icon name="caret-down" size={30} color="#F44336" />
            <Text style={{ color: '#0F6880' }}>Spending</Text>
          </View>
        </View>
        {dataGetChildTransaction.details.transactionsData.map((item, i) => {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              key={i}>
              <TransactionCard
                message={item.message}
                type={moment(item.date).format('D MMM YYYY, h:mm A')}
                amount={item.amount}
                transactionId={item.transactionId}
                indicator={item.indicator}
              />
            </View>
          );
        })}
      </View>
      {props.onPressMore && (
        <TouchableOpacity onPress={handlePressMore}>
          <Text
            style={{
              ...styles.textColor,
              fontSize: 14,
              marginVertical: 10,
              alignSelf:'center',
            }}>
            View More Transaction
          </Text>
        </TouchableOpacity>
      )}
    </>
  );
};

export default RecentTransaction;

RecentTransaction.propTypes = {
  childId: PropTypes.string,
  onPressMore: PropTypes.func,
  fromDashboard: PropTypes.bool,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileView: {
    height: '35%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  textColor: {
    color: '#0F6880',
  },
  ScrollView: {
    width: '100%',
  },
  transIndicatorCon: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  subTransCon: { flexDirection: 'row', alignItems: 'center' },
});
