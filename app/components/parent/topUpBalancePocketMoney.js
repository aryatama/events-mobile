import React, { useState, useContext } from 'react';
import { AmountCard, UserOption, TopUpBalanceOption } from '.';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../context/parent/parentContext';
import { postChildPocketMoney } from '../../api/parent';
import { Flow } from 'react-native-animated-spinkit';

const TopUpBalancePocketMoney = (props) => {
  const ParentContext = useContext(parentContext);
  const [selectedChild, setselectedChild] = useState(props.childId);

  const [rewardAmount, setRewardAmount] = useState('');
  const mutationPostChildPocketMoney = postChildPocketMoney();
  const data = ['One-off', 'Monthly', 'Weekly', 'Fortnightly', 'Daily'];
  const [selectedDate, setSelectedDate] = useState(data[0]);

  const handleConfirmButton = () => {
    if (rewardAmount.length === 0) {
      return Snackbar.show({
        text: 'Please Enter Amount For Pocket Money',
        duration: 4000,
        backgroundColor: 'red',
        action: {
          text: 'CLOSE',
          textColor: 'white',
          onPress: () => true,
        },
      });
    }
    mutationPostChildPocketMoney.mutate(
      {
        childId: selectedChild,
        pocketMoneyAmount: rewardAmount,
        pocketMoneyFrequency: selectedDate,
        pocketMoneyOn: true,
      },
      {
        onSuccess: (message) => {
          setRewardAmount('');
          setSelectedDate(data[0]);
          return Snackbar.show({
            text: message.data.message,
            duration: Snackbar.LENGTH_LONG,
          });
        },
        onError: (error) =>
          Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_INDEFINITE,
            backgroundColor: 'red',
            action: {
              text: 'CLOSE',
              textColor: 'white',
              onPress: () => true,
            },
          }),
      },
    );
  };

  if (mutationPostChildPocketMoney.isLoading) {
    return (
      <View
        style={{
          backgroundColor: '#eaf7fe',
          alignItems: 'center',
          width: '100%',
          paddingVertical: 20,
        }}>
        <Flow size={40} color="#24c1e7" />
      </View>
    )
  }

  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EAF7FE',
      }}>
      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{
            ...styles.headerText,
            fontWeight: 'bold',
            fontSize: 24,
            marginBottom: 5,
          }}>
          Pocket Money
        </Text>
        <Text>Give your child pocket money to smile them up</Text>
        <Text style={{ ...styles.headerText, fontSize: 20, marginTop: 20 }}>
          Select Your Child
        </Text>
        <UserOption
          childs={ParentContext.childs}
          onValueChange={(v) => setselectedChild(v.childId)}
          selectedChild={selectedChild}
        />
      </View>

      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 20,
            alignSelf: 'flex-start',
            marginVertical: 10,
          }}>
          Enter Amount
        </Text>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 5,
            borderBottomColor: 'rgba(0,0,0,0.4)',
            borderBottomWidth: 1,
            marginBottom: 20,
          }}>
          <Text
            style={{
              ...styles.headerText,
              color: rewardAmount ? '#0F6880' : '#7dafbf',
              marginLeft: 0,
              fontSize: 20,
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            $
          </Text>
          <TextInput
            style={{
              ...styles.headerText,
              color: '#0F6880',
              marginLeft: 0,
              fontSize: 20,
              height:50,
              flex: 1,
              fontWeight: 'bold',
            }}
            placeholder={'0'}
            placeholderTextColor="#7dafbf"
            keyboardType="numeric"
            value={rewardAmount}
            onChangeText={(v) => setRewardAmount(v)}
          />
        </View>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 16,
            fontWeight: 'bold',
            alignSelf: 'flex-start',
            marginLeft: 0,
            marginTop: 10,
            marginBottom: 10,
          }}>
          Or, select amount below
        </Text>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('100')}
            style={styles.tiles}>
            <AmountCard title="$ 100" iconName="one-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('200')}
            style={styles.tiles}>
            <AmountCard title="$ 200" iconName="some-coins" />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('300')}
            style={styles.tiles}>
            <AmountCard title="$ 300" iconName="lotof-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('500')}
            style={styles.tiles}>
            <AmountCard title="$ 500" iconName="money-bag" />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{ ...styles.headerText, fontSize: 18, marginVertical: 10 }}>
          Select date for your Child's allowance.
        </Text>
        <View style={{ width: '100%', flexDirection: 'row' }}>
          <TopUpBalanceOption
            data={data}
            onValueChange={(val) => setSelectedDate(val)}
            modalTitle="date"
          />
          <Text
            style={{ ...styles.headerText, fontSize: 18, marginVertical: 10 }}>
            to my child
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={{
          width: '80%',
          alignSelf: 'center',
          marginVertical: 30,
          backgroundColor: '#0F6880',
          padding: 16,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={handleConfirmButton}>
        <Text style={{ color: 'white', fontSize: 18 }}>Confirm</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TopUpBalancePocketMoney;

const styles = StyleSheet.create({
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  tiles: { width: '50%' },
});
