import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Modal,
  Pressable,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';

const TopUpBalanceOption = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [data, setData] = useState(props.data);
  const [selecteditem, setSelecteditem] = useState(data[0]);

  function handlePick(item) {
    setSelecteditem(item);
    props.onValueChange(item);
    setIsShow(false);
  }

  function ordinalFormat(i) {
    var j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + 'st';
    }
    if (j == 2 && k != 12) {
      return i + 'nd';
    }
    if (j == 3 && k != 13) {
      return i + 'rd';
    }
    return i + 'th';
  }

  return (
    <TouchableOpacity style={styles.container} onPress={() => setIsShow(!isShow)}>
      <View style={{}}>
        {typeof selecteditem === 'string' ? (
          <Text
            style={{
              ...styles.nameText,
              textDecorationLine: 'underline',
              fontSize: 28,
              fontWeight: 'bold',
            }}>
            {selecteditem}
          </Text>
        ) : (
          <Text
            style={{
              ...styles.nameText,
              textDecorationLine: 'underline',
              fontSize: 28,
              fontWeight: 'bold',
            }}>
            {ordinalFormat(selecteditem)}
          </Text>
        )}
      </View>
        <Icon
          name={isShow ? 'expand-less' : 'expand-more'}
          color="#0F6880"
          size={40}
        />

      <Modal
        animationType="fade"
        transparent={true}
        visible={isShow}
        onRequestClose={() => {
        }}>
        <Pressable style={styles.pressable} onPress={() => {}}>
          <View style={{ width: '80%' }}>
            <ScrollView style={styles.scrollview} showsVerticalScrollIndicator={false}>
              <Text style={styles.textModalTitle}>
                Select {props.modalTitle} :
              </Text>

              {data?.map((item) => {
                return (
                  <TouchableOpacity
                    key={item}
                    style={styles.optionStyle}
                    onPress={() => handlePick(item)}>
                    <View style={{ width: '90%' }}>
                      {typeof item === 'string' ? (
                        <Text style={{ ...styles.nameText }}>{item}</Text>
                      ) : (
                        <Text style={{ ...styles.nameText }}>
                          {ordinalFormat(item)}
                        </Text>
                      )}
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </TouchableOpacity>
  );
};

TopUpBalanceOption.propTypes = {
  onValueChange: PropTypes.func,
  modalTitle: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.any),
};

export default TopUpBalanceOption;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
  },
  nameText: {
    fontSize: 20,
    fontWeight: 'normal',
    color: '#0F6880',
  },
  optionStyle: {
    width: '100%',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    backgroundColor: '#EAF7FE',
    marginBottom: 10,
  },
  pressable: {
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  scrollview: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: 0,
  },
  textModalTitle: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});
