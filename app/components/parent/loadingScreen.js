import React from 'react';
import { SafeAreaView, StyleSheet,  } from 'react-native';

import { Pulse } from 'react-native-animated-spinkit';

const LoadingScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Pulse size={60} color="#24c1e7" />
    </SafeAreaView>
  );
};

export default LoadingScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#EAF7FE',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
