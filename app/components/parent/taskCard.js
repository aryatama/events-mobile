import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import currency from '../../utils/parent/currencyFormat';
import moment from 'moment';
import { shadowIOS } from '../../style/parent/shadowIOS';
import decodedImageURI from '../../utils/parent/decodedImageURI';

const TaskCard = (props) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          ...styles.indicator,
          backgroundColor: props.monetaryReward ? '#0F8020' : '#1DC1E6',
        }}>
        <Text style={styles.indicText}>
          {props.monetaryReward ? 'M' : 'NM'}
        </Text>
      </View>
      <View style={styles.desc}>
        <View>
          <Text style={styles.title} numberOfLines={1}>{decodedImageURI(props.title)}</Text>
          <Text style={{ fontSize: 14 }}>
            Due : {moment(props.dueDate).format('DD/MM/YYYY')}
          </Text>
        </View>
        <Text style={styles.money}>
          {props.monetaryReward
            ? currency(parseFloat(props.reward))
            : props.rewardName
            ? props.rewardName
            : ''}
        </Text>
      </View>
      <View
        style={styles.acceptedIndicator}>
        {props.status === '1' ? (
       <View style={{padding:2, borderColor:'#36CF58', borderWidth:2}}>
         <Text style={{fontSize:9, fontWeight:'bold', color:'#36CF58'}}>ACCEPTED</Text>
       </View>
       ) : (true)}
      </View>
    </View>
  );
};

export default TaskCard;

TaskCard.propTypes = {
  title: PropTypes.string,
  dueDate: PropTypes.string,
  rewardAmount: PropTypes.number,
  monetaryReward: PropTypes.bool,
  status: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    borderRadius: 10,
    marginVertical: 10,
    elevation: 1,
    ...shadowIOS,
  },
  acceptedIndicator:{
    width: '20%',
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: "white"
  },
  indicator: {
    width: '20%',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    height: 90,
    backgroundColor: '#0F8020',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: { color: '#0F6880', fontSize: 16 },
  indicText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  desc: {
    width: '60%',
    height: 90,
    
    backgroundColor: 'white',
    justifyContent: 'space-around',
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  money: {
    color: '#0F6880',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
