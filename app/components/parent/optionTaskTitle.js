import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Modal,
  Pressable,
  Dimensions,
  TextInput,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { shadowIOS } from '../../style/parent/shadowIOS';
import { listTask } from '../../utils/parent/listTask';

const OptionsTaskTitle = (props) => {
  const [isShow, setIsShow] = useState(false);
  // const [data, setData] = useState(dataOptions);
  const [data, setData] = useState(listTask);
  const [selecteditem, setSelecteditem] = useState('');

  function handlePick(item) {
    setSelecteditem(item);
    props.onValueChange(item);
    setIsShow(false);
  }

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        {selecteditem.length === 0 ? (
          <Text style={{ ...styles.nameText, color: 'grey', fontSize: 18 }}>
            Select reward
          </Text>
        ) : (
          <Text style={{ ...styles.nameText }}>{selecteditem}</Text>
        )}
      </View>
      <TouchableOpacity onPress={() => setIsShow(!isShow)}>
        <Icon name={isShow ? 'expand-less' : 'expand-more'} size={40} />
      </TouchableOpacity>

      <Modal animationType="fade" transparent={true} visible={isShow}>
        <Pressable style={styles.pressable} onPress={() => {}}>
          <View
            style={{
              width: '90%',
              borderRadius: 10,
              backgroundColor: 'white',
              padding: 10,
              paddingBottom: 0,
            }}>
            <Text style={styles.textModalTitle}>
              Select {props.modalTitle} :
            </Text>

            <View
              style={{ width: '90%', alignSelf: 'center', marginBottom: 10 }}>
              <Text style={{ color: 'grey', fontSize: 18, marginBottom: 4 }}>
                Reward name
              </Text>
              <View style={{ ...styles.container, padding: 0, height:50 }}>
                <TextInput
                  placeholder="Type Reward"
                  style={{ ...styles.nameText, width: '100%' }}
                  value={selecteditem}
                  onChangeText={(v) => {
                    props.setTaskName(v);
                    return setSelecteditem(v);
                  }}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: '80%',
                  alignSelf: 'center',
                  marginVertical: 10,
                  backgroundColor: '#0F6880',
                  padding: 16,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => setIsShow(false)}>
                <Text
                  style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
                  Select Task
                </Text>
              </TouchableOpacity>

              <Text
                style={{
                  alignSelf: 'center',
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: 'black',
                  marginVertical: 10,
                }}>
                Or select one of the following
              </Text>
            </View>
            <ScrollView
              style={styles.scrollview}
              showsVerticalScrollIndicator={false}>
              {/* {data?.map((item) => {
                return (
                  <TouchableOpacity key ={item.taskName}
                    style={styles.optionStyle}
                    onPress={() => handlePick(item.taskName)}>
                    <Icon name={item.iconName} size={30} />
                    <View style={{ width: '90%' }}>
                      <Text style={{ ...styles.nameText, marginLeft: 10 }}>
                        {item.taskName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })} */}
              {data?.map((item) => {
                return (
                  <TouchableOpacity
                    key={item.name}
                    style={styles.optionStyle}
                    onPress={() => handlePick(item.name)}>
                    <Icon name={item.iconName} size={30} />
                    <Image
                      source={item.icon}
                      style={{ width: 26, height: 26 }}
                      resizeMode="cover"
                    />
                    <View style={{ width: '90%' }}>
                      <Text style={{ ...styles.nameText, marginLeft: 10 ,color:"#0F6880"}}>
                        {item.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </View>
  );
};

OptionsTaskTitle.propTypes = {
  onValueChange: PropTypes.func,
  setTaskName: PropTypes.func,
  modalTitle: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.any),
};

export default OptionsTaskTitle;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS,
  },
  nameText: {
    color: 'black',
    marginLeft: 0,
    fontSize: 20,
    fontWeight: 'normal',
  },
  optionStyle: {
    width: '100%',
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',

    borderRadius: 14,
    backgroundColor: '#EAF7FE',
    marginBottom: 16,
  },
  pressable: {
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollview: {
    width: '90%',
    alignSelf: 'center',
    marginBottom: 20,
    maxHeight: (deviceHeight * 3) / 10,
  },
  textModalTitle: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

const dataOptions = [
  {
    taskName: 'Complete Homework',
    iconName: 'auto-stories',
  },
  {
    taskName: 'Cook Dinner/Breakfast',
    iconName: 'restaurant',
  },
  {
    taskName: 'Get Top School Grades',
    iconName: 'anchor',
  },
  {
    taskName: 'Make Bed',
    iconName: 'analytics',
  },
  {
    taskName: 'Walk Dog',
    iconName: 'all-out',
  },
  {
    taskName: 'Cook Dinner/Meal',
    iconName: 'airport-shuttle',
  },
  {
    taskName: 'Clean Room/House',
    iconName: 'anchor',
  },
  {
    taskName: 'Take Out Trash',
    iconName: 'all-inbox',
  },
  {
    taskName: 'Play Piano/Other Instruments',
    iconName: 'analytics',
  },
  {
    taskName: 'Join/Maintain Sport/Exercise Activities',
    iconName: 'airport-shuttle',
  },
];
