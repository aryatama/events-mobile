import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {StyleSheet, Text, View} from 'react-native';
import CustomIcon from '../../utils/parent/icon';

const AmountCard = (props) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: "92%",
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 16,
        padding: 20,
        backgroundColor: '#0F6980',
      }}>
      <CustomIcon name={props.iconName} size={28} color="white" />
      <Text
        style={{
          color: 'white',
          fontSize: 25,
        }}>
        {props.title}
      </Text>
    </View>
  );
};

export default AmountCard;

const styles = StyleSheet.create({});
