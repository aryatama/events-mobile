import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Linking,
} from 'react-native';
import { Options, RoundedButton } from '.';
import CheckBox from '@react-native-community/checkbox';
import { banks } from '../../utils/parent/banks';
import { postWalletVirtualAccounts } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import { shadowIOS } from '../../style/parent/shadowIOS';
import { Flow } from 'react-native-animated-spinkit';
import { LoadingScreen } from '../../components/parent';

const SetUpVirtualAccount = (props) => {
  const [selectedItem, setSelectedItem] = useState('ANZBSGSXAFX');
  const [toggleCheckBox, setToggleCheckBox] = useState();
  const [cardName, setCardName] = useState({ value: '', status: false });
  const [cardNumber, setCardNUmber] = useState({ value: '', status: false });
  const [inputStatus, setInputStatus] = useState(true);
  const [submitPressed, setSubmitPressed] = useState(false);
  const mutationPostWalletVirtualAccounts = postWalletVirtualAccounts();

  const handleSelectItem = (val) => {
    setSelectedItem(val);
  };

  const snackbar = (text) =>
    Snackbar.show({
      text: text,
      duration: 4000,
      backgroundColor: 'red',
      action: {
        text: 'CLOSE',
        textColor: 'white',
        onPress: () => true,
      },
    });

  function handleCheckName() {
    if (cardName.value.trim() == '') {
      setCardName({ ...cardName, status: false });
    } else setCardName({ ...cardName, status: true });
  }

  const handleChangeNum = (val) => {
    let num = val;
    if (num.length < 20) {
      setCardNUmber({ ...cardName, value: num });
      if (num.length < 19) {
        setInputStatus(false);
      } else setInputStatus(true);
    } else setInputStatus(true);
  };

  const handleSubmitButton = () => {
//    if (cardName.value.length === 0) {
//      return snackbar('Please Enter Your Card Name');
//    } else if (cardNumber.value.length === 0) {
//      return snackbar('Please Enter Your Card Number');
//    } else if (toggleCheckBox == false) {
//      return snackbar('Please agree to the terms of services');
//    }
    setSubmitPressed(true);
    let creditNum = cardNumber.value.replace(/-/g, '');
    if (toggleCheckBox && inputStatus && cardNumber.value.length > 0) {
      mutationPostWalletVirtualAccounts.mutate(
        {
          bankHolderName: cardName.value,
          bankAccountNumber: creditNum,
          bankCode: selectedItem,
        },
        {
          onSuccess: async (message) => {
            props.onRefetch()
            Snackbar.show({
              text: message.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          },
          onError: (error) =>
            Snackbar.show({
              text: error.message,
              duration: Snackbar.LENGTH_INDEFINITE,
              backgroundColor: 'red',
              action: {
                text: 'CLOSE',
                textColor: 'white',
                onPress: () => true,
              },
            }),
        },
      );
    } else setInputStatus(false);
  };

  function cc_format(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    var matches = v.match(/\d{4,16}/g);
    var match = (matches && matches[0]) || '';
    var parts = [];

    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
      return parts.join('-');
    } else {
      return value;
    }
  }

  if (mutationPostWalletVirtualAccounts.isLoading) {
    return (<View
      style={{
        backgroundColor: '#eaf7fe',
        alignItems: 'center',
        width: '100%',
        paddingVertical: 20,
      }}>
      <Flow size={40} color="#24c1e7" />
    </View>);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Virtual Account</Text>
        <Text style={styles.sub}>
          Set up your Virtual Account by completing your details bellow to start
          transferring money.
        </Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.inputText}>Select Bank</Text>
        <Options
          isMoney={false}
          onValueChange={(val) => {
            console.log('VALL', banks.find((bank) => bank.name === val).code);
            return handleSelectItem(
              banks.find((bank) => bank.name === val).code,
            );
          }}
          modalTitle="Select Bank"
          items={banks.map((item) => item.name)}
        />
        <Text style={{ ...styles.inputText, marginTop: 20 }}>Your Details</Text>
        <TextInput
          placeholder="Your Name"
          style={styles.textInput}
          value={cardName.val}
          onChangeText={(v) => setCardName({ ...cardName, value: v })}
          onEndEditing={() => handleCheckName()}
        />
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {submitPressed && !cardName.status && '*Please enter valid card holder name'}
        </Text>
        <TextInput
          placeholder="Your Bank Account Number"
          value={cc_format(cardNumber.value)}
          style={styles.textInput}
          onChangeText={(v) => handleChangeNum(v)}
          keyboardType="number-pad"
        />
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {submitPressed && !inputStatus && '*Please enter valid card number'}
        </Text>

        <View style={styles.agreements}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={(newValue) => setToggleCheckBox(newValue)}
          />
          <Text
            style={{
              fontSize: 14,
              color: '#0F6880',
            }}>
            I agree to MatchMove's
          </Text>
          <TouchableOpacity
          onPress={() => {
            Linking.openURL("https://www.matchmove.com/terms");
          }}>
            <Text
              style={{
                fontSize: 14,
                color: '#24C1E7',
              }}>
              {' '}
              Terms & Conditions
            </Text>
          </TouchableOpacity>
        </View>
        <Text style={{ color: 'red', marginBottom: 8 }}>
                  {' '}
                  {submitPressed && !toggleCheckBox && '*Please agree to the terms of services'}
        </Text>
        <View style={{ marginVertical: 20, width: '100%' }}>
          <RoundedButton
            onPress={handleSubmitButton}
            text="Submit"
            backgroundColor="#0F6880"
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SetUpVirtualAccount;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EAF7FE',
  },
  header: {
    width: '90%',
    padding: 20,
    backgroundColor: 'white',
    elevation: 2,
    borderRadius: 10,
    marginBottom: 20,
    ...shadowIOS,
  },
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  sub: {
    alignSelf: 'flex-start',
    color: '#0F6880',
    fontSize: 14,
    marginBottom: 10,
  },
  body: {
    width: '80%',
  },
  inputText: {
    color: '#0F6880',
    fontSize: 20,
    marginVertical: 10,
  },
  textInput: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height:50,
    fontSize: 18,
    marginBottom: 0,
  },
  agreements: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
