import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from '../../utils/parent/icon';
import currency from '../../utils/parent/currencyFormat';
import { postFavoriteGoal } from '../../api/parent';
import { shadowIOS } from '../../style/parent/shadowIOS';

const SavingTargetProgressCard = (props) => {
  const mutationPostFavoriteGoal = postFavoriteGoal();


  const [isLike, setIsLike] = useState(props.favourite);
  return (
    <View style={styles.savingContainer}>
      <View
        style={{
          ...styles.savingFill,
          width: `${(props.amountSave / props.amountNeeded) * 100}%`,
        }}></View>
      <View style={styles.savingContentContainer}>
        {/* <Icon name='pig-saving' size={32} color="#F44336" /> */}
        <View style={{ flex: 1, alignItems: 'flex-start', marginLeft: 10 }}>
          <Text style={styles.regularTextSaving}>{props.wishlistName}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ ...styles.smallTextSaving, marginRight: 14 }}>
              Target : {currency(parseFloat(props.amountNeeded))}
            </Text>
            <Text style={styles.smallTextSaving}>
              Current : {currency(parseFloat(props.amountSave))}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            setIsLike(!isLike);
            mutationPostFavoriteGoal.mutate(
              {
                childId: props.childId,
                goalId: props.goalId,
              },
              {
                onSuccess: (message) => {
                  props.refetchGetGoals();
                  return props.snackbar.show({
                    text: message.data.message,
                    duration: props.snackbar.LENGTH_LONG,
                  });
                },

                onError: (error) =>
                  props.snackbar.show({
                    text: error,
                    duration: Snackbar.LENGTH_INDEFINITE,
                    backgroundColor: 'red',
                    action: {
                      text: 'CLOSE',
                      textColor: 'white',
                      onPress: () => true,
                    },
                  }),
              },
            );
          }}>
          <Icon
            name="love"
            size={24}
            color={props.favourite ? '#F44336' : '#919191'}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

SavingTargetProgressCard.proptypes = {
  goalId: PropTypes.string,
  wishlistName: PropTypes.string,
  favourite: PropTypes.bool,
  amountNeeded: PropTypes.number,
  amountSave: PropTypes.number,
  snackbar: PropTypes.func.isRequired,
  //refetchGetGoals: PropTypes.func.isRequired,
};

export default SavingTargetProgressCard;

const styles = StyleSheet.create({
  savingContainer: {
    width: '100%',
    marginBottom: 10,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS,
  },
  savingFill: {
    height: '100%',
    borderRadius: 10,
    backgroundColor: '#C1FFC6',
    alignSelf: 'flex-start',
    position: 'absolute',
  },
  savingContentContainer: {
    width: '100%',
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
  },
  regularTextSaving: { color: '#0F6880', fontSize: 16, fontWeight: 'bold' },
  smallTextSaving: { color: '#0F6880', fontSize: 14 },
});
