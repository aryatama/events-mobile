import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { SavingTargetProgressCard } from '.';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import Icon from '../../utils/parent/icon';
import { getChildGoals } from '../../api/parent';
import currency from '../../utils/parent/currencyFormat';
import { shadowIOS } from '../../style/parent/shadowIOS';

const SavingTargetCard = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [dataGoals, setDataGoals] = useState(props.goals);

  const handlePressMore = () => {
      props.onPressMore();
    };

  const {
    isLoading: isLoadingGetGoals,
    error: errorGetGoals,
    data: dataGetGoals,
    refetch: refetchGetGoals,
  } = getChildGoals(props.childId);

  if (isLoadingGetGoals) return <Text>Loading...</Text>;

  if (errorGetGoals) {
    // return <Text>{errorGetGoals.message}</Text>;
    return <View></View>;
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => setIsShow(!isShow)}
        style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
        <Image
          style={{
            width: 50,
            height: 50,
            aspectRatio: 1,
            borderRadius: 100,
          }}
          resizeMode="cover"
          source={{ uri: props.imageSRC }}
        />
        <View style={{ flex: 1, paddingHorizontal: 10 }}>
          <Text style={styles.nameText} numberOfLines={1}>{props.name}</Text>
        </View>
        <MIcon name={isShow ? 'expand-less' : 'expand-more'} size={40} />
      </TouchableOpacity>
      {isShow && (
        <View style={styles.bodyContainer}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 12,
            }}>
            <Icon name="pig-saving" size={30} color="#F44336" />
            <View style={{ flex: 1 }}>
              <Text
                style={{ ...styles.textColor, fontSize: 18, marginLeft: 20 }}>
                Saving Target
              </Text>
            </View>
            <Text style={{ ...styles.textColor, fontSize: 18 }}>
              {currency(parseFloat(dataGetGoals.data.details.savingTarget))}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="two-money" size={30} color="#0F8020" />
            <View style={{ flex: 1 }}>
              <Text
                style={{ ...styles.textColor, fontSize: 18, marginLeft: 20 }}>
                Current Balance
              </Text>
            </View>
            <Text style={{ ...styles.textColor, fontSize: 18 }}>
              {currency(parseFloat(dataGetGoals.data.details.currBalance))}
            </Text>
          </View>

            <View
              style={{
                height: 1,
                backgroundColor: 'grey',
                marginVertical: 14,
                alignSelf: 'stretch',
              }}></View>
          {dataGetGoals.data.details.goalsData.length != 0 ? (dataGetGoals.data.details.goalsData.slice(0, 3).map((item, index) => {
            return (
              <View style={{ width: '100%' }} key={index}>
                <SavingTargetProgressCard
                  childId={props.childId}
                  goalId={item.goalId}
                  favourite={item.favorite}
                  wishlistName={item.wishlistName}
                  amountNeeded={item.amountNeeded}
                  amountSave={item.amountSave}
                  snackbar={props.snackbar}
                  refetchGetGoals={() => refetchGetGoals()}
                />
              </View>
            );
          })
          ) :
          (
            <Text
              style={{
                ...styles.textColor,
                fontSize: 20,
                marginVertical: 30,
                textAlign: 'center',
              }}>
              No goals to show from this child
            </Text>
            )}
        {dataGetGoals.data.details.goalsData.length != 0 ? (
            <TouchableOpacity onPress={handlePressMore}>
              <Text
                style={{
                  ...styles.textColor,
                  fontSize: 14,
                  marginVertical: 10,
                  textAlign: 'center',
                }}>
                View More Goals
              </Text>
            </TouchableOpacity>) : (true)}
        </View>
      )}
    </View>
  );
};

SavingTargetCard.propTypes = {
  name: PropTypes.string.isRequired,
  weeklySavingTarget: PropTypes.number,
  totalCardBalance: PropTypes.number,
  imageSRC: PropTypes.string,
  onPressMore: PropTypes.func,
};

export default SavingTargetCard;

const styles = StyleSheet.create({
  textColor: { color: '#0F6880' },
  container: {
    width: '90%',
    marginVertical: 10,
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS,
  },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
  nameText: {
    color: '#0F6880',
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  bodyContainer: {
    width: '100%',
    justifyContent: 'center',
    marginVertical: 10,
  },
  line: {
    width: '100%',
    height: 2,
    backgroundColor: 'grey',
    marginVertical: 14,
  },
});
