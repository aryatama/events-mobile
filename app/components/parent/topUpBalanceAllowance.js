import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import { AmountCard, UserOption, TopUpBalanceOption, ErrorAPIHandler } from '.';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../context/parent/parentContext';
import { getChildAllowance, postChildAllowance } from '../../api/parent';
import currency from '../../utils/parent/currencyFormat';
import { Flow } from 'react-native-animated-spinkit';
const TopUpBalanceAllowance = (props) => {
  const ParentContext = useContext(parentContext);
  const [selectedChild, setselectedChild] = useState(props.childId);

  const [rewardAmount, setRewardAmount] = useState('');

  const [selectedDate, setSelectedDate] = useState(dataDate[0]);
  const [currentRewardAmount, setCurrentRewardAmount] = useState('');
  const [currentRewardDate, setCurrentRewardDate] = useState('');
  const [currentAllowanceOn, setCurrentAllowanceOn] = useState('');
  const mutationPostChildAllowance = postChildAllowance();

  const {
    isLoading: isLoadingGetChildAllowance,
    error: errorGetChildAllowance,
    data: dataGetChildAllowance,
    refetch: refetchGetChildAllowance,
  } = getChildAllowance(selectedChild);

  if (isLoadingGetChildAllowance) return (<View
    style={{
      backgroundColor: '#eaf7fe',
      alignItems: 'center',
      width: '100%',
      paddingVertical: 20,
    }}>
    <Flow size={40} color="#24c1e7" />
  </View>)

  if (errorGetChildAllowance) {
    return <ErrorAPIHandler message={errorGetChildAllowance.message} />;
  }

  if (mutationPostChildAllowance.isLoading) {
    return (<View
      style={{
        backgroundColor: '#eaf7fe',
        alignItems: 'center',
        width: '100%',
        paddingVertical: 20,
      }}>
      <Flow size={40} color="#24c1e7" />
    </View>)
  }

  function ordinalFormat(i) {
    var j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + 'st';
    }
    if (j == 2 && k != 12) {
      return i + 'nd';
    }
    if (j == 3 && k != 13) {
      return i + 'rd';
    }
    return i + 'th';
  }

  const handleCancelAllowance = () => {
    mutationPostChildAllowance.mutate(
      {
        childId: selectedChild,
        monthlyAllowance: currentRewardAmount,
        allowanceDate: currentRewardDate,
        allowanceOn: false,
      },
      {
        onSuccess: (message) => {
          Snackbar.show({
            text: message.data.message,
            duration: Snackbar.LENGTH_LONG,
          });
          refetchGetChildAllowance();
          setRewardAmount('');
          setCurrentAllowanceOn(false);
          setSelectedDate(dataDate[0]);
        },
        onError: (error) =>
          Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_INDEFINITE,
            backgroundColor: 'red',
            action: {
              text: 'CLOSE',
              textColor: 'white',
              onPress: () => true,
            },
          }),
      },
    );
  };

  const handleChangeAllowance = () => {
    if (rewardAmount.length === 0) {
      return Snackbar.show({
        text: 'Please Enter Amount To Allowance',
        duration: 4000,
        backgroundColor: 'red',
        action: {
          text: 'CLOSE',
          textColor: 'white',
          onPress: () => true,
        },
      });
    }
    mutationPostChildAllowance.mutate(
      {
        childId: selectedChild,
        monthlyAllowance: rewardAmount,
        allowanceDate: selectedDate,
        allowanceOn: true,
      },
      {
        onSuccess: (message) => {
          Snackbar.show({
            text: message.data.message,
            duration: Snackbar.LENGTH_LONG,
          });
          refetchGetChildAllowance();
          setCurrentAllowanceOn(true)
          setCurrentRewardAmount(rewardAmount);
          setCurrentRewardDate(selectedDate);
          setRewardAmount('');
          setSelectedDate(dataDate[0]);
        },
        onError: (error) =>
          Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_INDEFINITE,
            backgroundColor: 'red',
            action: {
              text: 'CLOSE',
              textColor: 'white',
              onPress: () => true,
            },
          }),
      },
    );
  };

  if (
    dataGetChildAllowance.data &&
    !currentRewardAmount &&
    !currentRewardDate
  ) {
    if (
      !dataGetChildAllowance.data.details.monthlyAllowance ||
      !dataGetChildAllowance.data.details.allowanceDate
    ) {
      setCurrentRewardAmount('0');
      setCurrentRewardDate('1');
    } else {
      setCurrentRewardAmount(
        dataGetChildAllowance.data.details.monthlyAllowance,
      );
      setCurrentRewardDate(dataGetChildAllowance.data.details.allowanceDate);
      setCurrentAllowanceOn(dataGetChildAllowance.data.details.allowanceOn);
    }
  }

  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EAF7FE',
      }}>
      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{
            ...styles.headerText,
            fontWeight: 'bold',
            fontSize: 24,
            marginBottom: 5,
          }}>
          Allowance
        </Text>
        <Text>Set your child's monthly allowances</Text>
        <Text style={{ ...styles.headerText, fontSize: 20, marginTop: 20 }}>
          Select Your Child
        </Text>
        <UserOption
          childs={ParentContext.childs}
          onValueChange={(v) => {
            setCurrentRewardAmount('');
            setCurrentRewardDate('');
            setselectedChild(v.childId);
          }}
          selectedChild={selectedChild}
        />

        <View style={{ flexDirection: 'row', flex: 1, marginTop: 10 }}>
          <View
            style={{
              padding: 8,
              paddingHorizontal: 16,
              flex: 2,
              borderTopLeftRadius: 20,
              borderBottomLeftRadius: 20,
              backgroundColor: '#0F6880',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{ color: 'white', fontSize: 16 }}>Allowance</Text>
          </View>
          <View
            style={{
              padding: 8,
              flex: 4,
              paddingHorizontal: 10,
              borderTopRightRadius: 20,
              borderBottomRightRadius: 20,
              borderWidth: 2,
              justifyContent: 'center',
              borderColor: '#0F6880',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text
              style={{ color: '#0F6880', fontWeight: 'bold', fontSize: 16 }}>
              {currency(parseFloat(currentRewardAmount))} (
              {ordinalFormat(currentRewardDate)}){' '}
            </Text>
            <Text style={{ color: '#0F6880', fontSize: 16 }}>Monthly</Text>
          </View>
        </View>
        {currentAllowanceOn == true ? (
        <TouchableOpacity
          style={{
            alignSelf: 'center',
            marginTop: 10,
            width: '60%',
            borderRadius: 50,
            padding: 6,
            alignItems: 'center',
            backgroundColor: '#F44336',
          }}
          onPress={handleCancelAllowance}>
          <Text style={{ color: 'white' }}>Cancel Allowance</Text>
        </TouchableOpacity>
        ) : (
           <View
            style={{
              alignSelf: 'center',
              marginTop: 10,
              width: '60%',
              borderRadius : 20,
              padding: 6,
              alignItems: 'center',
              backgroundColor: '#0F6880',
            }}>
            <Text style={{ color: 'white' }}>Auto allowance is off</Text>
            </View>
        )}
      </View>

      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 20,
            alignSelf: 'flex-start',
            marginVertical: 10,
          }}>
          Enter Amount
        </Text>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 5,
            borderBottomColor: 'rgba(0,0,0,0.4)',
            borderBottomWidth: 1,
            marginBottom: 20,
          }}>
          <Text
            style={{
              ...styles.headerText,
              color: rewardAmount ? '#0F6880' : '#7dafbf',
              marginLeft: 0,
              fontSize: 20,
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            $
          </Text>
          <TextInput
            style={{
              ...styles.headerText,
              color: '#0F6880',
              marginLeft: 0,
              fontSize: 20,
              height:50,
              flex: 1,
              fontWeight: 'bold',
            }}
            placeholder={'0'}
            placeholderTextColor="#7dafbf"
            keyboardType="numeric"
            value={rewardAmount}
            onChangeText={(v) => setRewardAmount(v.replace(/[^0-9]/g, ''))}
          />
        </View>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 16,
            alignSelf: 'flex-start',
            marginLeft: 0,
            marginTop: 10,
            marginBottom: 10,
            fontWeight: 'bold',
          }}>
          Or, select amount below
        </Text>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('100')}
            style={styles.tiles}>
            <AmountCard title="$ 100" iconName="one-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('200')}
            style={styles.tiles}>
            <AmountCard title="$ 200" iconName="some-coins" />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('300')}
            style={styles.tiles}>
            <AmountCard title="$ 300" iconName="lotof-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('500')}
            style={styles.tiles}>
            <AmountCard title="$ 500" iconName="money-bag" />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{ ...styles.headerText, fontSize: 17, marginVertical: 10 }}>
          Select date for your Child's allowance.
        </Text>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'baseline',
          }}>
          <TopUpBalanceOption
            data={dataDate}
            onValueChange={(val) => setSelectedDate(val)}
            modalTitle="Select date"
          />
          <Text
            style={{
              ...styles.headerText,
              fontSize: 17,
              marginVertical: 10,
            }}>
            of every month
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={{
          width: '80%',
          alignSelf: 'center',
          marginVertical: 10,
          backgroundColor: '#0F6880',
          padding: 16,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={handleChangeAllowance}>
        <Text style={{ color: 'white', fontSize: 18 }}>Update Allowance</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TopUpBalanceAllowance;

const styles = StyleSheet.create({
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  tiles: { width: '50%' },
});

const dataDate = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  26,
  27,
  28,
  29,
  30,
];

const dataDay = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];
