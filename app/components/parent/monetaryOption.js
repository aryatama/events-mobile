import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Switch,
} from 'react-native';
import { AmountCard, Options, OptionsNonMonetory } from '../parent';

const monetaryOption = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [rewardName, setRewardName] = useState('Reward Name');
  const [isMonetary, setIsMonetary] = useState(true);
  const [rewardAmount, setRewardAmount] = useState('');

  const toggleSwitch = () => {
    /* setIsMonetary((previousState) => {
      !previousState;
      return props.setIsMonetaryReward(isMonetary);
    }); */
    setIsMonetary(!isMonetary);
  };

  useEffect(() => {
    props.setIsMonetaryReward(isMonetary);
  }, [isMonetary]);

  useEffect(() => {
    props.setIsMonetaryReward(true);
  }, []);

  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        paddingHorizontal: 40,
        marginVertical: 10,
        backgroundColor: '#EAF7FE',
      }}>
      
      <View
        style={{
          width: '100%',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginBottom: 20,
        }}>
        <Text
          style={{
            ...styles.nameText,
            fontSize: 20,
            fontWeight: 'normal',
            alignSelf: 'flex-start',
            marginLeft: 0,
          }}>
          {props.title}
        </Text>
        {/* <Switch
          trackColor={{ false: '#eaf7fe', true: '#4fd262' }}
          thumbColor={'white'}
          ios_backgroundColor="#eaf7fe"
          onValueChange={toggleSwitch}
          value={isMonetary}
        />
       */}
        <Options
        items={['Monetary', 'Non-Monetary']}
        onValueChange={(val) => {
          if (val == 'Monetary') {
            setIsMonetary(true);
          } else setIsMonetary(false);
        }}
      />
      </View>
     
      {!isMonetary ? (
        <OptionsNonMonetory
          onValueChange={(val) => props.setRewardName(val)}
          modalTitle="Reward"
          setRewardName={props.setRewardName}
        />
      ) : (
        <>
          <Text
            style={{
              ...styles.headerText,
              fontSize: 20,
              alignSelf: 'flex-start',
              marginVertical: 10,
            }}>
            Reward Amount
          </Text>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
              borderBottomColor: 'rgba(0,0,0,0.4)',
              borderBottomWidth: 1,
              marginBottom: 20,
            }}>
            <Text
              style={{
                ...styles.headerText,
                color: rewardAmount ? '#0F6880' : '#7dafbf',
                marginLeft: 0,
                fontSize: 20,
                alignSelf: 'center',
                fontWeight: 'bold',
              }}>
              $
            </Text>
            <TextInput
              style={{
                ...styles.headerText,
                height:50,
                color: '#0F6880',
                marginLeft: 0,
                fontSize: 20,
                flex: 1,
                fontWeight: 'bold',
              }}
              placeholder={'0'}
              placeholderTextColor="#7dafbf"
              keyboardType="numeric"
              value={rewardAmount}
              onChangeText={(v) => {
                setRewardAmount(v);
                props.setRewardAmount(v);
              }}
            />
          </View>
          <Text
            style={{
              ...styles.headerText,
              fontSize: 16,
              fontWeight: '600',
              alignSelf: 'flex-start',
              marginLeft: 0,
              marginTop: 10,
              marginBottom: 10,
            }}>
            Or, quick fill with
          </Text>
          <View
            style={{
              width: '100%',
              paddingVertical: 5,
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => {
                setRewardAmount('5');
                props.setRewardAmount('5');
              }}
              style={styles.tiles}>
              <AmountCard title="$5" iconName="one-coins" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setRewardAmount('10');
                props.setRewardAmount('10');
              }}
              style={styles.tiles}>
              <AmountCard title="$10" iconName="some-coins" />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '100%',
              paddingVertical: 5,
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => {
                setRewardAmount('15');
                props.setRewardAmount('15');
              }}
              style={styles.tiles}>
              <AmountCard title="$15" iconName="lotof-coins" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setRewardAmount('20');
                props.setRewardAmount('20');
              }}
              style={styles.tiles}>
              <AmountCard title="$20" iconName="money-bag" />
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
};

export default monetaryOption;

const styles = StyleSheet.create({
  nameText: {
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  tiles: { width: '50%' },
});
