import { useNavigation } from '@react-navigation/core';
import React, { useContext, useEffect } from 'react';
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import parentContext from '../../context/parent/parentContext';

const NotificationIcon = () => {
  const navigation = useNavigation();

  const ParentContext = useContext(parentContext);
  useEffect(() => {
    ParentContext.refetchNotifications();
   
    return () => {};
  }, []);

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('NotificationsScreen')}>
      <Image
        style={{ margin: 10, resizeMode: 'center' }}
        source={
          ParentContext.notifications?.filter(
            (notification) => notification.status === '0',
          ).length > 0
            ? require('../../../assets/icons/parent/notification.png')
            : require('../../../assets/icons/parent/notification-all-read.png')
        }
      />
    </TouchableOpacity>
  );
};

export default NotificationIcon;
