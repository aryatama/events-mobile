import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';

const OnlyZimbleCard = (props) => {
  const [imgSrc, setImg] = useState('');

  useEffect(() => {
    if (props.cardColor == 'zimbl2mcpcard') {
      setImg(require('../../../assets/images/app/Blue_Card.png'));
    } else if (props.cardColor == 'zimblemcpcard') {
      setImg(require('../../../assets/images/app/Green_Card.png'));
    } else {
      setImg(require('../../../assets/images/app/Purple_Card.png'));
    }
  }, []);

  const handleActivate = () => {
    if (props.cardActiveStatus === '0') {
      props.onPressActivate();
    } else props.onPressUnlock();
  };

  function cc_format(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    var matches = v.match(/\d{4,16}/g);
    var match = (matches && matches[0]) || '';
    var parts = [];

    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
      return parts.join('  ');
    } else {
      return value;
    }
  }
  return (
    <View style={styles.container}>
      {props.cardActiveStatus === '0' ? (
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.imageCard}>
            <Image
              style={{ ...styles.imageCard }}
              resizeMode="contain"
              source={require('../../../assets/images/app/disabled_card.png')}
            />
            <View style={styles.cardNameContainer}>
              <Text style={styles.cardText}>{cc_format(props.matchMoveWalletCard)}</Text>
              <Text style={styles.cardText}>{props.firstName}</Text>
            </View>
          </View>
          {/* <TouchableOpacity onPress={handleActivate} style={styles.buttonCon}>
            <Icon name="launch" size={30} color="white" />
            <Text style={{ fontSize: 18, color: 'white', marginLeft: 10 }}>
              Activate your card
            </Text>
          </TouchableOpacity> */}
        </View>
      ) : props.temporaryCardStatus === '0' ? (
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.imageCard}>
            <Image
              style={{ ...styles.imageCard }}
              resizeMode="contain"
              source={require('../../../assets/images/app/disabled_card.png')}
            />
            <View style={styles.cardNameContainer}>
              <Text style={styles.cardText}>{cc_format(props.matchMoveWalletCard)}</Text>
              <Text style={styles.cardText}>{props.firstName}</Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={handleActivate}
            style={{ ...styles.buttonCon, backgroundColor: '#F44336' }}>
            <Icon name="lock" size={30} color="white" />
            <Text style={{ fontSize: 18, color: 'white', marginLeft: 10 }}>
              Account Locked
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.imageCard}>
          <Image
            style={{ ...styles.imageCard }}
            resizeMode="contain"
            source={imgSrc}
          />
          <View style={styles.cardNameContainer}>
            <Text style={styles.cardText}>{cc_format(props.matchMoveWalletCard)}</Text>
            <Text style={styles.cardText}>{props.firstName}</Text>
          </View>
        </View>
      )}
    </View>
  );
};

export default OnlyZimbleCard;

OnlyZimbleCard.propTypes = {
  cardActiveStatus: PropTypes.string,
  temporaryCardStatus: PropTypes.string,
  firstName: PropTypes.string,
  onPressActivate: PropTypes.func,
  onPressUnlock: PropTypes.func,
};

let deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  buttonCon: {
    padding: 8,
    paddingHorizontal: 23,
    flexDirection: 'row',
    alignSelf: 'center',
    borderRadius: 50,
    backgroundColor: '#25C1E7',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardText: { color: 'white', fontSize: 16 },
  cardNameContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    padding: 30,
    justifyContent: 'flex-end',
  },
  container: { width: '100%', justifyContent: 'center', alignItems: 'center' },
  imageProfile: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  imageCard: {
    width: '100%',
    height: (deviceWidth * 4.5) / 10,
    resizeMode: 'contain',
  },
  textColor: {
    color: '#0F6880',
  },
});
