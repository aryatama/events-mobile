import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import { Options } from '.';
import { shadowIOS } from '../../style/parent/shadowIOS';

const OptionTask = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [isDateShow, setIsDateShow] = useState(false);
  const [taskFrequency, setTaskFrequency] = useState('Once');
  const [dueDate, setDueDate] = useState(
    moment(new Date()).format('D MMM YYYY'),
  );
  const [date, setDate] = useState(new Date());

  const onDateChange = (date) => {
    let _date = moment(date).format('D MMM YYYY');
    setDate(date);
    setDueDate(_date);
    props.setDueDate(_date);
  };

  const options = ['Once', 'Daily', 'Weekly', 'Monthly'];
  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        paddingHorizontal: 40,
        marginVertical: 10,
        backgroundColor: '#EAF7FE',
      }}>
      <Text
        style={{
          ...styles.nameText,
          fontSize: 20,
          fontWeight: 'normal',
          alignSelf: 'flex-start',
          marginLeft: 0,
          marginBottom: 20,
        }}>
        {props.title}
      </Text>
      <Options
        items={options}
        onValueChange={(val) => props.setIsRepeat(val === 'Once' ? '0' : '1')}
        modalTitle="Time Period"
        isMoney={false}
      />
      <Text
        style={{
          ...styles.nameText,
          fontSize: 20,
          fontWeight: 'normal',
          alignSelf: 'flex-start',
          marginLeft: 0,
          marginVertical: 20,
        }}>
        {props.dueTitle}
      </Text>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
          paddingHorizontal: 20,
          borderRadius: 14,
          backgroundColor: 'white',
          elevation: 2,
          ...shadowIOS
        }}>
        <View style={{ flex: 1 }}>
          <Text
            style={{
              ...styles.nameText,
              color: 'black',
              marginLeft: 0,
              fontSize: 20,
              fontWeight: 'normal',
            }}>
            {dueDate}
          </Text>
        </View>
        <TouchableOpacity onPress={() => setIsDateShow(!isDateShow)}>
          <Icon name={isDateShow ? 'expand-less' : 'expand-more'} size={40} />
        </TouchableOpacity>
      </View>

      {isDateShow && (
        <DatePicker mode={'date'} date={date} onDateChange={onDateChange} />
      )}
    </View>
  );
};

export default OptionTask;

const styles = StyleSheet.create({
  nameText: {
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 20,
  },
});
