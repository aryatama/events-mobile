import React, { useState, useContext } from 'react';
import { AmountCard, LoadingScreen } from '.';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { postWalletFunds } from '../../api/parent';
import Snackbar from 'react-native-snackbar';
import parentContext from '../../context/parent/parentContext';

// import { PaymentCardTextField } from 'tipsi-stripe';
import { Pulse } from 'react-native-animated-spinkit';
import { shadowIOS } from '../../style/parent/shadowIOS';
import currency from '../../utils/parent/currencyFormat';

const TransferFundsTransfer = (props) => {
  const [rewardAmount, setRewardAmount] = useState('');
  const [cardName, setCardName] = useState({ value: '', status: true });
  // const [cardNumber, setCardNUmber] = useState({ value: '', status: true });
  // const [expiryDate, setExpiryDate] = useState({ value: '', status: true });
  // const [cvv, setCvv] = useState({ value: '', status: true });
  const mutationPostWalletFunds = postWalletFunds();

  const ParentContext = useContext(parentContext);

  const [creditCard, setCreditCard] = useState({
    valid: true,
    number: '',
    expMonth: '',
    expYear: '',
    cvc: '',
  });
  const handleFieldParamsChange = (valid, params) => {
    setCreditCard({
      valid: valid,
      number: params.number || '-',
      expMonth: params.expMonth || '-',
      expYear: params.expYear || '-',
      cvc: params.cvc || '-',
    });
    console.log(`
      Valid: ${valid}
      Number: ${params.number || '-'}
      Month: ${params.expMonth || '-'}
      Year: ${params.expYear || '-'}
      CVC: ${params.cvc || '-'}
    `);
  };

  function handleCheckName() {
    if (cardName.value.length === 0) {
      setCardName({ ...cardName, status: false });
    } else setCardName({ ...cardName, status: true });
  }

  const snackbar = (text) =>
    Snackbar.show({
      text: text,
      duration: 4000,
      backgroundColor: 'red',
      action: {
        text: 'CLOSE',
        textColor: 'white',
        onPress: () => true,
      },
    });

  const handleSubmit = () => {
    if (cardName.value.length === 0) {
      return snackbar('Please enter valid card holder name');
    } else if (!creditCard.valid) {
      return snackbar('Please enter valid Number, Expiry & CVV');
    } else if (rewardAmount.length === 0) {
      return snackbar('Please enter amount of money');
    } else handleSubmitTransferMoney();
  };

  const handleSubmitTransferMoney = () => {
    mutationPostWalletFunds.mutate(
      {
        amount: rewardAmount,
        description: `${rewardAmount} Reward Amount.`,
        cardNumber: creditCard.number,
        cardExpiryMonth: creditCard.expMonth,
        cardExpiryYear: creditCard.expYear,
        cardCVC: creditCard.cvc,
      },
      {
        onSuccess: (message) => {
          ParentContext.refetchBalance();
          setRewardAmount('');
          setCardName({ value: '', status: true });
          // setCardNUmber({ value: '', status: true });
          // setExpiryDate({ value: '', status: true });
          // setCvv({ value: '', status: true });
          Snackbar.show({
            text: message.data.message,
            duration: Snackbar.LENGTH_LONG,
          });
          props.onBackToDashboard();
        },
        onError: (error) =>
          Snackbar.show({
            text: error.message,
            duration: Snackbar.LENGTH_INDEFINITE,
            backgroundColor: 'red',
            action: {
              text: 'CLOSE',
              textColor: 'white',
              onPress: () => true,
            },
          }),
      },
    );
  };

  if (mutationPostWalletFunds.isLoading) {
    return (
      <View
        style={{
          width: '100%',
          height: 600,
          backgroundColor: '#EAF7FE',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Pulse size={60} color="#24c1e7" />
      </View>
    );
  }

  return (
    <View
      style={{
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EAF7FE',
      }}>
      <View style={{ width: '90%', marginVertical: 10 }}>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 16,
            marginBottom: 5,
            lineHeight: 25,
          }}>
          Use your
          <Text
            style={{
              ...styles.headerText,
              fontWeight: 'bold',
              fontSize: 16,
              marginBottom: 5,
            }}>
            {' '}
            Credit/Debit Card
          </Text>{' '}
          to transfer to your Zimble Account. Complete the details below.
        </Text>
      </View>

      <View style={{ width: '90%', marginVertical: 10 }}>
        <View
          style={{
            width: '100%',
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: '#0F6880',
            }}>
            Card Holder Name
          </Text>
          <TextInput
            style={styles.textInput}
            placeholder="Name"
            placeholderTextColor="rgba(15,104,128,0.5)"
            value={cardName.value}
            onChangeText={(v) => setCardName({ ...cardName, value: v })}
            onEndEditing={() => handleCheckName()}
          />
        </View>
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {!cardName.status && '*Please enter valid card holder name'}
        </Text>
        <Text
          style={{
            fontSize: 16,
            marginBottom: 20,
            fontWeight: 'bold',
            color: '#0F6880',
          }}>
          Card Number, Expiration date & CVV
        </Text>
        <View style={styles.paymentField}>
          {/* <PaymentCardTextField
            style={styles.field}
            numberPlaceholder={'Credit Card Number'}
            expirationPlaceholder={'MM/YY'}
            cvcPlaceholder={'CVV'}
            onParamsChange={handleFieldParamsChange}
          /> */}
        </View>
        <Text style={{ color: 'red' }}>
          {' '}
          {!creditCard.valid && '*Please enter valid Number, Expiry & CVV'}
        </Text>
      </View>

      <View style={{ width: '90%' }}>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 20,
            alignSelf: 'flex-start',
            marginVertical: 10,
          }}>
          Enter Amount
        </Text>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 5,
            borderBottomColor: 'rgba(0,0,0,0.4)',
            borderBottomWidth: 1,
            marginBottom: 20,
          }}>
          <Text
            style={{
              ...styles.headerText,
              color: rewardAmount ? '#0F6880' : '#7dafbf',
              marginLeft: 0,
              fontSize: 20,
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            $
          </Text>
          <TextInput
            style={{
              ...styles.headerText,
              color: '#0F6880',
              marginLeft: 0,
              height:50,
              fontSize: 20,
              flex: 1,
              fontWeight: 'bold',
            }}
            placeholder={'0'}
            placeholderTextColor="#7dafbf"
            keyboardType="numeric"
            value={rewardAmount}
            onChangeText={(v) => setRewardAmount(v)}
          />
        </View>
        <Text
          style={{
            ...styles.headerText,
            fontSize: 16,
            fontWeight: '600',
            alignSelf: 'flex-start',
            marginLeft: 0,
            marginTop: 10,
            marginBottom: 10,
          }}>
          Or, quick fill with
        </Text>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('100')}
            style={styles.tiles}>
            <AmountCard title="$ 100" iconName="one-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('200')}
            style={styles.tiles}>
            <AmountCard title="$ 200" iconName="some-coins" />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            paddingVertical: 5,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => setRewardAmount('300')}
            style={styles.tiles}>
            <AmountCard title="$ 300" iconName="lotof-coins" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setRewardAmount('500')}
            style={styles.tiles}>
            <AmountCard title="$ 500" iconName="money-bag" />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{ width: '85%' }}>
        {/* <Text
          style={{
            ...styles.headerText,
            fontSize: 20,
            alignSelf: 'flex-start',
            marginVertical: 20,
            fontWeight: 'bold',
          }}>
          Credit Card Details
        </Text>

        <View style={styles.containerCC}>
          <View style={{ flex: 2 }}>
            <View style={styles.fieldCC}>
              <Text style={styles.textFieldCC}>Valid</Text>
            </View>
          </View>
          <View style={styles.valueCC}>
            <Text style={styles.textValueCC}>
              {creditCard.valid.toString()}
            </Text>
          </View>
        </View>
        <View style={styles.containerCC}>
          <View style={{ flex: 2 }}>
            <View style={styles.fieldCC}>
              <Text style={styles.textFieldCC}>Number</Text>
            </View>
          </View>
          <View style={styles.valueCC}>
            <Text style={styles.textValueCC}>{creditCard.number}</Text>
          </View>
        </View>
        <View style={styles.containerCC}>
          <View style={{ flex: 2 }}>
            <View style={styles.fieldCC}>
              <Text style={styles.textFieldCC}>Month</Text>
            </View>
          </View>
          <View style={styles.valueCC}>
            <Text style={styles.textValueCC}>{creditCard.expMonth}</Text>
          </View>
        </View>
        <View style={styles.containerCC}>
          <View style={{ flex: 2 }}>
            <View style={styles.fieldCC}>
              <Text style={styles.textFieldCC}>Year</Text>
            </View>
          </View>
          <View style={styles.valueCC}>
            <Text style={styles.textValueCC}>{creditCard.expYear}</Text>
          </View>
        </View>
        <View style={styles.containerCC}>
          <View style={{ flex: 2 }}>
            <View style={styles.fieldCC}>
              <Text style={styles.textFieldCC}>CVV</Text>
            </View>
          </View>
          <View style={styles.valueCC}>
            <Text style={styles.textValueCC}>{creditCard.cvc}</Text>
          </View>
        </View> */}

        {/* 
        <View
          style={{
            width: '100%',
            borderBottomColor: '#A1A1A1',
            borderBottomWidth: 2,
          }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#A1A1A1' }}>
            CARD NUMBER
          </Text>
          <TextInput
            keyboardType="number-pad"
            style={{ color: '#0F6880', width: '100%', fontSize: 16 }}
            placeholder="00000000000"
            placeholderTextColor="rgba(15,104,128,0.5)"
            value={cardNumber.value}
            onChangeText={(v) => handleEnterCardNumber(v)}
            onEndEditing={() => handleCheckCardNumber()}
          />
        </View>
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {!cardNumber.status && '*Please enter valid card number'}
        </Text>

        <View
          style={{
            width: '100%',
            borderBottomColor: '#A1A1A1',
            borderBottomWidth: 2,
          }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#A1A1A1' }}>
            EXPIRY DATE
          </Text>
          <TextInput
            keyboardType="phone-pad"
            style={{ color: '#0F6880', width: '100%', fontSize: 16 }}
            placeholder="MM/YY"
            placeholderTextColor="rgba(15,104,128,0.5)"
            value={expiryDate.value}
            onChangeText={(v) => handleEnterExpiryDate(v)}
            onEndEditing={() => handleCheckExpiryDate()}
          />
        </View>
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {!expiryDate.status && '*Please enter valid expiry date'}
        </Text>

        <View
          style={{
            width: '100%',
            borderBottomColor: '#A1A1A1',
            borderBottomWidth: 2,
          }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#A1A1A1' }}>
            CVV
          </Text>
          <TextInput
            keyboardType="number-pad"
            style={{ color: '#0F6880', width: '100%', fontSize: 16 }}
            placeholder="0000"
            placeholderTextColor="rgba(15,104,128,0.5)"
            value={cvv.value}
            onChangeText={(v) => handleEnterCVV(v)}
            onEndEditing={() => handleCheckCVV()}
          />
        </View>
        <Text style={{ color: 'red', marginBottom: 8 }}>
          {' '}
          {!cvv.status && '*Please enter valid CVV number'}
        </Text> */}
      </View>

      <TouchableOpacity
        style={{
          width: '80%',
          alignSelf: 'center',
          marginVertical: 30,
          backgroundColor: '#0F6880',
          padding: 16,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={handleSubmit}>
        <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
          Submit
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TransferFundsTransfer;

const styles = StyleSheet.create({
  headerText: {
    alignSelf: 'flex-start',
    color: '#0F6880',
  },
  tiles: { width: '50%' },
  label: {
    color: 'black',
    fontSize: 12,
  },
  input: {
    fontSize: 16,
    color: 'black',
  },
  field: {
    width: '100%',
    color: '#449aeb',
    borderRadius: 5,
    elevation: 2,
    ...shadowIOS,
    backgroundColor: 'white',
  },
  fieldCC: { paddingVertical: 5 },
  textFieldCC: { fontSize: 14, fontWeight: 'bold', color: '#0F6880' },
  valueCC: {
    paddingVertical: 6,
    flex: 6,
    paddingHorizontal: 20,
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: '#75FF8A',
    elevation: 1,
    ...shadowIOS,
  },
  textValueCC: { fontSize: 14, color: '#0F6880' },
  containerCC: {
    width: '100%',
    marginTop: 8,
    flexDirection: 'row',
  },
  paymentField: {
    width: '100%',
    height: 50,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS,
    paddingHorizontal: 18,
    height: 50,
    color: 'black',
    fontSize: 16,
    marginVertical: 10,
  },
});

// function handleCheckCardNumber() {
//   if (cardNumber.value.length < 10) {
//     setCardNUmber({ ...cardNumber, status: false });
//   } else setCardNUmber({ ...cardNumber, status: true });
// }
// function handleCheckExpiryDate() {
//   if (expiryDate.value.length < 5) {
//     setExpiryDate({ ...expiryDate, status: false });
//   } else setExpiryDate({ ...expiryDate, status: true });
// }

// function handleCheckCVV() {
//   if (cvv.value.length < 3) {
//     setCvv({ ...cvv, status: false });
//   } else setCvv({ ...cvv, status: true });
// }

// function handleEnterCardNumber(v) {
//   let data = { ...cardNumber, value: v.replace(/[^0-9]/g, '') };
//   if (data.value.length < 17) {
//     setCardNUmber(data);
//   }
// }
// function handleEnterExpiryDate(v) {
//   let data = { ...expiryDate, value: v.replace(/[^0-9/]/g, '') };
//   if (data.value.length < 6) {
//     if (data.value.length === 2 && !data.value.includes('/')) {
//       setExpiryDate({ ...data, value: data.value + '/' });
//     } else setExpiryDate(data);
//   }
// }
// function handleEnterCVV(v) {
//   let data = { ...cvv, value: v.replace(/[^0-9]/g, '') };
//   if (data.value.length < 4) {
//     setCvv(data);
//   }
// }
