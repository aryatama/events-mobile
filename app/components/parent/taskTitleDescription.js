import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';
import { shadowIOS } from '../../style/parent/shadowIOS';
import OptionsTaskTitle from './optionTaskTitle';

const TaskTitleDescription = (props) => {
  return (
    <View
      style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        paddingHorizontal: 40,
        backgroundColor: '#EAF7FE',
      }}>
      <Text
        style={{
          ...styles.nameText,
          fontSize: 20,
          fontWeight: 'normal',
          alignSelf: 'flex-start',
          marginLeft: 0,
          marginBottom: 10,
        }}>
        {props.title}
      </Text>
      <OptionsTaskTitle setTaskName={props.setTaskTitle} onValueChange={(val) => props.setTaskTitle(val)} modalTitle="Task"   />
    
      <Text
        style={{
          ...styles.nameText,
          fontSize: 20,
          fontWeight: 'normal',
          alignSelf: 'flex-start',
          marginLeft: 0,
          marginTop:10,
          marginBottom: 20,
        }}>
        {props.description}
      </Text>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          padding: 10,
          paddingHorizontal: 20,
          borderRadius: 14,
          backgroundColor: 'white',
          elevation: 2,
          ...shadowIOS,
          height: 120,
        }}>
        <View style={{ flex: 1 }}>
          <TextInput
            style={{
              ...styles.nameText,
              color: 'black',
              marginLeft: 0,
              fontSize: 20,
              fontWeight: 'normal',
              textAlignVertical:'top'

            }}
            placeholder="Type your task description"
            placeholderTextColor="grey"
            multiline={true}
            numberOfLines={5}
            value={props.taskDescription}
            onChangeText={(text) => props.setTaskDescription(text)}
          />
        </View>
      </View>
    </View>
  );
};

export default TaskTitleDescription;

const styles = StyleSheet.create({
  nameText: {
    color: '#0F6880',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 20,
  },
});
