import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList } from 'react-native';
import TransactionCard from './transactionCard';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Flow } from 'react-native-animated-spinkit';
import { getSomeTransactions } from '../../api/parent';

const AllTransaction = (props) => {
  const [pageNum, setpageNum] = useState(1);
  const [transactions, setTransactions] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  const [isEnd, setisEnd] = useState(false);

  useEffect(() => {
    if (pageNum === 1) {
      getChildTransactions(props.childId, 1);
    }
  }, []);

  const getChildTransactions = async (childID, pageNo) => {
    try {
      setisLoading(true);
      const response = await getSomeTransactions(childID, pageNo);
      let data = response.data.details.transactionsData;
      if (data.length === 0) {
        setisEnd(true);
      }
      setTransactions([...transactions, ...data]);
      setisLoading(false);
    } catch (error) {
      console.log(error.message);
      setisLoading(false);
    }
  };

  const handleLoadMore = (pageNo) => {
    if (isLoading) {
      return;
    }
    if (isEnd) {
      return;
    }
    setpageNum(pageNo);
    getChildTransactions(props.childId, pageNo);
  };

  if (!props.childId) {
    return null;
  }

  if (transactions.length === 0 && !isLoading)
    return (
      <Text
        style={{
          ...styles.textColor,
          fontSize: 20,
          marginVertical: 30,
        }}>
        This child has no transaction history
      </Text>
    );
  if (transactions.length === 0 && isLoading)
    return (
      <View
        style={{
          backgroundColor: '#eaf7fe',
          alignItems: 'center',
          width: '100%',
          paddingVertical: 20,
        }}>
        <Flow size={40} color="#24c1e7" />
      </View>
    );
  return (
    <View style={{ width: '100%' }}>
      <View>
        <Text
          style={{
            ...styles.textColor,
            fontSize: 20,
            marginTop: 10,
          }}>
          Transactions History
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 20,
        }}>
        <View style={styles.transIndicatorCon}>
          <View style={styles.subTransCon}>
            <Icon name="caret-up" size={30} color="#0F8020" />
            <Text style={{ color: '#0F6880' }}>Income</Text>
          </View>
          <View style={styles.subTransCon}>
            <Icon name="caret-down" size={30} color="#F44336" />
            <Text style={{ color: '#0F6880' }}>Spending</Text>
          </View>
        </View>
        <View style={{ width: '100%', height: (deviceHeight * 6) / 10 }}>
          <FlatList
            nestedScrollEnabled={true}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 30 }}
            data={transactions}
            onEndReachedThreshold={0.8}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
              <TransactionCard
                message={item.message}
                type={moment(item.date).format('D MMM YYYY, h:mm A')}
                amount={item.amount}
                transactionId={item.transactionId}
                indicator={item.indicator}
              />
            )}
            refreshing={isLoading}
            onEndReached={() => handleLoadMore(pageNum + 1)}
            ListFooterComponent={() =>
              isLoading ? (
                <View
                  style={{
                    backgroundColor: '#eaf7fe',
                    alignItems: 'center',
                    width: '100%',
                    paddingVertical: 20,
                  }}>
                  <Flow size={40} color="#24c1e7" />
                </View>
              ) : null
            }
          />
        </View>
      </View>
    </View>
  );
};

export default AllTransaction;

AllTransaction.propTypes = {
  childId: PropTypes.string,
  onPressMore: PropTypes.func,
  fromDashboard: PropTypes.bool,
};
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  transIndicatorCon: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  subTransCon: { flexDirection: 'row', alignItems: 'center' },
  profileView: {
    height: '35%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  textColor: {
    color: '#0F6880',
  },
  ScrollView: {
    width: '100%',
  },
});
