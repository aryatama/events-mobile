import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Modal,
  Pressable,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { shadowIOS } from '../../style/parent/shadowIOS';

const OptionsNonMonetory = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [data, setData] = useState(dataOptions);
  const [selecteditem, setSelecteditem] = useState('');

  function handlePick(item) {
    setSelecteditem(item);
    props.onValueChange(item);
    setIsShow(false);
  }

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        {selecteditem.length === 0 ? (
          <Text style={{ ...styles.nameText, color: 'grey', fontSize: 18 }}>
            Select reward
          </Text>
        ) : (
          <Text style={{ ...styles.nameText }}>{selecteditem}</Text>
        )}
      </View>
      <TouchableOpacity onPress={() => setIsShow(!isShow)}>
        <Icon name={isShow ? 'expand-less' : 'expand-more'} size={40} />
      </TouchableOpacity>

      <Modal animationType="fade" transparent={true} visible={isShow}>
        <Pressable style={styles.pressable} onPress={() => {}}>
          <View
            style={{
              width: '90%',
              borderRadius: 10,
              backgroundColor: 'white',
              padding: 10,
              paddingBottom: 0,
            }}>
            <Text style={styles.textModalTitle}>
              Select {props.modalTitle} :
            </Text>

            <View
              style={{ width: '90%', alignSelf: 'center', marginBottom: 10 }}>
              <Text style={{ color: 'grey', fontSize: 18, marginBottom: 4 }}>
                Reward name
              </Text>
              <View style={{ ...styles.container, padding: 0, height:50 }}>
                <TextInput
                  placeholder="Type Reward"
                  style={{ ...styles.nameText, width: '100%' }}
                  value={selecteditem}
                  onChangeText={(v) => {
                    props.setRewardName(v);
                    return setSelecteditem(v);
                  }}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: '80%',
                  alignSelf: 'center',
                  marginVertical: 10,
                  backgroundColor: '#0F6880',
                  padding: 16,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => setIsShow(false)}>
                <Text
                  style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>
                  Select Reward
                </Text>
              </TouchableOpacity>

              <Text
                style={{
                  alignSelf: 'center',
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: 'black',
                  marginVertical: 10,
                }}>
                Or select one of the following
              </Text>
            </View>
            <ScrollView style={styles.scrollview} showsVerticalScrollIndicator={false}>
              {data?.map((item) => {
                return (
                  <TouchableOpacity key ={item.rewardName}
                    style={styles.optionStyle}
                    onPress={() => handlePick(item.rewardName)}>
                    <Icon name={item.iconName} size={30} />
                    <View style={{ width: '90%' }}>
                      <Text style={{ ...styles.nameText, marginLeft: 10 }}>
                        {item.rewardName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </View>
  );
};

OptionsNonMonetory.propTypes = {
  onValueChange: PropTypes.func,
  setRewardName: PropTypes.func,
  modalTitle: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.any),
};

export default OptionsNonMonetory;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS
  },
  nameText: {
    color: 'black',
    marginLeft: 0,
    fontSize: 20,
    fontWeight: 'normal',
  },
  optionStyle: {
    width: '100%',
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',

    borderRadius: 14,
    backgroundColor: '#EAF7FE',
    marginBottom: 16,
  },
  pressable: {
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollview: {
    width: '90%',
    alignSelf: 'center',
    marginBottom: 20,
    maxHeight: (deviceHeight * 3) / 10,
  },
  textModalTitle: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

const dataOptions = [
  {
    rewardName: 'An Event',
    iconName: 'airport-shuttle',
  },
  {
    rewardName: 'A Favorite Movie',
    iconName: 'all-inbox',
  },
  {
    rewardName: 'Free Leisure Time',
    iconName: 'anchor',
  },
  {
    rewardName: 'Time with Friend',
    iconName: 'analytics',
  },
  {
    rewardName: 'Extra Time to Play Game',
    iconName: 'all-out',
  },
  {
    rewardName: 'Favorite Food/Dish',
    iconName: 'airport-shuttle',
  },
  {
    rewardName: 'Go To A Concert',
    iconName: 'anchor',
  },
  {
    rewardName: 'Go on a Holiday',
    iconName: 'all-inbox',
  },
  {
    rewardName: 'Computer Privilages',
    iconName: 'analytics',
  },
  {
    rewardName: 'Extra Family Time',
    iconName: 'airport-shuttle',
  },
  {
    rewardName: 'Play Piano/Other Instruments',
    iconName: 'all-out',
  },
];
