import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import { shadowIOS } from '../../style/parent/shadowIOS';

const currencyFormat = (amount) => {
  return amount;
};

const TransactionCard = (props) => {
  return (
    <View key={props.transactionId} style={styles.container}>
      <View style={styles.imageDummy}>
        {props.indicator === 'credit' ? (
          <Icon name="caret-up" size={30} color="#0F8020" />
        ) : (
          <Icon name="caret-down" size={30} color="#F44336" />
        )}
      </View>
      <View style={{ flex: 1, alignItems: 'flex-start' }}>
        <Text style={styles.regularText}>{props.message}</Text>
        <Text style={styles.smallText}>{props.type}</Text>
      </View>
      <Text style={styles.regularText}>
        {props.indicator !== 'credit' ? '-' : '+'} ${currencyFormat(props.amount)}
      </Text>
    </View>
  );
};

TransactionCard.propTypes = {
  message: PropTypes.string,
  amount: PropTypes.number,
  type: PropTypes.string,
  indicator: PropTypes.string,
};
export default TransactionCard;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    elevation: 2,
    ...shadowIOS
  },
  imageDummy: {
    height: 30,
    width: 30,
    marginRight: 10,
    borderRadius: 20,
    backgroundColor: 'white',
  },
  regularText: { color: '#0F6880', fontSize: 16, fontWeight: 'bold' },
  smallText: { color: '#919191', fontSize: 14 },
});
