import React, { useEffect } from 'react';
import { View } from 'react-native';
import Snackbar from 'react-native-snackbar';

const ErrorAPIHandler = (props) => {
  useEffect(() => {
    Snackbar.show({
      text: props.message,
      duration: Snackbar.LENGTH_LONG,
    });
  }, []);

  return <View />;
};

export default ErrorAPIHandler;
