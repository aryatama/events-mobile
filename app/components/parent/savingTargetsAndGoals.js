import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import TransactionCard from './transactionCard';
import { getChildTransactions } from '../../api/parent';

const SavingTargetsAndGoals = (props) => {
  if (!props.childId) {
    return null;
  }
  const {
    isLoading: isLoadingGetChildTransaction,
    error: errorGetChildTransaction,
    data: dataGetChildTransaction,
  } = getChildTransactions(props.childId);

  if (isLoadingGetChildTransaction) return <Text>Loading...</Text>;

  if (errorGetChildTransaction) {
    return <Text>{errorGetChildTransaction.message}</Text>;
  }

  if (dataGetChildTransaction.data.details.transactionData.length === 0) {
    return null;
  }
  return (
    <>
      <Text
        style={{
          ...styles.textColor,
          fontSize: 20,
          fontWeight: 'bold',
          marginTop: 10,
        }}>
        Recent Transaction
      </Text>
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 20,
        }}>
        {dataGetChildTransaction.data.details.transactionData.map((item) => {
          return (
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              key={item.transactionId}>
              <TransactionCard
                message={item.message}
                type={item.type}
                amount={item.amount}
                transactionId={item.transactionId}
              />
            </View>
          );
        })}
      </View>
      <Text
        style={{
          ...styles.textColor,
          fontSize: 14,
          marginVertical: 10,
        }}>
        View More Transaction
      </Text>
    </>
  );
};

export default SavingTargetsAndGoals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileView: {
    height: '35%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  textColor: {
    color: '#0F6880',
  },
  ScrollView: {
    width: '100%',
  },
});
