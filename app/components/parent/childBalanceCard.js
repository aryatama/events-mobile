import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const ChildBalanceCard = (props) => {
  return (
    <View key={props.childId} style={styles.cardContainer}>
      <Text style={styles.balanceText}>{props.totalCardBalance}</Text>
      <Text numberOfLines={2} style={styles.nameText}>{props.firstName} Balance</Text>
    </View>
  );
};

ChildBalanceCard.prototypes = {
  totalCardBalance: PropTypes.number,
  firstName: PropTypes.string,
};

export default ChildBalanceCard;
let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  cardContainer: {
    width: (deviceWidth * 25) / 100,
    aspectRatio: 0.9,
    marginRight: 10,
    borderRadius: 16,
    padding: 8,
    backgroundColor: '#1DC1E6',
    justifyContent: 'space-between',
  },
  balanceText: {
    color: '#0F6880',
    color: 'white',
    alignSelf: 'flex-end',
    fontSize: 16,
    fontWeight: 'bold',
  },
  nameText: { color: '#0F6880', color: 'white', width: '100%', fontSize: 16 },
});
