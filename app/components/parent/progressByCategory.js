import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View } from 'react-native';
import { shadowIOS } from '../../style/parent/shadowIOS';

const ProgressByCategory = () => {
  return (
    <View style={{ width: '100%', marginBottom: 16 }}>
      <View
        style={{ flexDirection: 'row', width: '100%', alignItems: 'center' }}>
        <Icon name="airport-shuttle" color="#0F6880" size={38} />
        <View style={{ flex: 1, paddingHorizontal: 10 }}>
          <Text style={{ ...styles.regularTextSaving, fontWeight: 'normal' }}>
            Transport
          </Text>
        </View>
        <Text style={styles.regularTextSaving}>24%</Text>
      </View>
      <View
        style={{
          width: '100%',
          height: 8,
          backgroundColor: 'white',
          elevation: 1,
          ...shadowIOS,
        }}>
        <View
          style={{
            width: '70%',
            height: 8,
            backgroundColor: '#E15241',
            elevation: 1,
            ...shadowIOS,
          }}></View>
      </View>
    </View>
  );
};

export default ProgressByCategory;

const styles = StyleSheet.create({
  regularTextSaving: { color: '#0F6880', fontSize: 18, fontWeight: 'bold' },
});
