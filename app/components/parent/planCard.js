import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import currency from '../../utils/parent/currencyFormat';

var child3 = require('../../../assets/icons/parent/child3.png');
var child4 = require('../../../assets/icons/parent/child4.png');
var child2 = require('../../../assets/icons/parent/child2.png');

const PlanCard = (props) => {
  return (
    <View style={styles.descButt}>
      <View>
        <Text style={styles.sumChilds}>{props.numOfChild} Childs</Text>
        <Text style={styles.priceText}>
          {currency(props.price)}
          <Text style={{ ...styles.priceText, fontWeight: 'normal' }}>
            /year
          </Text>
        </Text>
       {props.expire &&  <Text style={styles.expireText}>Expire: {props.expire} </Text>}
      </View>
      <View
        style={{
          alignSelf: 'flex-end',
          flexDirection: 'row',
          alignItems: 'flex-end',
        }}>
        <Image
          style={{ width: 60, height: 60 }}
          resizeMode="cover"
          source={
            props.numOfChild === '2'
              ? child2
              : props.numOfChild === '3'
              ? child3
              : child4
          }
        />
      </View>
    </View>
  );
};

export default PlanCard;

PlanCard.propTypes = {
  expire: PropTypes.string,
  price: PropTypes.number,
  numOfChild: PropTypes.number,
};

const styles = StyleSheet.create({
  descButt: {
    backgroundColor: '#0F6880',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginVertical: 10,
    borderRadius: 14,
  },
  sumChilds: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },
  priceText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  expireText: {
    marginTop: 10,
    color: 'white',
    fontSize: 14,
  },
});
