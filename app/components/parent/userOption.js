import React, { useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  Modal,
  Pressable,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import decodedImageURI from '../../utils/parent/decodedImageURI';
import { shadowIOS } from '../../style/parent/shadowIOS';

const UserOption = (props) => {
  const [isShow, setIsShow] = useState(false);
  const [selectedChild, setSelectedChild] = useState(props.childs[0]);

  function handlePick(item) {
    setSelectedChild(item);
    props.onValueChange(item);
    setIsShow(false);
  }

  useEffect(() => {
    let dataChild = props.childs.find(
      (child) => child.childId === props.selectedChild,
    );
    setSelectedChild(dataChild);
  }, []);

  return (
    <View style={{ ...styles.profilePicker }}>
      <TouchableOpacity
        onPress={() => setIsShow(!isShow)}
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
          paddingHorizontal: 20,
          borderRadius: 10,
        }}>
        <Image
          style={{
            width: 50,
            height: 50,
            aspectRatio: 1,
            borderRadius: 100,
          }}
          resizeMode="cover"
          source={{ uri: decodedImageURI(selectedChild.profilePicture) }}
        />
        <View style={{ flex: 1, paddingHorizontal: 10 }}>
          <Text style={styles.nameText}>{selectedChild.firstName}</Text>
        </View>
        <Icon name={isShow ? 'expand-less' : 'expand-more'} size={40} />
      </TouchableOpacity>
      <Modal animationType="fade" transparent={true} visible={isShow}>
        <Pressable
          style={{
            height: deviceHeight,
            backgroundColor: 'rgba(0,0,0,0.2)',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => setIsShow(false)}>
          <View style={{ width: '90%' }}>
            <ScrollView
              style={{
                width: '100%',
                borderRadius: 10,
                backgroundColor: 'white',
                padding: 10,
                paddingBottom: 0,
              }}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  color: '#0F6880',
                  fontSize: 20,
                  fontWeight: 'bold',
                  marginBottom: 18,
                }}>
                Choose your child :
              </Text>

              {props.childs.map((item) => {
                return (
                  <TouchableOpacity
                    key={item.childId}
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      padding: 10,
                      paddingHorizontal: 20,
                      borderRadius: 10,
                      marginBottom: 10,
                      backgroundColor: '#EAF7FE',
                      elevation: 2,
                      ...shadowIOS,
                    }}
                    onPress={() => handlePick(item)}>
                    <Image
                      style={{
                        width: 50,
                        height: 50,
                        aspectRatio: 1,
                        borderRadius: 100,
                      }}
                      resizeMode="cover"
                      source={{ uri: decodedImageURI(item.profilePicture) }}
                    />
                    <View style={{ flex: 1, paddingHorizontal: 10 }}>
                      <Text style={styles.nameText}>{item.firstName}</Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </View>
  );
};

export default UserOption;

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  nameText: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  profilePicker: {
    width: '100%',
    marginVertical: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 2,
    ...shadowIOS,
  },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
});
