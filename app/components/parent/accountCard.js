import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Pressable,
  ScrollView,
  Dimensions,
} from 'react-native';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import currency from '../../utils/parent/currencyFormat';

const AccountCard = (props) => {
  const [isShow, setIsShow] = useState(false);

  const handleCameraPress = (type) => {
    props.onCameraPress(type);
    setIsShow(false);
  };
  const handleShowModal = () => {
    setIsShow(true);
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.imageBack}
        resizeMode="cover"
        source={{ uri: props.coverPicture }}
      />
      <View style={styles.innerCon}>
        <Image
          style={{
            width: 70,
            height: 70,
            aspectRatio: 1,
            borderRadius: 100,
          }}
          resizeMode="cover"
          source={props.profilePicture}
        />
        <View style={{ flex: 1, paddingHorizontal: 18 }}>
          <Text style={styles.name}>{props.name}</Text>
          <Text style={styles.desc}>
            Your Balance : {currency(parseFloat(props.balance))}
          </Text>
        </View>
        <TouchableOpacity onPress={handleShowModal}>
          <MIcon name="camera-alt" size={30} color="white" />
        </TouchableOpacity>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isShow}
       >
        <Pressable style={styles.pressable} onPress={() => setIsShow(false)}>
          <View style={{ width: '100%' }}>
            <ScrollView style={styles.scrollview}>
              <View style={styles.greyLine}></View>
              <TouchableOpacity
                style={styles.optionStyle}
                onPress={() => handleCameraPress('profile')}>
                <View
                  style={{
                    width: '90%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      borderWidth: 2,
                      borderRadius: 50,
                      padding: 5,
                      borderColor: '#0F6880',
                    }}>
                    <MIcon name="camera-alt" size={28} color="#0F6880" />
                  </View>
                  <Text style={{ ...styles.nameText, marginLeft: 12 }}>
                    Profile Photo
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.optionStyle}
                onPress={() => handleCameraPress('cover')}>
                <View
                  style={{
                    width: '90%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      borderWidth: 2,
                      borderRadius: 8,
                      padding: 5,
                      borderColor: '#0F6880',
                      borderStyle: 'dotted',
                    }}>
                    <MIcon name="add" size={28} color="#0F6880" />
                  </View>
                  <Text style={{ ...styles.nameText, marginLeft: 12 }}>
                    Cover Photo
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </Pressable>
      </Modal>
    </View>
  );
};

AccountCard.propTypes = {
  name: PropTypes.string,
  balance: PropTypes.number,
  coverPicture: PropTypes.string,
  profilePicture: PropTypes.string,
  onCameraPress: PropTypes.func,
};

let deviceHeight = Dimensions.get('window').height;

export default AccountCard;

const styles = StyleSheet.create({
  greyLine: {
    width: '20%',
    height: 8,
    alignSelf: 'center',
    borderRadius: 20,
    marginBottom: 10,
    backgroundColor: '#DBDBDB',
  },
  container: {
    width: '100%',
    aspectRatio: 3,
    marginVertical: 10,
  },
  optionStyle: {
    width: '100%',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 20,
    borderRadius: 14,
    marginBottom: 10,
  },
  nameText: {
    color: '#0F6880',
    marginLeft: 0,
    fontSize: 20,
    fontWeight: 'normal',
  },
  imageBack: {
    borderRadius: 16,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  innerCon: {
    flexDirection: 'row',
    borderRadius: 16,
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(15,104,128,0.4)',
    alignItems: 'center',
    padding: 20,
  },
  name: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  desc: { color: 'white', fontSize: 16 },
  imageProfile: { width: 60, height: 60, resizeMode: 'contain' },
  pressable: {
    height: deviceHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: 'white',
    padding: 10,
    paddingBottom: 30,
  },
  textModalTitle: {
    color: '#0F6880',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});
