import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import CustomIcon from '../../utils/parent/icon';

const MenuCard = (props) => {
    return (
        <View style={{ width: deviceWidth*25/100, aspectRatio: 0.9, justifyContent: 'space-between', marginRight: 10, alignItems:'flex-end', borderRadius: 16, padding: 8, backgroundColor: props.color }}>
           {props.iconName ? <CustomIcon name={props.iconName} size={24} color="white" />: <View></View>}
            <Text style={{ ...styles.textColor, color: 'white', width: '100%', fontSize: 16 }}>{props.title}</Text>
        </View>
    )
}

let deviceWidth = Dimensions.get('window').width

export default MenuCard

const styles = StyleSheet.create({})
