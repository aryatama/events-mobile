import React, { useState, useContext } from 'react';
import axios from 'axios';
import { useMutation } from 'react-query';
import { API_BASE_URL, API_KEY, STRIPE_PK } from '@env';
import useQuery from '../utils/parent/useQuery';


export const getSomeNotifications = async (pageNum, limitAmt) => {
  try {
    return await axios.get(
      `${API_BASE_URL}/parent/notifications?pageNo=${pageNum}&limitAmt=${limitAmt}`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );
  } catch (error) {
    throw error.response.data;
  }
};

export const getSomeTransactions = async (childID, pageNo) => {
  try {
    return await axios.get(
      `${API_BASE_URL}/parent/child/${childID}/transactions?pageNo=${pageNo}`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );
  } catch (error) {
    throw error.response.data;
  }
};

export const getParent = () =>
  useQuery('getParent', async () => {
    const response = await axios.get(`${API_BASE_URL}/parent`, {
      headers: {
        Authorization: API_KEY,
      },
    });
    return response.data;
  });

  export const getSubscription = () =>
  useQuery('getSubscription', async () => {
    const response = await axios.get(`${API_BASE_URL}/parent/subscriptions`, {
      headers: {
        Authorization: API_KEY,
      },
    });
    return response.data;
  });


export const getParentBalance = () =>
  useQuery('getBalance', async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/parent/balance`, {
        headers: {
          Authorization: API_KEY,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildren = () =>
  useQuery('getChildren', async () => {
    const response = await axios.get(`${API_BASE_URL}/parent/children`, {
      headers: {
        Authorization: API_KEY,
      },
    });

    return response.data;
  });

export const getChildTransactions = (childID) =>
  useQuery(['getChildTransaction', childID], async () => {
    const response = await axios.get(
      `${API_BASE_URL}/parent/child/${childID}/transactions`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );
    return response.data;
  });

export const getChildInsights = (childID) =>
  useQuery(['getChildInsights', childID], async () => {
    try {
      return await axios.get(
        `${API_BASE_URL}/parent/child/${childID}/insights`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildSummary = (childID, startWeek, endWeek) =>
  useQuery(['getChildSummary', childID, startWeek, endWeek], async () => {
    try {
      console.log('8080', childID, startWeek, endWeek);
      return await axios.get(
        `${API_BASE_URL}/parent/child/${childID}/summary?startWeek=${startWeek}&endWeek=${endWeek}`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildGoals = (childID) =>
  useQuery(['getChildGoals', childID], async () => {
    try {
      return await axios.get(`${API_BASE_URL}/parent/child/${childID}/goals`, {
        headers: {
          Authorization: API_KEY,
        },
      });
    } catch (error) {
      throw error.response.data;
    }
  });

export const postFavoriteGoal = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/goal/${data.goalId}`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postOnBoardingSendEmailOTP = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/onboarding/sendEmailOtp`,
        {
          email: data.email,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postParentChildRegister = () =>
  useMutation(async (data) => {
    try {
      console.log('9970', data);
      return await axios.post(
        `${API_BASE_URL}/parent/child/register`,
        {
          email: data.email,
          phone: data.phone,
          password: data.password,
          fullName: data.fullName,
          yob: data.dob,
          personalizedCardId: data.personalizedCardId,
          deviceType: data.deviceType,
          sameAddressOn: data.sameAddressOn,
          address: {
            address_1: data.address.address_1,
            address_2: data.address.address_2,
            city: data.address.country,
            countryCode: data.address.countryCode,
            country: data.address.country,
            postalCode: data.address.postalCode,
            state: data.address.country,
          },
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildLock = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/lock`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildUnlock = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/unlock`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildActivateCard = () =>
  useMutation(async (data) => {
    try {
      console.log('postChildActivateCard', data);
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/activateCard`,
        {
          proxyCode: data.proxyCode,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const deleteChild = () =>
  useMutation(async (data) => {
    try {
      return await axios.delete(
        `${API_BASE_URL}/parent/child/${data.childId}`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
        {},
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getNotifications = (pageNo, limitAmt) =>
  useQuery(['getNotifications', pageNo, limitAmt], async () => {
    try {
      const response = await axios.get(
        `${API_BASE_URL}/parent/notifications?pageNo=${pageNo}&limitAmt=${limitAmt}`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
      return response.data
    } catch (error) {
      throw error.response.data;
    }
  });

export const getNotificationStatus = () =>
  useQuery('getNotificationStatus', async () => {
    const response = await axios.get(
      `${API_BASE_URL}/parent/notifications/pushNotification`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );
    return response;
  });

export const postToggleNotification = () =>
  useMutation(async () => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/notifications`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postNotificationsRead = () =>
  useMutation(
    async () =>
      await axios.post(
        `${API_BASE_URL}/parent/notifications/read`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      ),
  );

export const getParentChildrenTask = () =>
  useQuery(['getParentChildrenTask'], async () => {
    try {
      return await axios.get(`${API_BASE_URL}/parent/children/tasks`, {
        headers: {
          Authorization: API_KEY,
        },
      });
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildTaskById = (childId, taskId) =>
  useQuery(['getChildTaskById', childId, taskId], async () => {
    try {
      let data = await axios.get(
        `${API_BASE_URL}/parent/child/${childId}/task/${taskId}`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
      return data;
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildTask = () =>
  useMutation(async (data) => {
    console.log('ADDTASKRECEIVED', data);
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/task`,
        {
          taskName: data.taskName,
          taskDescription: data.taskDescription,
          repeat: data.repeat,
          dueDate: data.dueDate,
          monetaryReward: data.monetaryReward,
          rewardAmount: data.rewardAmount,
          reward: data.rewardName,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildAllowance = (childId) =>
  useQuery(['getChildAllowance', childId], async () => {
    const response = await axios.get(
      `${API_BASE_URL}/parent/child/${childId}/allowance`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );

    return response;
  });

export const postChildAllowance = () =>
  useMutation(async (data) => {
    console.log('ALLOWANCE', data);
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/allowance`,
        {
          monthlyAllowance: data.monthlyAllowance,
          allowanceDate: data.allowanceDate,
          allowanceOn: data.allowanceOn,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getChildPocketMoney = (childId) =>
  useQuery(['getChildPocketMoney', childId], async () => {
    const response = await axios.get(
      `${API_BASE_URL}/parent/child/${childId}/pocketMoney`,
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );

    return response;
  });

export const postChildPocketMoney = () =>
  useMutation(async (data) => {
    let frequency;
    switch (data.pocketMoneyFrequency) {
      case 'One-off':
        frequency = 'oneOff';
        break;
      case 'Monthly':
        frequency = 'monthly';
        break;
      case 'Weekly':
        frequency = 'weekly';
        break;
      case 'Fortnightly':
        frequency = 'fortnightly';
        break;
      case 'Daily':
        frequency = 'daily';
        break;
    }

    /* console.log('POCKETMONEY', data);
    console.log('POCKETMONEY frequency', frequency); */
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/pocketMoney`,
        {
          pocketMoneyAmount: data.pocketMoneyAmount,
          pocketMoneyFrequency: frequency,
          pocketMoneyOn: data.pocketMoneyOn,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postWithdrawChildBalance = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/withdraw`,
        {
          amount: data.amount,
          message: data.message,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postWalletFunds = () =>
  useMutation(async (data) => {
    let cardDetails = {
      // 'card[number]': data.cardNumber.value,
      // 'card[exp_month]': data.cardExpiryMonth.value.split('/')[0],
      // 'card[exp_year]': data.cardExpiryYear.value.split('/')[1],
      // 'card[cvc]': data.cardCVC.value,
      'card[number]': data.cardNumber,
      'card[exp_month]': data.cardExpiryMonth,
      'card[exp_year]': data.cardExpiryYear,
      'card[cvc]': data.cardCVC,
    };
    let formBody = [];
    for (let property in cardDetails) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(cardDetails[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    const getStripeToken = await axios.post(
      'https://api.stripe.com/v1/tokens',
      formBody,
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${STRIPE_PK}`,
        },
      },
    );

    try {
      return await axios.post(
        `${API_BASE_URL}/parent/wallet/funds`,
        {
          amount: data.amount,
          description: data.description,
          tokenId: getStripeToken.data.id,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postWalletVirtualAccounts = () =>
  useMutation(async (data) => {
    console.log('postWalletVirtualAccounts', data);
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/wallet/virtualAccount`,
        {
          bankHolderName: data.bankHolderName,
          bankAccountNumber: data.bankAccountNumber,
          bankCode: data.bankCode,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getWalletVirtualAccounts = () =>
  useQuery('getWalletVirtualAccounts', async () => {
    try {
      return await axios.get(`${API_BASE_URL}/parent/wallet/virtualAccount`, {
        headers: {
          Authorization: API_KEY,
        },
      });
    } catch (error) {
      throw error.response.data;
    }
  });

export const postProfilePicture = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/profilePicture`,
        {
          profilePicture: data.profilePicture,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildProfilePicture = () =>
  useMutation(async (data) => {
    console.log('1109', data);
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/profilePicture`,
        {
          profilePicture: data.profilePicture,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postCoverPicture = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/coverPicture`,
        {
          coverPicture: data.coverPicture,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postChildCoverPicture = () =>
  useMutation(async (data) => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/child/${data.childId}/coverPicture`,
        {
          coverPicture: data.coverPicture,
        },
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const getSpendingLimits = (childID) =>
  useQuery(['getSpendingLimits', childID], async () => {
    try {
      return await axios.get(
        `${API_BASE_URL}/parent/child/${childID}/adjustLimits`,
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });

export const postSpendingLimits = () =>
  useMutation(async (data) => {
    console.log('ADJUSTLIMITRECEIVED', data);
    return await axios.post(
      `${API_BASE_URL}/parent/child/${data.childId}/adjustLimits`,
      {
        dailySpendLimit: data.dailySpendLimit,
        weeklySpendLimit: data.weeklySpendLimit,
        fortnightlySpendLimit: data.fortnightlySpendLimit,
        monthlySpendLimit: data.monthlySpendLimit,
      },
      {
        headers: {
          Authorization: API_KEY,
        },
      },
    );
  });

export const postLogout = () =>
  useMutation(async () => {
    try {
      return await axios.post(
        `${API_BASE_URL}/parent/logout`,
        {},
        {
          headers: {
            Authorization: API_KEY,
          },
        },
      );
    } catch (error) {
      throw error.response.data;
    }
  });
