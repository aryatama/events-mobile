import 'react-native-gesture-handler';
import React from 'react';
import RootNavigation from './app/navigationStack/Root';
import {
  QueryClientProvider,
} from 'react-query'
import queryClient from './app/utils/parent/query';
import { LogBox } from 'react-native';


const App = () => {
  LogBox.ignoreLogs(['Setting a timer', "VirtualizedLists", "currently", "Deprecation", "Each", "Failed", "Can't"])
  return (
    <QueryClientProvider client={queryClient}>
      <RootNavigation />
    </QueryClientProvider>
  )
}

export default App;
